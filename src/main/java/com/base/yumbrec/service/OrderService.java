package com.base.yumbrec.service;

import java.util.List;
import java.util.Map;

import com.base.yumbrec.model.AllAreaDetails;
import com.base.yumbrec.model.Area;
import com.base.yumbrec.model.BillDetails;
import com.base.yumbrec.model.City;
import com.base.yumbrec.model.DeliveryCharges;
import com.base.yumbrec.model.Orders;
import com.base.yumbrec.model.PaymodeDetails;
import com.base.yumbrec.model.SubArea;
import com.base.yumbrec.model.Tax;
import com.base.yumbrec.model.User;

public interface OrderService 
{

	public Map<String, Object> getPackageDetails();

	public Map<String,Object> getPackageDetailsBasedId(String str);
	
	public Map<String,Object> getAddressBasedOnUserId(User userForAddress);
	
	public Map<String,Object> getMasterAddressDetails();
	
	public List<DeliveryCharges> deliveryChargesList();
	
	public List<Tax> gettaxList();
	
	public BillDetails orderSave(Orders orders);
	
	public boolean  updatePaymodeId(PaymodeDetails paymodeDetails);
	
	public Map<String, Object> getMyOrders(int userId);
	
	public List<AllAreaDetails> getAreaDetails();
	
	//public List<SubArea> getSubAreas(String areaId);
	
	//public List<Area> onedayorderArea();
	
	 //public	Map<String, Object> getpincode();
	 
	 //public List<City> getcityList();
	 
	 public String addresSave(User user);
	 
	 //public Map<String, Object> getAddressList(String userId);

}
