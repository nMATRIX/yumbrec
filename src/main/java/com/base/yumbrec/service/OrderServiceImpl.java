package com.base.yumbrec.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.base.yumbrec.dao.OrderDao;
import com.base.yumbrec.model.Address;
import com.base.yumbrec.model.AllAreaDetails;
import com.base.yumbrec.model.Area;
import com.base.yumbrec.model.BillDetails;
import com.base.yumbrec.model.BillWiseList;
import com.base.yumbrec.model.City;
import com.base.yumbrec.model.DeliveryCharges;
import com.base.yumbrec.model.ListOfPackages;
import com.base.yumbrec.model.OrderDetails;
import com.base.yumbrec.model.Orders;
import com.base.yumbrec.model.PaymodeDetails;
import com.base.yumbrec.model.PincodeDetails;
import com.base.yumbrec.model.State;
import com.base.yumbrec.model.Tax;
import com.base.yumbrec.model.User;
import com.base.yumbrec.util.ProjectDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class OrderServiceImpl implements OrderService
{

	@Autowired
	OrderDao orderDao;

	
	@Override
	public Map<String, Object> getPackageDetails() 
	{
		ResponseEntity<String> result=orderDao.getPackageDetails();
		JSONObject jObject  = new JSONObject(result.getBody());
		String successkey=jObject.getString("key");
		Map<String,Object> model=new HashMap<String, Object>();
		if(successkey.equals(ProjectDetails.SUCCESSKEY))
		{
		JSONArray packageDetail=jObject.getJSONArray("listOfPackages");
		String packageDetailinString=packageDetail.toString();
		Type listType = new TypeToken<ArrayList<ListOfPackages>>(){}.getType();
		Gson gson = new Gson();
		List<ListOfPackages> allpackageDetails = gson.fromJson(packageDetailinString,listType);
	
		model.put("allpackageDetails", allpackageDetails);
	    }
		return model;
    }

/*	@Override
	public Map<String, Object> getMasterAddressDetails() 
	{
		Map<String,Object> model=new HashMap<String, Object>(); 
		ResponseEntity<String> addressList=orderDao.getAddressDetails();
	    JSONObject jObjectArea  = new JSONObject(addressList.getBody());
	    JSONArray jsonArry=jObjectArea.getJSONArray("allAreaDetails");
	    String addresslistinstring=jsonArry.toString();
	    Type listType = new TypeToken<ArrayList<AllAreaDetails>>(){}.getType();
	    Gson gson = new Gson();
	    List<AllAreaDetails> allAreadetails = gson.fromJson(addresslistinstring,listType);
	    
	    model.put("allAreadetails", allAreadetails);
		
	    System.out.println("allAreadetails from order service in get masterdetails "+allAreadetails);
	    
	return model;
	}*/
	
	
	


	@Override
	public Map<String, Object> getPackageDetailsBasedId(String str) 
	{
		return null;
	}


	@Override
	public Map<String, Object> getAddressBasedOnUserId(User userForAddress) 
	{
		Map<String,Object> model=new HashMap<String, Object>();
		String  result=orderDao.getAddessBasedOnUserId(userForAddress); 
		JSONObject jObject  = new JSONObject(result);
		String ketSucess=jObject.getString("key");
		if(ketSucess.equals(ProjectDetails.SUCCESSKEY))
		{
		JSONArray areaDetails=jObject.getJSONArray("Address");
		String areaDetailsString=areaDetails.toString();
		Type listType = new TypeToken<ArrayList<Address>>(){}.getType();
		Gson gson = new Gson();
		List<Address> addressList = gson.fromJson(areaDetailsString,listType);
		model.put("newAddress", null);
		if(addressList!=null && addressList.size()>0)
		  {
		    for(Address address:addressList)
		     {
		       if(address.getAddrTypeId()==1)
		          {
		             model.put("homeAddress", address);
		          }
		        else if(address.getAddrTypeId()==2)
		        {
		             model.put("officeAddress", address);
		          }  
		          else if(address.getAddrTypeId()==3)
		          {
		            model.put("newAddress", address);
		          }
		     }
		  }
		}
		else
		{
		   model.put("homeAddress", null);
		   model.put("officeAddress", null);
		   model.put("newAddress", null);
		}
		  return model;
	}

	@Override
	public Map<String, Object> getMasterAddressDetails() 
	{
		Map<String,Object> model=new HashMap<String, Object>(); 
		ResponseEntity<String> addressList=orderDao.getAddressDetails();
	    JSONObject jObjectArea  = new JSONObject(addressList.getBody());
		String successkeyArea=jObjectArea.getString("key");
		if(successkeyArea.equals(ProjectDetails.SUCCESSKEY))
		{
		JSONArray areaDetail=jObjectArea.getJSONArray("areaList");
		String areaDetailinString=areaDetail.toString();
		Type listType = new TypeToken<ArrayList<Area>>(){}.getType();
		Gson gson=new Gson();
		List<Area> allAreaDetails = gson.fromJson(areaDetailinString,listType);
			
		model.put("allAreaDetails", allAreaDetails);
			
		JSONArray cityDetail=jObjectArea.getJSONArray("cityList");
		String cityDetailinString=cityDetail.toString();
		listType = new TypeToken<ArrayList<City>>(){}.getType();
		List<City> allCityDetails = gson.fromJson(cityDetailinString,listType);
		System.out.println("cityDetail list:"+allCityDetails);
		model.put("allCityDetails", allCityDetails);
			
		JSONArray pincodeListDetails=jObjectArea.getJSONArray("Pincode");
		String  pincodeDetailsString=pincodeListDetails.toString();
	    listType = new TypeToken<ArrayList<PincodeDetails>>(){}.getType();
	    List<PincodeDetails> pincodeList = gson.fromJson(pincodeDetailsString,listType);
	    model.put("pincodeList", pincodeList);
	    
	    JSONArray statelist=jObjectArea.getJSONArray("stateList");
	    String statelistString=statelist.toString();
	    listType= new TypeToken<ArrayList<State>>() {}.getType();
	    List<State> stateList = gson.fromJson(statelistString,listType);
	    model.put("stateList", stateList);
	    return model;
		}
	return model;
	}


	@Override
	public List<DeliveryCharges> deliveryChargesList() 
	{
		ResponseEntity<String> string=orderDao.deliveryChargesList();
		JSONObject jObject  = new JSONObject(string.getBody());
		Map<String,Object> model=new HashMap<String, Object>();
		JSONArray chargeListObject=jObject.getJSONArray("DeliveryChargesList");
		String deliverychargeString=chargeListObject.toString();
		Type listType = new TypeToken<ArrayList<DeliveryCharges>>(){}.getType();
		Gson gson=new Gson();
		List<DeliveryCharges> deliveryCharges = gson.fromJson(deliverychargeString,listType);
	return deliveryCharges;
	}

	@Override
	public List<Tax> gettaxList() 
	{
		ResponseEntity<String> string=orderDao.gettaxList();
	    JSONObject jObject  = new JSONObject(string.getBody());
	    Map<String,Object> model=new HashMap<String, Object>();
	    JSONArray taxListObject=jObject.getJSONArray("TaxList");
	    String taxString=taxListObject.toString();
	    Type listType = new TypeToken<ArrayList<Tax>>(){}.getType();
	    Gson gson=new Gson();
	    List<Tax> taxCharges = gson.fromJson(taxString,listType);
	return taxCharges;
}


	@Override
	public BillDetails orderSave(Orders orders) 
	{
         String result=orderDao.orderSave(orders);
		 JSONObject jObject  = new JSONObject(result);
		 if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
		 {
			 BillDetails billDetails=new BillDetails();
			 JSONArray chargeListObject=jObject.getJSONArray("SavedOrderDetails");
			 String deliverychargeString=chargeListObject.toString();
			 Type listType = new TypeToken<ArrayList<OrderDetails>>(){}.getType();
			 Gson gson=new Gson();
			 List<OrderDetails> savedOrderDetails = gson.fromJson(deliverychargeString,listType);
			  
			 billDetails.setCustBillId(jObject.getInt("custBillId"));
			 billDetails.setPaymodId(jObject.getInt("paymodId") );
			 billDetails.setTransactionId(jObject.getString("TransactionId") ); 
			 billDetails.setStatus(jObject.getString("status") ); 
			 billDetails.setSavedOrderDetails(savedOrderDetails);
			return billDetails;
		  }
		  else
		  {
			 return null;
		  }
	}


	@Override
	public boolean updatePaymodeId(PaymodeDetails paymodeDetails) 
	{
		String result=orderDao.updatePaymodeId(paymodeDetails);
		JSONObject jObject  = new JSONObject(result);
		if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
		{
			return true;
		}
		else
		{
		   return false;		 
	    }
	}

	@Override
	public Map<String, Object> getMyOrders(int userId) 
	{
		  int paidId=7;
		  String result=orderDao.getMyOrders(userId,paidId);
		  JSONObject jObject  = new JSONObject(result);
		  Map<String,Object> model=new HashMap<String, Object>();
		  if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
		  {
		     Gson gson = new Gson();
		     JSONArray packageDetail=jObject.getJSONArray("billWiseList");
		     String packagedetails=packageDetail.toString();
		     Type listType = new TypeToken<ArrayList<BillWiseList>>(){}.getType();
		     List<BillWiseList> paidOrdersList = gson.fromJson(packagedetails,listType);
		     System.out.println("paidOrdersList in service: " +packagedetails);
		     model.put("paidOrdersList", paidOrdersList);
		  }
		  else if(jObject.getString("key").equals(ProjectDetails.FAILUREKEY))
		  {
			 System.out.println("false");
		     model.put("paidOrdersList", null);
		  }
		  
		  paidId=6;
		  result=orderDao.getMyOrders(userId,paidId);
		  jObject  = new JSONObject(result);
		  if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
		   {
		     Gson gson = new Gson();
		     JSONArray packageDetail=jObject.getJSONArray("billWiseList");
		     String packagedetails=packageDetail.toString();
		     Type listType = new TypeToken<ArrayList<BillWiseList>>(){}.getType();
		     List<BillWiseList> unPaidOrdersList = gson.fromJson(packagedetails,listType);
		     System.out.println("paid id =6 un paid OrdersList in service:"+packagedetails);
		     model.put("unPaidOrdersList", unPaidOrdersList);
		  }
		  else if(jObject.getString("key").equals(ProjectDetails.FAILUREKEY))
		  {
			  System.out.println("false");
		      model.put("unPaidOrdersList", null);
		  }
            System.out.println("from order service for get my orders "+model);
		  return model;
	}


	@Override
	public List<AllAreaDetails> getAreaDetails() 
	{
		String result=orderDao.getAreaDetails();
		System.out.println("result from get area details "+result);
		JSONObject jObject  = new JSONObject(result);
		/*JSONArray areaDetailsarray=jObject.getJSONArray("");*/
		String areaDetailsString=jObject.toString();
		Type listType = new TypeToken<ArrayList<AllAreaDetails>>(){}.getType();
		Gson gson = new Gson();
		List<AllAreaDetails> areaDetailsList = gson.fromJson(areaDetailsString,listType);
		return areaDetailsList;
	}

	@Override
	public String addresSave(User user) 
	{
		String result=orderDao.addresSave(user);
		return result;
	}
}
