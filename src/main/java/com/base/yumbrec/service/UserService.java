package com.base.yumbrec.service;

import java.util.Map;

import com.base.yumbrec.model.User;

public interface UserService 
{
	public User registerUser(User user);
	
	public User userLogin(User user);
	
	public Map<String,Object> getAddressBasedOnUserId(User userForAddress);
	
	public User forgotUserPassword(User user);
	
	public User changePassword(User user);
	
	public Map<String, Object> getPackageDetails();
	
	public String updateUserAddress(User user);
	
	public boolean getcutoffTimeBasedonId(String mealtimeId, String startdateIdon) ;
	
	
}
