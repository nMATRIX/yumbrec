package com.base.yumbrec.service;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.base.yumbrec.dao.UserDao;
import com.base.yumbrec.model.Address;
import com.base.yumbrec.model.Area;
import com.base.yumbrec.model.City;
import com.base.yumbrec.model.ListOfPackages;
import com.base.yumbrec.model.MealDuration;
import com.base.yumbrec.model.MealTime;
import com.base.yumbrec.model.PincodeDetails;
import com.base.yumbrec.model.User;
import com.base.yumbrec.model.WeekType;
import com.base.yumbrec.util.CurrentDate;
import com.base.yumbrec.util.ProjectDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserServiceImpl implements UserService 
{

	@Autowired
	UserDao userDao;
	
	@Override
	public User registerUser(User user) 
	{
		String result=userDao.registerUser(user);
		JSONObject jObject  = new JSONObject(result);
		if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
		{
			user.setUserId(jObject.getInt("customerId"));
			user.setSuccessMessage(jObject.getString("status"));
			user.setSuccesskey(jObject.getString("key"));
			System.out.println("json key "+jObject.getString("key"));
			
			
			/*user.setUserFirstName(jObject.getString("firstname"));
			user.setUserLastName(jObject.getString("lastname"));*/
		}
		else
		{
			user.setErrorMessage(jObject.getString("status"));
			user.setFailurekey("2");
		}
		
		return user;
	}

	@Override
	public User userLogin(User user)
	{
			  String result=userDao.userLogin(user);
			  JSONObject jObject  = new JSONObject(result);
			  if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY))
			  {
			   user.setUserId(jObject.getInt("custid"));
			   user.setUserContactNumber(jObject.getString("custcontact"));
			   user.setUserEmailId(jObject.getString("emailId"));
			   user.setUserFirstName(jObject.getString("firstname"));
			   user.setUserLastName(jObject.getString("lastname"));
			   user.setSuccessMessage(jObject.getString("status"));
			  }
			  else if(jObject.getString("key").equals(ProjectDetails.FAILUREKEY))
			  {
			   user.setErrorMessage("Your Password is wrong");
			  }
			  else if(jObject.getString("key").equals(ProjectDetails.DATANOTSEND))
			  {
			   user.setErrorMessage("please enter correct details");
			  }
			  else if(jObject.getString("key").equals(ProjectDetails.NOTREGISTERKEY))
			  {
			   user.setErrorMessage("Invalid Details,please try again");
			  }
			  return user;
	}

	@Override
	public Map<String, Object> getAddressBasedOnUserId(User userForAddress) 
	{
		  Map<String,Object> model=new HashMap<String, Object>();
		  String  result=userDao.getAddessBasedOnUserId(userForAddress); 
		  JSONObject jObject  = new JSONObject(result);
		  String ketSucess=jObject.getString("key");
		  if(ketSucess.equals(ProjectDetails.SUCCESSKEY))
		  {
		  JSONArray areaDetails=jObject.getJSONArray("Address");
		  String areaDetailsString=areaDetails.toString();
		  Type listType = new TypeToken<ArrayList<Address>>(){}.getType();
		  Gson gson = new Gson();
		  List<Address> addressList = gson.fromJson(areaDetailsString,listType);
		  model.put("newAddress", null);
		  if(addressList!=null && addressList.size()>0)
		  {
		    for(Address address:addressList)
		     {
		      if(address.getAddrTypeId()==1)
		       {
		          model.put("homeAddress", address);
		          
		          System.out.println("home adress"+address.getId());
		          //System.out.println("for home address "+address.getArea().getAreaDesc());
		          
		       }
		       else if(address.getAddrTypeId()==2)
		       {
		         model.put("officeAddress", address);
		         
		         //System.out.println("for office address "+address.getArea().getAreaDesc());
		       }
		       else if(address.getAddrTypeId()==3)
		       {
		         model.put("newAddress", address);
		        // System.out.println("for new address "+address.getArea().getAreaDesc());

		       }
		    }
		  }
		  }
		  else
		  {
		   model.put("homeAddress", null);
		   model.put("officeAddress", null);
		   model.put("newAddress", null);
		  }
		  return model;
	}

	@Override
	public User forgotUserPassword(User user) 
	{
		String result=userDao.forgotUserPassword(user);
		JSONObject jObject  = new JSONObject(result);
		if(jObject.getString("key").equals(ProjectDetails.SUCCESSKEY)){
			user.setSuccessMessage(jObject.getString("status"));
		}else if(jObject.getString("key").equals(ProjectDetails.FAILUREKEY)){
			user.setErrorMessage(jObject.getString("status"));
		}
		return user;
	}

	@Override
	public User changePassword(User user) 
	{
		String result=userDao.changePassword(user);
		
		return null;
	}

	@Override
	public Map<String, Object> getPackageDetails() 
	{
		ResponseEntity<String> result=userDao.getPackageDetails();
		JSONObject jObject  = new JSONObject(result.getBody());
		String successkey=jObject.getString("key");
		Map<String,Object> model=new HashMap<String, Object>();
		if(successkey.equals(ProjectDetails.SUCCESSKEY)){
		JSONArray packageDetail=jObject.getJSONArray("listOfPackages");
		String packageDetailinString=packageDetail.toString();
		Type listType = new TypeToken<ArrayList<ListOfPackages>>(){}.getType();
		Gson gson = new Gson();
		List<ListOfPackages> allpackageDetails = gson.fromJson(packageDetailinString,listType);
	
		model.put("allpackageDetails", allpackageDetails);
		
		//enouf
		
		String mealTimeString=userDao.getMealTimeDetails();
		JSONObject jObjectForMealTime  = new JSONObject(mealTimeString);
		JSONArray mealTimeListObject=jObjectForMealTime.getJSONArray("MealTimeList");
		String mealTimeListDetailinString=mealTimeListObject.toString();
		listType = new TypeToken<ArrayList<MealTime>>(){}.getType();
		List<MealTime> mealTimeList = gson.fromJson(mealTimeListDetailinString,listType);
		model.put("mealTimeList", mealTimeList);
		String getMealDurationString=userDao.getMealDurationList();
	    JSONObject jObjectForMealDuration  = new JSONObject(getMealDurationString);
		JSONArray mealDurationListObject=jObjectForMealDuration.getJSONArray("MealDurationList");
		String mealDurationDetailinString=mealDurationListObject.toString();
		listType = new TypeToken<ArrayList<MealDuration>>(){}.getType();
		List<MealDuration> mealDurationList = gson.fromJson(mealDurationDetailinString,listType);
		model.put("mealDurationList", mealDurationList);
		String weekDurationString=userDao.getWeekType();
		JSONObject jObjectweekDuration  = new JSONObject(weekDurationString);
		JSONArray weekTypeDetails=jObjectweekDuration.getJSONArray("WeekDurationList");
		String weekTypeDetailsString=weekTypeDetails.toString();
		Type listType1 = new TypeToken<ArrayList<WeekType>>(){}.getType();
		List<WeekType> weekDuartionList = gson.fromJson(weekTypeDetailsString,listType1);
		model.put("weekDuartionList", weekDuartionList);
		ResponseEntity<String> addressList=userDao.getAddressDetails();
		JSONObject jObjectArea  = new JSONObject(addressList.getBody());
		String successkeyArea=jObjectArea.getString("key");
		JSONArray areaDetail=jObjectArea.getJSONArray("areaList");
		String areaDetailinString=areaDetail.toString();
		listType = new TypeToken<ArrayList<Area>>(){}.getType();
		List<Area> allAreaDetails = gson.fromJson(areaDetailinString,listType);
		model.put("allAreaDetails", allAreaDetails);
		JSONArray cityDetail=jObjectArea.getJSONArray("cityList");
		String cityDetailinString=cityDetail.toString();
		listType = new TypeToken<ArrayList<City>>(){}.getType();
		List<City> allCityDetails = gson.fromJson(cityDetailinString,listType);
		System.out.println("cityDetail list:"+allCityDetails);
		model.put("allCityDetails", allCityDetails);
		JSONArray pincodeListDetails=jObjectArea.getJSONArray("Pincode");
		String  pincodeDetailsString=pincodeListDetails.toString();
		listType = new TypeToken<ArrayList<PincodeDetails>>(){}.getType();
		List<PincodeDetails> pincodeList = gson.fromJson(pincodeDetailsString,listType);
		model.put("pincodeList", pincodeList);
		    return model;
		}
		return model;
	}
	
	
	@Override
	public boolean getcutoffTimeBasedonId(String mealtimeId, String startdateIdon) 
	 {
	  Gson gson = new Gson();
	  String mealTime=userDao.getMealTimeDetails();
	  boolean status = false;
	  JSONObject jObjectForMealTime  = new JSONObject(mealTime);
	  JSONArray mealTimeListObject=jObjectForMealTime.getJSONArray("MealTimeList");
	  String mealTimeListDetailinString=mealTimeListObject.toString();
	  Type listType = new TypeToken<ArrayList<MealTime>>(){}.getType();
	  List<MealTime> mealTimeList = gson.fromJson(mealTimeListDetailinString,listType);
	  System.out.println("mealtime list from getcutoffTimeBasedonId in user service: "+mealTimeList);
	  String currentDateString=CurrentDate.dateInGMT();
	  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  Date inputDate=null;
	  Date currentDate=null;
	  try 
	    {
	       inputDate=inputDateFormat.parse(startdateIdon);
	       currentDate=dateFormat.parse(currentDateString);
	    } 
	  catch (ParseException e) 
	    {
	   e.printStackTrace();
	    }
	  String inputDateFormattedString=dateFormat.format(inputDate);
	  Date inputDateCorrectFormat=null;
	  try
	   {
	     inputDateCorrectFormat=dateFormat.parse(inputDateFormattedString);
	     
	     System.out.println("inputDateCorrectFormat "+inputDateCorrectFormat);
	   } 
	  catch (ParseException e) 
	  {
	   e.printStackTrace();
	  }
	  if(inputDateCorrectFormat.after(currentDate))
	  {
		  //if user select tomorrow date then no need to bother about cut off time so return true...
	    System.out.println("input date is greater");
	    return true;
	  }
	  else
	  {
	    System.out.println("input date is smaller");
	    System.out.println("current time: from getcutoff.. metbhod in user service "+currentDate.getTime());
	    if(mealTimeList!=null && mealTimeList.size()>0)
	    {
	      for(MealTime mealTimeObject:mealTimeList)
	       {
	         if(mealTimeObject.getId()==Integer.parseInt(mealtimeId))
	         {
	            if(timevalidator(mealTimeObject.getCutoffTime()))
	            {
	               return true;
	            }
	            else
	            {
	              continue;
	            }
	         }
	         else
	         {
	           status=false;
	         }
	       }
	    }
	  }
	  return status; 
	}
	    
	public boolean timevalidator(String cutofftime)
    {
        DateFormat dateintimeformat=new SimpleDateFormat("h:mm a");
        SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
        String onlytime = sdf4.format(new Date());
        System.out.println("only time: from timevalidator in user service "+onlytime);
        Date cutofftimeindate=new Date(),currenttime=new Date();
        try 
        {
            cutofftimeindate=dateintimeformat.parse(cutofftime);
            currenttime=dateintimeformat.parse(onlytime);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        System.out.println("cut off time in date : fromtime validator in user service "+cutofftimeindate.getTime());
        
        return cutofftimeindate.getTime() - currenttime.getTime() > 0;

    }
	      
	@Override
	public String updateUserAddress(User user)
	{
		String result=userDao.updateUserAddress(user);
		return result;
	}
}
