package com.base.yumbrec.model;

public class CouponDetails {

	private int id;
	private String cupnTypCode;
	private User user;
	private float grandTotal;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCupnTypCode() {
		return cupnTypCode;
	}
	public void setCupnTypCode(String cupnTypCode) {
		this.cupnTypCode = cupnTypCode;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public float getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(float grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
}
