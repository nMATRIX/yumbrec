package com.base.yumbrec.model;

import java.io.Serializable;

public class CartOrder implements Serializable
{
	 /**
	 * 
	 */
	 private MealPlan mealPlan;
	 private MealTime mealTime;
	 private Price price;
	 private WeekType weekType;
	 private String packageName;
	 private FoodType foodType;
	 private PackageDescription pkgDesc;
	 private int packageId;
	 private int quantity;
	 private float priceOfItem;
	 private int id;
	 private String date;
	 private String mealTimeDetails;
	 private boolean packageStatus;
	 private String orderStartDate;
	 private String orderEndDate;
	 private float itemPriceForOneQuantity;
	 private PackageDuration packageDuration;
	 private Address address;
	 private MealType  mealType;
	 private Tax   tax;
	 private float taxAmount;
	 
	 
	 public String getOrderStartDate() {
			return orderStartDate;
		}
		public void setOrderStartDate(String orderStartDate) {
			this.orderStartDate = orderStartDate;
		}
	public MealPlan getMealPlan() {
		return mealPlan;
	}
	public void setMealPlan(MealPlan mealPlan) {
		this.mealPlan = mealPlan;
	}
	public MealTime getMealTime() {
		return mealTime;
	}
	public void setMealTime(MealTime mealTime) {
		this.mealTime = mealTime;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}
	public WeekType getWeekType() {
		return weekType;
	}
	public void setWeekType(WeekType weekType) {
		this.weekType = weekType;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public FoodType getFoodType() {
		return foodType;
	}
	public void setFoodType(FoodType foodType) {
		this.foodType = foodType;
	}
	
	
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getPriceOfItem() {
		return priceOfItem;
	}
	public void setPriceOfItem(float priceOfItem) {
		this.priceOfItem = priceOfItem;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMealTimeDetails() {
		return mealTimeDetails;
	}
	public void setMealTimeDetails(String mealTimeDetails) {
		this.mealTimeDetails = mealTimeDetails;
	}
	public boolean isPackageStatus() {
		return packageStatus;
	}
	public void setPackageStatus(boolean packageStatus) {
		this.packageStatus = packageStatus;
	}
	public String getOrderEndDate() {
		return orderEndDate;
	}
	public void setOrderEndDate(String orderEndDate) {
		this.orderEndDate = orderEndDate;
	}
	public float getItemPriceForOneQuantity() {
		return itemPriceForOneQuantity;
	}
	public void setItemPriceForOneQuantity(float itemPriceForOneQuantity) {
		this.itemPriceForOneQuantity = itemPriceForOneQuantity;
	}
	public PackageDuration getPackageDuration() {
		return packageDuration;
	}
	public void setPackageDuration(PackageDuration packageDuration) {
		this.packageDuration = packageDuration;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public MealType getMealType() {
		return mealType;
	}
	public void setMealType(MealType mealType) {
		this.mealType = mealType;
	}
	public Tax getTax() {
		return tax;
	}
	public void setTax(Tax tax) {
		this.tax = tax;
	}
	public float getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(float taxAmount) {
		this.taxAmount = taxAmount;
	}
	public PackageDescription getPkgDesc() {
		return pkgDesc;
	}
	public void setPkgDesc(PackageDescription pkgDesc) {
		this.pkgDesc = pkgDesc;
	}
	@Override
	public String toString() {
		return "CartOrder [mealPlan=" + mealPlan + ", mealTime=" + mealTime + ", price=" + price + ", weekType="
				+ weekType + ", packageName=" + packageName + ", foodType=" + foodType + ", pkgDesc=" + pkgDesc
				+ ", packageId=" + packageId + ", quantity=" + quantity + ", priceOfItem=" + priceOfItem + ", id=" + id
				+ ", date=" + date + ", mealTimeDetails=" + mealTimeDetails + ", packageStatus=" + packageStatus
				+ ", orderEndDate=" + orderEndDate + ", itemPriceForOneQuantity=" + itemPriceForOneQuantity
				+ ", packageDuration=" + packageDuration + ", address=" + address + ", mealType=" + mealType + ", tax="
				+ tax + ", taxAmount=" + taxAmount + "]";
	}
	
	
}
