package com.base.yumbrec.model;

public class MyOrders {
private int packagQuantity;
private int packagId;
private int orderBillId;
private float  packageAmount;
private String orderStartDate;
private String packagType;
private String orderEndDate;
private String weekDuration;
private String mealTime;
private String packageDuration;
public int getPackagQuantity() {
	return packagQuantity;
}
public void setPackagQuantity(int packagQuantity) {
	this.packagQuantity = packagQuantity;
}
public int getPackagId() {
	return packagId;
}
public void setPackagId(int packagId) {
	this.packagId = packagId;
}
public int getOrderBillId() {
	return orderBillId;
}
public void setOrderBillId(int orderBillId) {
	this.orderBillId = orderBillId;
}
public float getPackageAmount() {
	return packageAmount;
}
public void setPackageAmount(float packageAmount) {
	this.packageAmount = packageAmount;
}
public String getOrderStartDate() {
	return orderStartDate;
}
public void setOrderStartDate(String orderStartDate) {
	this.orderStartDate = orderStartDate;
}
public String getPackagType() {
	return packagType;
}
public void setPackagType(String packagType) {
	this.packagType = packagType;
}
public String getOrderEndDate() {
	return orderEndDate;
}
public void setOrderEndDate(String orderEndDate) {
	this.orderEndDate = orderEndDate;
}
public String getWeekDuration() {
	return weekDuration;
}
public void setWeekDuration(String weekDuration) {
	this.weekDuration = weekDuration;
}
public String getMealTime() {
	return mealTime;
}
public void setMealTime(String mealTime) {
	this.mealTime = mealTime;
}
public String getPackageDuration() {
	return packageDuration;
}
public void setPackageDuration(String packageDuration) {
	this.packageDuration = packageDuration;
}
@Override
public String toString() {
	return "MyOrders [packagQuantity=" + packagQuantity + ", packagId=" + packagId + ", orderBillId=" + orderBillId
			+ ", packageAmount=" + packageAmount + ", orderStartDate=" + orderStartDate + ", packagType=" + packagType
			+ ", orderEndDate=" + orderEndDate + ", weekDuration=" + weekDuration + ", mealTime=" + mealTime
			+ ", packageDuration=" + packageDuration + "]";
}




}
