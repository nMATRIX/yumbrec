package com.base.yumbrec.model;

import java.io.Serializable;

public class MealPlan implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String mealPlanDesc;
	private int mealPlanF;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMealPlanDesc() {
		return mealPlanDesc;
	}
	public void setMealPlanDesc(String mealPlanDesc) {
		this.mealPlanDesc = mealPlanDesc;
	}
	public int getMealPlanF() {
		return mealPlanF;
	}
	public void setMealPlanF(int mealPlanF) {
		this.mealPlanF = mealPlanF;
	}
	
	
}
