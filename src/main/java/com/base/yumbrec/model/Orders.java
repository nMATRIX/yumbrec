package com.base.yumbrec.model;

import java.util.List;

public class Orders {
	private String customerId;
	private String delChrg="0";
	private String cupnDiscAmnt="0";
	private String payModId="0";
	private String subAmnt="0";
	private String cupnId="0";
	private String taxAmnt="0";
	private String grndTot="0";
	private String redeemCancelAmnt="0";
	private String transactionId="0";
	private String subGrandTotal="0";
	private List<SavePkg> savePkgs;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDelChrg() {
		return delChrg;
	}
	public void setDelChrg(String delChrg) {
		this.delChrg = delChrg;
	}
	public String getCupnDiscAmnt() {
		return cupnDiscAmnt;
	}
	public void setCupnDiscAmnt(String cupnDiscAmnt) {
		this.cupnDiscAmnt = cupnDiscAmnt;
	}
	public String getPayModId() {
		return payModId;
	}
	public void setPayModId(String payModId) {
		this.payModId = payModId;
	}
	public String getSubAmnt() {
		return subAmnt;
	}
	public void setSubAmnt(String subAmnt) {
		this.subAmnt = subAmnt;
	}
	public String getCupnId() {
		return cupnId;
	}
	public void setCupnId(String cupnId) {
		this.cupnId = cupnId;
	}
	public String getTaxAmnt() {
		return taxAmnt;
	}
	public void setTaxAmnt(String taxAmnt) {
		this.taxAmnt = taxAmnt;
	}
	public String getGrndTot() {
		return grndTot;
	}
	public void setGrndTot(String grndTot) {
		this.grndTot = grndTot;
	}
	public String getRedeemCancelAmnt() {
		return redeemCancelAmnt;
	}
	public void setRedeemCancelAmnt(String redeemCancelAmnt) {
		this.redeemCancelAmnt = redeemCancelAmnt;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getSubGrandTotal() {
		return subGrandTotal;
	}
	public void setSubGrandTotal(String subGrandTotal) {
		this.subGrandTotal = subGrandTotal;
	}
	public List<SavePkg> getSavePkgs() {
		return savePkgs;
	}
	public void setSavePkgs(List<SavePkg> savePkgs) {
		this.savePkgs = savePkgs;
	}
	@Override
	public String toString() {
		return "Orders [customerId=" + customerId + ", delChrg=" + delChrg + ", cupnDiscAmnt=" + cupnDiscAmnt
				+ ", payModId=" + payModId + ", subAmnt=" + subAmnt + ", cupnId=" + cupnId + ", taxAmnt=" + taxAmnt
				+ ", grndTot=" + grndTot + ", redeemCancelAmnt=" + redeemCancelAmnt + ", transactionId=" + transactionId
				+ ", subGrandTotal=" + subGrandTotal + ", savePkgs=" + savePkgs + "]";
	}
	
	

}
