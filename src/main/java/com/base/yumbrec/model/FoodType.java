package com.base.yumbrec.model;

import java.io.Serializable;

public class FoodType implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int 	id;
	private int foodTypeCode;
	private String foodTypeDesc;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFoodTypeCode() {
		return foodTypeCode;
	}
	public void setFoodTypeCode(int foodTypeCode) {
		this.foodTypeCode = foodTypeCode;
	}
	public String getFoodTypeDesc() {
		return foodTypeDesc;
	}
	public void setFoodTypeDesc(String foodTypeDesc) {
		this.foodTypeDesc = foodTypeDesc;
	}
	
	
}
