package com.base.yumbrec.model;

public class Tax {

	
	private int id;
	private int 	taxCode;
	private String taxDesc;
		private String 	taxPnct;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getTaxCode() {
			return taxCode;
		}
		public void setTaxCode(int taxCode) {
			this.taxCode = taxCode;
		}
		public String getTaxDesc() {
			return taxDesc;
		}
		public void setTaxDesc(String taxDesc) {
			this.taxDesc = taxDesc;
		}
		public String getTaxPnct() {
			return taxPnct;
		}
		public void setTaxPnct(String taxPnct) {
			this.taxPnct = taxPnct;
		}
		
		
}
