package com.base.yumbrec.model;

import java.util.List;

public class BillDetails {

	
	
	
	  private int custBillId;
	  private String TransactionId;
	  private String status;
	  private List<OrderDetails> SavedOrderDetails;
	  private String key;
	  private int paymodId;
	  
	public int getCustBillId() {
		return custBillId;
	}
	public void setCustBillId(int custBillId) {
		this.custBillId = custBillId;
	}
	public String getTransactionId() {
		return TransactionId;
	}
	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getPaymodId() {
		return paymodId;
	}
	public void setPaymodId(int paymodId) {
		this.paymodId = paymodId;
	}
	public List<OrderDetails> getSavedOrderDetails() {
		return SavedOrderDetails;
	}
	public void setSavedOrderDetails(List<OrderDetails> savedOrderDetails) {
		SavedOrderDetails = savedOrderDetails;
	}
	  
	
	
}
