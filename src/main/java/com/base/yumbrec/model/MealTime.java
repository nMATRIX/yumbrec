package com.base.yumbrec.model;

import java.io.Serializable;

public class MealTime implements Serializable
{
	private int id;
	private int	mealTimeCode;
	private String	mealTimeDesc;
	private String	slotStrtTime;
	private String	slotEndTime;
	private String	cutoffTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMealTimeCode() {
		return mealTimeCode;
	}
	public void setMealTimeCode(int mealTimeCode) {
		this.mealTimeCode = mealTimeCode;
	}
	public String getMealTimeDesc() {
		return mealTimeDesc;
	}
	public void setMealTimeDesc(String mealTimeDesc) {
		this.mealTimeDesc = mealTimeDesc;
	}
	public String getSlotStrtTime() {
		return slotStrtTime;
	}
	public void setSlotStrtTime(String slotStrtTime) {
		this.slotStrtTime = slotStrtTime;
	}
	public String getSlotEndTime() {
		return slotEndTime;
	}
	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}
	public String getCutoffTime() {
		return cutoffTime;
	}
	public void setCutoffTime(String cutoffTime) {
		this.cutoffTime = cutoffTime;
	}
}
