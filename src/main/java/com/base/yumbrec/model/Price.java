package com.base.yumbrec.model;

public class Price 
{
	private int id;
	private float oneDayPrice;
	private float oneWeekFiveDayPrice;
	private float oneMonthFiveDayPrice;
	private float twoMonthFiveDayPrice;
	private float threeMonthFiveDayPrice;
	private float fourMonthFiveDayPrice;
	private float fiveMonthFiveDayPrice;
	private float sixMonthFiveDayPrice;
	private float oneWeekSixDayPrice;
	private float twoMonthSixDayPrice;
	private float oneMonthSixDayPrice;
	private float fourMonthSixDayPrice;
	private float threeMonthSixDayPrice;
	private float fiveMonthSixDayPrice;
	private float sixMonthSixDayPrice;
	
	private float twoWeekFiveDayPrice;
	private float twoWeekSixDayPrice;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getOneDayPrice() {
		return oneDayPrice;
	}
	public void setOneDayPrice(float oneDayPrice) {
		this.oneDayPrice = oneDayPrice;
	}
	public float getOneWeekFiveDayPrice() {
		return oneWeekFiveDayPrice;
	}
	public void setOneWeekFiveDayPrice(float oneWeekFiveDayPrice) {
		this.oneWeekFiveDayPrice = oneWeekFiveDayPrice;
	}
	public float getOneMonthFiveDayPrice() {
		return oneMonthFiveDayPrice;
	}
	public void setOneMonthFiveDayPrice(float oneMonthFiveDayPrice) {
		this.oneMonthFiveDayPrice = oneMonthFiveDayPrice;
	}
	public float getTwoMonthFiveDayPrice() {
		return twoMonthFiveDayPrice;
	}
	public void setTwoMonthFiveDayPrice(float twoMonthFiveDayPrice) {
		this.twoMonthFiveDayPrice = twoMonthFiveDayPrice;
	}
	public float getThreeMonthFiveDayPrice() {
		return threeMonthFiveDayPrice;
	}
	public void setThreeMonthFiveDayPrice(float threeMonthFiveDayPrice) {
		this.threeMonthFiveDayPrice = threeMonthFiveDayPrice;
	}
	public float getFourMonthFiveDayPrice() {
		return fourMonthFiveDayPrice;
	}
	public void setFourMonthFiveDayPrice(float fourMonthFiveDayPrice) {
		this.fourMonthFiveDayPrice = fourMonthFiveDayPrice;
	}
	public float getFiveMonthFiveDayPrice() {
		return fiveMonthFiveDayPrice;
	}
	public void setFiveMonthFiveDayPrice(float fiveMonthFiveDayPrice) {
		this.fiveMonthFiveDayPrice = fiveMonthFiveDayPrice;
	}
	public float getSixMonthFiveDayPrice() {
		return sixMonthFiveDayPrice;
	}
	public void setSixMonthFiveDayPrice(float sixMonthFiveDayPrice) {
		this.sixMonthFiveDayPrice = sixMonthFiveDayPrice;
	}
	public float getOneWeekSixDayPrice() {
		return oneWeekSixDayPrice;
	}
	public void setOneWeekSixDayPrice(float oneWeekSixDayPrice) {
		this.oneWeekSixDayPrice = oneWeekSixDayPrice;
	}
	public float getTwoMonthSixDayPrice() {
		return twoMonthSixDayPrice;
	}
	public void setTwoMonthSixDayPrice(float twoMonthSixDayPrice) {
		this.twoMonthSixDayPrice = twoMonthSixDayPrice;
	}
	public float getOneMonthSixDayPrice() {
		return oneMonthSixDayPrice;
	}
	public void setOneMonthSixDayPrice(float oneMonthSixDayPrice) {
		this.oneMonthSixDayPrice = oneMonthSixDayPrice;
	}
	public float getFourMonthSixDayPrice() {
		return fourMonthSixDayPrice;
	}
	public void setFourMonthSixDayPrice(float fourMonthSixDayPrice) {
		this.fourMonthSixDayPrice = fourMonthSixDayPrice;
	}
	public float getThreeMonthSixDayPrice() {
		return threeMonthSixDayPrice;
	}
	public void setThreeMonthSixDayPrice(float threeMonthSixDayPrice) {
		this.threeMonthSixDayPrice = threeMonthSixDayPrice;
	}
	public float getFiveMonthSixDayPrice() {
		return fiveMonthSixDayPrice;
	}
	public void setFiveMonthSixDayPrice(float fiveMonthSixDayPrice) {
		this.fiveMonthSixDayPrice = fiveMonthSixDayPrice;
	}
	public float getSixMonthSixDayPrice() {
		return sixMonthSixDayPrice;
	}
	public void setSixMonthSixDayPrice(float sixMonthSixDayPrice) {
		this.sixMonthSixDayPrice = sixMonthSixDayPrice;
	}
	public float getTwoWeekFiveDayPrice() {
		return twoWeekFiveDayPrice;
	}
	public void setTwoWeekFiveDayPrice(float twoWeekFiveDayPrice) {
		this.twoWeekFiveDayPrice = twoWeekFiveDayPrice;
	}
	public float getTwoWeekSixDayPrice() {
		return twoWeekSixDayPrice;
	}
	public void setTwoWeekSixDayPrice(float twoWeekSixDayPrice) {
		this.twoWeekSixDayPrice = twoWeekSixDayPrice;
	}
	
}
