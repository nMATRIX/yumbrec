package com.base.yumbrec.model;

public class MealType {
	 
    private int id;
    private String mealTypDesc;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMealTypDesc() {
		return mealTypDesc;
	}
	public void setMealTypDesc(String mealTypDesc) {
		this.mealTypDesc = mealTypDesc;
	}


}
