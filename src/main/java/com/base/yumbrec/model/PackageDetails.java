package com.base.yumbrec.model;

public class PackageDetails {

	private String date;
	private String pkgImage;
	private String packageMenuDesc;
	private Address address;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPkgImage() {
		return pkgImage;
	}
	public void setPkgImage(String pkgImage) {
		this.pkgImage = pkgImage;
	}
	public String getPackageMenuDesc() {
		return packageMenuDesc;
	}
	public void setPackageMenuDesc(String packageMenuDesc) {
		this.packageMenuDesc = packageMenuDesc;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "PackageDetails [date=" + date + ", pkgImage=" + pkgImage + ", packageMenuDesc=" + packageMenuDesc
				+ ", address=" + address + "]";
	}
	
	
	
}
