package com.base.yumbrec.model;

import java.io.Serializable;

public class ListOfPackages implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private PackageDetails packageDetails;
	private int key;
	private String mealCutOffTime;
	private MealTime mealTime;
	private Price price;
	private FoodType foodType;
	private int packageId;
	private MealType mealType;
	private WeekType weekType;
	private Tax tax;
	private int packageStockCount;
	private String packageName;
	private MealPlan mealPlan;

	public PackageDetails getPackageDetails() {
		return packageDetails;
	}
	public void setPackageDetails(PackageDetails packageDetails) {
		this.packageDetails = packageDetails;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public String getMealCutOffTime() {
		return mealCutOffTime;
	}
	public void setMealCutOffTime(String mealCutOffTime) {
		this.mealCutOffTime = mealCutOffTime;
	}
	public MealTime getMealTime() {
		return mealTime;
	}
	public void setMealTime(MealTime mealTime) {
		this.mealTime = mealTime;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}
	public FoodType getFoodType() {
		return foodType;
	}
	public void setFoodType(FoodType foodType) {
		this.foodType = foodType;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public MealType getMealType() {
		return mealType;
	}
	public void setMealType(MealType mealType) {
		this.mealType = mealType;
	}
	public WeekType getWeekType() {
		return weekType;
	}
	public void setWeekType(WeekType weekType) {
		this.weekType = weekType;
	}
	public Tax getTax() {
		return tax;
	}
	public void setTax(Tax tax) {
		this.tax = tax;
	}
	public int getPackageStockCount() {
		return packageStockCount;
	}
	public void setPackageStockCount(int packageStockCount) {
		this.packageStockCount = packageStockCount;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public MealPlan getMealPlan() {
		return mealPlan;
	}
	public void setMealPlan(MealPlan mealPlan) {
		this.mealPlan = mealPlan;
	}
	@Override
	public String toString() {
		return "ListOfPackages [packageDetails=" + packageDetails + ", key=" + key + ", mealCutOffTime="
				+ mealCutOffTime + ", mealTime=" + mealTime + ", price=" + price + ", foodType=" + foodType
				+ ", packageId=" + packageId + ", mealType=" + mealType + ", weekType=" + weekType + ", tax=" + tax
				+ ", packageStockCount=" + packageStockCount + ", packageName=" + packageName + ", mealPlan=" + mealPlan
				+ "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
