package com.base.yumbrec.model;

public class DeliveryCharges {
	
	private int id;
	private String delvDesc;
	private String delvChrg;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDelvDesc() {
		return delvDesc;
	}
	public void setDelvDesc(String delvDesc) {
		this.delvDesc = delvDesc;
	}
	public String getDelvChrg() {
		return delvChrg;
	}
	public void setDelvChrg(String delvChrg) {
		this.delvChrg = delvChrg;
	}
	@Override
	public String toString() {
		return "DeliveryCharges [id=" + id + ", delvDesc=" + delvDesc + ", delvChrg=" + delvChrg + "]";
	}
	

}
