package com.base.yumbrec.model;

import java.util.List;

public class OrderDetails {

	private int pkgDurId;
	private String startDate;
	private String pkgDurdesc;
	private String endDate;
	private User user;
	private float delChrg;
	private float cupnDiscAmnt;
	private int payModId;
	private float subAmnt;
	private int cupnId;
	private float taxAmnt;
	private float grndTot;
	private float redeemCancelAmnt;
	private List<PackageDetails> packageDetails;
	private String transactionId;
	
	private float subGrandTotal;

	public int getPkgDurId() {
		return pkgDurId;
	}
	public void setPkgDurId(int pkgDurId) {
		this.pkgDurId = pkgDurId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getPkgDurdesc() {
		return pkgDurdesc;
	}
	public void setPkgDurdesc(String pkgDurdesc) {
		this.pkgDurdesc = pkgDurdesc;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public float getDelChrg() {
		return delChrg;
	}
	public void setDelChrg(float delChrg) {
		this.delChrg = delChrg;
	}
	public float getCupnDiscAmnt() {
		return cupnDiscAmnt;
	}
	public void setCupnDiscAmnt(float cupnDiscAmnt) {
		this.cupnDiscAmnt = cupnDiscAmnt;
	}
	public int getPayModId() {
		return payModId;
	}
	public void setPayModId(int payModId) {
		this.payModId = payModId;
	}
	public float getSubAmnt() {
		return subAmnt;
	}
	public void setSubAmnt(float subAmnt) {
		this.subAmnt = subAmnt;
	}
	public int getCupnId() {
		return cupnId;
	}
	public void setCupnId(int cupnId) {
		this.cupnId = cupnId;
	}
	public float getTaxAmnt() {
		return taxAmnt;
	}
	public void setTaxAmnt(float taxAmnt) {
		this.taxAmnt = taxAmnt;
	}
	public float getGrndTot() {
		return grndTot;
	}
	public void setGrndTot(float grndTot) {
		this.grndTot = grndTot;
	}
	public float getRedeemCancelAmnt() {
		return redeemCancelAmnt;
	}
	public void setRedeemCancelAmnt(float redeemCancelAmnt) {
		this.redeemCancelAmnt = redeemCancelAmnt;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public float getSubGrandTotal() {
		return subGrandTotal;
	}
	public void setSubGrandTotal(float subGrandTotal) {
		this.subGrandTotal = subGrandTotal;
	}
	public List<PackageDetails> getPackageDetails() {
		return packageDetails;
	}
	public void setPackageDetails(List<PackageDetails> packageDetails) {
		this.packageDetails = packageDetails;
	}
	
	
}
