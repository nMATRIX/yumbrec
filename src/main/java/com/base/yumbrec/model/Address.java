package com.base.yumbrec.model;

public class Address {
	private String houseNo;
	private String laneDesc;
	private String landMrk;
	private int	actAddrFlg;
	private int	addrflg;
	private int	addrTypeId;
	private int	id;
	private PincodeDetails pincode;
	private State state;
	private Area area;
	private City city;
	
	@Override
	public String toString() {
		return "Address [houseNo=" + houseNo + ", laneDesc=" + laneDesc + ", landMrk=" + landMrk + ", actAddrFlg="
				+ actAddrFlg + ", addrflg=" + addrflg + ", addrTypeId=" + addrTypeId + ", id=" + id + ", pincode="
				+ pincode + ", state=" + state + ", area=" + area + ", city=" + city + "]";
	}
	public String getHouseNo() {
		return houseNo;
	}
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	public String getLaneDesc() {
		return laneDesc;
	}
	public void setLaneDesc(String laneDesc) {
		this.laneDesc = laneDesc;
	}
	public String getLandMrk() {
		return landMrk;
	}
	public void setLandMrk(String landMrk) {
		this.landMrk = landMrk;
	}
	public int getActAddrFlg() {
		return actAddrFlg;
	}
	public void setActAddrFlg(int actAddrFlg) {
		this.actAddrFlg = actAddrFlg;
	}
	public int getAddrflg() {
		return addrflg;
	}
	public void setAddrflg(int addrflg) {
		this.addrflg = addrflg;
	}
	public int getAddrTypeId() {
		return addrTypeId;
	}
	public void setAddrTypeId(int addrTypeId) {
		this.addrTypeId = addrTypeId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PincodeDetails getPincode() {
		return pincode;
	}
	public void setPincode(PincodeDetails pincode) {
		this.pincode = pincode;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	


}