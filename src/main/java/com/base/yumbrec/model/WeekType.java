package com.base.yumbrec.model;

public class WeekType {
private int 	id;
private String  weekTypDesc;
private int  weekTypF;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getWeekTypDesc() {
	return weekTypDesc;
}
public void setWeekTypDesc(String weekTypDesc) {
	this.weekTypDesc = weekTypDesc;
}
public int getWeekTypF() {
	return weekTypF;
}
public void setWeekTypF(int weekTypF) {
	this.weekTypF = weekTypF;
}


}
