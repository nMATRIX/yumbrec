package com.base.yumbrec.model;

public class PincodeDetails {
	private int id;
	private String pincodeDesc;
	private int areaId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPincodeDesc() {
		return pincodeDesc;
	}
	public void setPincodeDesc(String pincodeDesc) {
		this.pincodeDesc = pincodeDesc;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	

}
