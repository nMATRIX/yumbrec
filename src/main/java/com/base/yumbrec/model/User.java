package com.base.yumbrec.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class User
{
private int userId;
private String userFirstName;
private String userLastName;
private String userContactNumber;
private String confirmPassword;
@Email
@NotEmpty
private String userEmailId;
private String userName;
@NotEmpty
private String userPassword;
private String errorMessage;
private String successMessage;
private String oldPassword;
private PincodeDetails pincodeDetails;
private City city;
private String houseDescription;
private String landmark;
private Area area;
private String addressType;
private int addressTypeId;
private State state;
private int  addressTypeDesc;
private String addressMealTimeAndDate;
private int paymode; 
private String failurekey;
private String successkey;
private SubArea subArea;


public int getAddressTypeDesc() 
{
	return addressTypeDesc;
}
public void setAddressTypeDesc(int addressTypeDesc) 
{
	this.addressTypeDesc = addressTypeDesc;
}
public String getAddressMealTimeAndDate() 
{
	return addressMealTimeAndDate;
}
public void setAddressMealTimeAndDate(String addressMealTimeAndDate) 
{
	this.addressMealTimeAndDate = addressMealTimeAndDate;
}
public PincodeDetails getPincodeDetails() 
{
	return pincodeDetails;
}
public void setPincodeDetails(PincodeDetails pincodeDetails) 
{
	this.pincodeDetails = pincodeDetails;
}
public City getCity() 
{
	return city;
}
public void setCity(City city) 
{
	this.city = city;
}
public String getHouseDescription()
{
	return houseDescription;
}
public void setHouseDescription(String houseDescription)
{
	this.houseDescription = houseDescription;
}
public String getLandmark()
{
	return landmark;
}
public void setLandmark(String landmark) 
{
	this.landmark = landmark;
}
public int getUserId() 
{
	return userId;
}
public void setUserId(int userId) 
{
	this.userId = userId;
}
public String getUserContactNumber() {
	return userContactNumber;
}
public void setUserContactNumber(String userContactNumber) {
	this.userContactNumber = userContactNumber;
}
public String getUserEmailId() {
	return userEmailId;
}
public void setUserEmailId(String userEmailId) {
	this.userEmailId = userEmailId;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getUserPassword() {
	return userPassword;
}
public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
}
public String getErrorMessage() {
	return errorMessage;
}
public void setErrorMessage(String errorMessage) {
	this.errorMessage = errorMessage;
}
public String getSuccessMessage() {
	return successMessage;
}
public void setSuccessMessage(String successMessage) {
	this.successMessage = successMessage;
}
public String getOldPassword() {
	return oldPassword;
}
public void setOldPassword(String oldPassword) {
	this.oldPassword = oldPassword;
}
public Area getArea() {
	return area;
}
public void setArea(Area area) {
	this.area = area;
}
public String getAddressType() {
	return addressType;
}
public void setAddressType(String addressType) {
	this.addressType = addressType;
}
public State getState() {
	return state;
}
public void setState(State state) {
	this.state = state;
}
public int getAddressTypeId() {
	return addressTypeId;
}
public void setAddressTypeId(int addressTypeId) {
	this.addressTypeId = addressTypeId;
}
public int getPaymode() {
	return paymode;
}
public void setPaymode(int paymode) {
	this.paymode = paymode;
}
@Override
public String toString() {
	return "User [userId=" + userId + ", userContactNumber=" + userContactNumber + ", userEmailId=" + userEmailId
			+ ", userName=" + userName + ", userPassword=" + userPassword + ", errorMessage=" + errorMessage
			+ ", successMessage=" + successMessage + ", oldPassword=" + oldPassword + ", pincodeDetails="
			+ pincodeDetails + ", city=" + city + ", houseDescription=" + houseDescription + ", landmark=" + landmark
			+ ", area=" + area + ", addressType=" + addressType + ", addressTypeId=" + addressTypeId + ", state="
			+ state + ", addressTypeDesc=" + addressTypeDesc + ", addressMealTimeAndDate=" + addressMealTimeAndDate
			+ ", paymode=" + paymode + "]";
}
public String getUserLastName() {
	return userLastName;
}
public void setUserLastName(String userLastName) {
	this.userLastName = userLastName;
}
public String getUserFirstName() {
	return userFirstName;
}
public void setUserFirstName(String userFirstName) {
	this.userFirstName = userFirstName;
}
public String getFailurekey() {
	return failurekey;
}
public void setFailurekey(String failurekey) {
	this.failurekey = failurekey;
}
public String getSuccesskey() {
	return successkey;
}
public void setSuccesskey(String successkey) {
	this.successkey = successkey;
}
public SubArea getSubArea() {
	return subArea;
}
public void setSubArea(SubArea subArea) {
	this.subArea = subArea;
}
public String getConfirmPassword() {
	return confirmPassword;
}
public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
}


}
