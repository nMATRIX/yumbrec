package com.base.yumbrec.model;

import java.util.List;

public class AllAreaDetails 
{

	private List<Area> areaList;
	private List<State> stateList;
	private List<City> cityList;
	private List<PincodeDetails> Pincode;
	
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	public List<State> getStateList() {
		return stateList;
	}
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	public List<PincodeDetails> getPincode() {
		return Pincode;
	}
	public void setPincode(List<PincodeDetails> pincode) {
		Pincode = pincode;
	}
	
	
}
