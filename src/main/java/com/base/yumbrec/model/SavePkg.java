package com.base.yumbrec.model;

public class SavePkg {
	private String pkgDurId;
	private String pkgId;
	private String pkgQunty;
	private String packageAmount;
	private String addressId;
	private String mealTimeId;
	private String ordStDt;
	private String ordEndDt;
	private String foodTypId;
	private String weekDur;
	private String mealTypeId;
	public String getPkgDurId() {
		return pkgDurId;
	}
	public void setPkgDurId(String pkgDurId) {
		this.pkgDurId = pkgDurId;
	}
	public String getPkgId() {
		return pkgId;
	}
	public void setPkgId(String pkgId) {
		this.pkgId = pkgId;
	}
	public String getPkgQunty() {
		return pkgQunty;
	}
	public void setPkgQunty(String pkgQunty) {
		this.pkgQunty = pkgQunty;
	}
	public String getPackageAmount() {
		return packageAmount;
	}
	public void setPackageAmount(String packageAmount) {
		this.packageAmount = packageAmount;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getMealTimeId() {
		return mealTimeId;
	}
	public void setMealTimeId(String mealTimeId) {
		this.mealTimeId = mealTimeId;
	}
	public String getOrdStDt() {
		return ordStDt;
	}
	public void setOrdStDt(String ordStDt) {
		this.ordStDt = ordStDt;
	}
	public String getOrdEndDt() {
		return ordEndDt;
	}
	public void setOrdEndDt(String ordEndDt) {
		this.ordEndDt = ordEndDt;
	}
	public String getFoodTypId() {
		return foodTypId;
	}
	public void setFoodTypId(String foodTypId) {
		this.foodTypId = foodTypId;
	}
	public String getWeekDur() {
		return weekDur;
	}
	public void setWeekDur(String weekDur) {
		this.weekDur = weekDur;
	}
	public String getMealTypeId() {
		return mealTypeId;
	}
	public void setMealTypeId(String mealTypeId) {
		this.mealTypeId = mealTypeId;
	}
	@Override
	public String toString() {
		return "SavePkg [pkgDurId=" + pkgDurId + ", pkgId=" + pkgId + ", pkgQunty=" + pkgQunty + ", packageAmount="
				+ packageAmount + ", addressId=" + addressId + ", mealTimeId=" + mealTimeId + ", ordStDt=" + ordStDt
				+ ", ordEndDt=" + ordEndDt + ", foodTypId=" + foodTypId + ", weekDur=" + weekDur + ", mealTypeId="
				+ mealTypeId + "]";
	}
	


}
