package com.base.yumbrec.model;

import java.util.List;

public class BillWiseList {
	private int subGrandTotal;
	private int subAmount;
	private int redeemCancelAmnt;
	private int deliveryChrges;
	private int grandTotal;
	private int billId;
	private int cupnDiscAmnt;
	private int taxAmount;
	private List<LunchOrders> lunchOrdersList;
	private List<DinnerOrders> dinnerOrdersList;
	private List<LunchOrders>  breakFastOrdersList;
	public int getSubGrandTotal() {
		return subGrandTotal;
	}
	public void setSubGrandTotal(int subGrandTotal) {
		this.subGrandTotal = subGrandTotal;
	}
	public int getSubAmount() {
		return subAmount;
	}
	public void setSubAmount(int subAmount) {
		this.subAmount = subAmount;
	}
	public int getRedeemCancelAmnt() {
		return redeemCancelAmnt;
	}
	public void setRedeemCancelAmnt(int redeemCancelAmnt) {
		this.redeemCancelAmnt = redeemCancelAmnt;
	}
	public int getDeliveryChrges() {
		return deliveryChrges;
	}
	public void setDeliveryChrges(int deliveryChrges) {
		this.deliveryChrges = deliveryChrges;
	}
	public int getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(int grandTotal) {
		this.grandTotal = grandTotal;
	}
	public int getBillId() {
		return billId;
	}
	public void setBillId(int billId) {
		this.billId = billId;
	}
	public int getCupnDiscAmnt() {
		return cupnDiscAmnt;
	}
	public void setCupnDiscAmnt(int cupnDiscAmnt) {
		this.cupnDiscAmnt = cupnDiscAmnt;
	}
	public int getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(int taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	
	
	public List<LunchOrders> getLunchOrdersList() {
		return lunchOrdersList;
	}
	public void setLunchOrdersList(List<LunchOrders> lunchOrdersList) {
		this.lunchOrdersList = lunchOrdersList;
	}
	public List<DinnerOrders> getDinnerOrdersList() {
		return dinnerOrdersList;
	}
	public void setDinnerOrdersList(List<DinnerOrders> dinnerOrdersList) {
		this.dinnerOrdersList = dinnerOrdersList;
	}
	
	public List<LunchOrders> getBreakFastOrdersList() {
		return breakFastOrdersList;
	}
	public void setBreakFastOrdersList(List<LunchOrders> breakFastOrdersList) {
		this.breakFastOrdersList = breakFastOrdersList;
	}
	
}
