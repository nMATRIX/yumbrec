package com.base.yumbrec.model;


public class PaymodeDetails {
private String status;
private String key;
private String txnid;
private String amount;
private  String productinfo;
private String firstname;
private  String lastname;
private String email;
private String Error;
private String phone;
private String hash;
private String PG_TYPE;
private String bank_ref_num;
private String payuMoneyId;
private String orderId;
private int paymodeId;
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getKey() {
	return key;
}
public void setKey(String key) {
	this.key = key;
}
public String getTxnid() {
	return txnid;
}
public void setTxnid(String txnid) {
	this.txnid = txnid;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getProductinfo() {
	return productinfo;
}
public void setProductinfo(String productinfo) {
	this.productinfo = productinfo;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getError() {
	return Error;
}
public void setError(String error) {
	Error = error;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getHash() {
	return hash;
}
public void setHash(String hash) {
	this.hash = hash;
}
public String getPG_TYPE() {
	return PG_TYPE;
}
public void setPG_TYPE(String pG_TYPE) {
	PG_TYPE = pG_TYPE;
}
public String getBank_ref_num() {
	return bank_ref_num;
}
public void setBank_ref_num(String bank_ref_num) {
	this.bank_ref_num = bank_ref_num;
}
public String getPayuMoneyId() {
	return payuMoneyId;
}
public void setPayuMoneyId(String payuMoneyId) {
	this.payuMoneyId = payuMoneyId;
}
public String getOrderId() {
	return orderId;
}
public void setOrderId(String orderId) {
	this.orderId = orderId;
}
public int getPaymodeId() {
	return paymodeId;
}
public void setPaymodeId(int paymodeId) {
	this.paymodeId = paymodeId;
}




}
