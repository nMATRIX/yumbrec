package com.base.yumbrec.model;

public class PackageDuration {

	private int id;
	private String pkgDurDesc;
	private int pkgDurF;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPkgDurDesc() {
		return pkgDurDesc;
	}
	public void setPkgDurDesc(String pkgDurDesc) {
		this.pkgDurDesc = pkgDurDesc;
	}
	public int getPkgDurF() {
		return pkgDurF;
	}
	public void setPkgDurF(int pkgDurF) {
		this.pkgDurF = pkgDurF;
	}
	
	
	
}
