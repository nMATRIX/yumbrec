package com.base.yumbrec.model;

public class SubArea 
{

	private int id;
	private String subAreaDesc;
	private int areaId;
	private int subAreaF;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubAreaDesc() {
		return subAreaDesc;
	}
	public void setSubAreaDesc(String subAreaDesc) {
		this.subAreaDesc = subAreaDesc;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getSubAreaF() {
		return subAreaF;
	}
	public void setSubAreaF(int subAreaF) {
		this.subAreaF = subAreaF;
	}
	@Override
	public String toString() {
		return "SubArea [id=" + id + ", subAreaDesc=" + subAreaDesc + ", areaId=" + areaId + ", subAreaF=" + subAreaF + "]";
	}
}
