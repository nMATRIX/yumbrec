package com.base.yumbrec.model;

public class LunchOrders {
	private String pkgName;
	private int pkgId;
	private int pkgAmount;
	private int pkgQuantity;
	private String weekTypDesc;
	private String orderStartDate;
	private String orderEndDate;
	private int orderId;
	private String pkgDurDescription;
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	public int getPkgId() {
		return pkgId;
	}
	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}
	public int getPkgAmount() {
		return pkgAmount;
	}
	public void setPkgAmount(int pkgAmount) {
		this.pkgAmount = pkgAmount;
	}
	public int getPkgQuantity() {
		return pkgQuantity;
	}
	public void setPkgQuantity(int pkgQuantity) {
		this.pkgQuantity = pkgQuantity;
	}
	public String getWeekTypDesc() {
		return weekTypDesc;
	}
	public void setWeekTypDesc(String weekTypDesc) {
		this.weekTypDesc = weekTypDesc;
	}
	public String getOrderStartDate() {
		return orderStartDate;
	}
	public void setOrderStartDate(String orderStartDate) {
		this.orderStartDate = orderStartDate;
	}
	public String getOrderEndDate() {
		return orderEndDate;
	}
	public void setOrderEndDate(String orderEndDate) {
		this.orderEndDate = orderEndDate;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getPkgDurDescription() {
		return pkgDurDescription;
	}
	public void setPkgDurDescription(String pkgDurDescription) {
		this.pkgDurDescription = pkgDurDescription;
	}
	@Override
	public String toString() {
		return "LunchOrders [pkgName=" + pkgName + ", pkgId=" + pkgId
				+ ", pkgAmount=" + pkgAmount + ", pkgQuantity=" + pkgQuantity
				+ ", weekTypDesc=" + weekTypDesc + ", orderStartDate="
				+ orderStartDate + ", orderEndDate=" + orderEndDate
				+ ", orderId=" + orderId + ", pkgDurDescription="
				+ pkgDurDescription + "]";
	}
	
}
