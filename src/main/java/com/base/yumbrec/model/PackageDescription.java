package com.base.yumbrec.model;

public class PackageDescription {
	private String date;
	private String	packageMenuDesc;
	private String	key;
	private String pkgImage;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPackageMenuDesc() {
		return packageMenuDesc;
	}
	public void setPackageMenuDesc(String packageMenuDesc) {
		this.packageMenuDesc = packageMenuDesc;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getPkgImage() {
		return pkgImage;
	}
	public void setPkgImage(String pkgImage) {
		this.pkgImage = pkgImage;
	}
}
