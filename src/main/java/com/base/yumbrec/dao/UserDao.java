package com.base.yumbrec.dao;

import org.springframework.http.ResponseEntity;

import com.base.yumbrec.model.User;

public interface UserDao 
{

	public String registerUser(User user);
	
	public String userLogin(User user);
	
	public String getAddessBasedOnUserId(User userForAddress);
	
	public String forgotUserPassword(User user);
	
	public String changePassword(User user);
	
	public ResponseEntity<String> getPackageDetails();
	
	public String getMealTimeDetails();
	
	public String getMealDurationList();
	
	public String getWeekType();
	
	public ResponseEntity<String> getAddressDetails();
	
	public String updateUserAddress(User user);

}
