package com.base.yumbrec.dao;

import org.springframework.http.ResponseEntity;

import com.base.yumbrec.model.Orders;
import com.base.yumbrec.model.PaymodeDetails;
import com.base.yumbrec.model.User;

public interface OrderDao 
{

	public ResponseEntity<String> getPackageDetails();
	
	public String getAddessBasedOnUserId(User userForAddress);
	
	public ResponseEntity<String> getAddressDetails();
	
	public ResponseEntity<String> deliveryChargesList();
	
	public ResponseEntity<String> gettaxList();
	
	public String orderSave(Orders orders);
	
	public String updatePaymodeId(PaymodeDetails paymodeDetails);
	
	public String getMyOrders(int userId,int paidId);
	
	public String getAreaDetails();
	
	//public String getSubAreas(String areaId);
	
	//public String getAddressList(String userId);
	
	//public String getPincodeList();
	
	 public String addresSave(User user);
	
	//public String onedayorderArea();
	
	
	
	
}
