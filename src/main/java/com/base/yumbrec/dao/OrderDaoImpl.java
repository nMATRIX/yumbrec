package com.base.yumbrec.dao;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.base.yumbrec.model.Orders;
import com.base.yumbrec.model.PaymodeDetails;
import com.base.yumbrec.model.User;
import com.base.yumbrec.util.ApplicationMediaType;
import com.base.yumbrec.util.ProjectDetails;

public class OrderDaoImpl implements OrderDao 
{

	@Autowired
	RestTemplate restTemplate;

	@Override
	public ResponseEntity<String> getPackageDetails()
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ALLPACKAGEDETAILS, String.class); 
		 System.out.println("getting all package details from web services here ===="+result);   
		 return result;
	}

	@Override
	public String getAddessBasedOnUserId(User userForAddress) 
	{
		  JSONObject inputData = new JSONObject();
	      try 
	      {
	       if(userForAddress.getUserId()!=0)
	        {
	         inputData.put("id", userForAddress.getUserId());
	        }
	      }
	      catch (JSONException e) 
	      {
	        e.printStackTrace();
	      }
	      String inputDataInString=inputData.toString();
	      System.out.println("address for user from order dao in getadressbasedonuser id  "+inputDataInString);
	      ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	      @SuppressWarnings("unchecked")
		  HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	      String result=restTemplate.postForObject(ProjectDetails.ADDRESSDETAILSBASEDONUSER,  entity, String.class);
	      System.out.println("result for user from order dao in getadressbasedonuser id   "+result); 
	  return result;
	}

	@Override
	public ResponseEntity<String> getAddressDetails() 
	{
		ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ADDRESSLIST, String.class); 
	    System.out.println("address list from orderdao in getaddressdetaisl "+result);
	 return result;
	}

	@Override
	public ResponseEntity<String> deliveryChargesList() 
	{
		   ResponseEntity<String> string=restTemplate.getForEntity(ProjectDetails.DELIVERYCHARGEDETAILS, String.class);
	    return string;
	}

	@Override
	public ResponseEntity<String> gettaxList() 
	{
          ResponseEntity<String> string=restTemplate.getForEntity(ProjectDetails.TAXDETAILS, String.class);
	    return string;
	}

	@Override
	public String orderSave(Orders orders) 
	{
		JSONObject inputData = new JSONObject();
		JSONObject mainObject = new JSONObject();
	    try
	    {
			inputData.put("customerId", orders.getCustomerId());
			inputData.put("delChrg", orders.getDelChrg());
			inputData.put("cupnDiscAmnt", orders.getCupnDiscAmnt());
			inputData.put("payModId", orders.getPayModId());
			inputData.put("subAmnt", orders.getSubAmnt());
			inputData.put("cupnId", orders.getCupnId());
			inputData.put("taxAmnt", orders.getTaxAmnt());
			inputData.put("grndTot", orders.getGrndTot());
			inputData.put("redeemCancelAmnt", orders.getRedeemCancelAmnt());
			inputData.put("transactionId", orders.getTransactionId());
			inputData.put("subGrandTotal", orders.getSubGrandTotal());
			inputData.put("savePkg", orders.getSavePkgs());
		} 
	    catch (JSONException e) 
	    {
			e.printStackTrace();
		}
	    mainObject.put("orders", inputData);
	    String inputDataInString=mainObject.toString();
	    System.out.println("inpudata"+inputDataInString);
	    ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	    HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	    String result=restTemplate.postForObject(ProjectDetails.ORDERSAVE,  entity, String.class);	
	    System.out.println("save order result is "+ result );
	  return result;
	}


	@Override
	public String updatePaymodeId(PaymodeDetails paymodeDetails) 
	{
		JSONObject inputData = new JSONObject();
	    try
	    {
			inputData.put("paymodeId", paymodeDetails.getPaymodeId());
			inputData.put("transactionId", paymodeDetails.getTxnid());
			inputData.put("custBillId", paymodeDetails.getOrderId());
		} 
	    catch (JSONException e) 
	    {
			e.printStackTrace();
		}
//	    mainObject.put("orders", inputData);
	    String inputDataInString=inputData.toString();
	    System.out.println("pay mode inpudata "+inputDataInString);
	    ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	    @SuppressWarnings("unchecked")
		HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	    String result=restTemplate.postForObject(ProjectDetails.UPDATEPAYMODE,  entity, String.class);	
	    System.out.println("pay mode result is "+ result );
	  return result;
	}

	@Override
	public String getMyOrders(int userId, int paidId) 
	{
		JSONObject inputData = new JSONObject();
	    try
	    {
	         inputData.put("custId", userId);
	         inputData.put("orderStatusId", paidId);
	    } 
	    catch (JSONException e)
	    {
	     e.printStackTrace();
	    }
	    String inputDataInString=inputData.toString();
	    ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	    System.out.println(inputData+"inputdata");
	    String result=restTemplate.postForObject(ProjectDetails.MYORDERS,  entity, String.class);
	    System.out.println("result from order dao  for get my oders "+result); 
	  return result;
	}

	@Override
	public String getAreaDetails() 
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ADDRESSLIST, String.class); 
		 System.out.println(result);   
		 return result.getBody();
	}

	@Override
	public String addresSave(User user) 
	{
		
		//System.out.println("user.getAddressTypeId() "+user.getAddressTypeId());
		
		JSONObject inputData = new JSONObject();
		System.out.println("user details from adress save "+user);
		inputData.put("pincodeId", user.getPincodeDetails().getPincodeDesc());
		inputData.put("addrFlag","1");
		inputData.put("custDetId", user.getUserId());
		inputData.put("stateId","1");
		inputData.put("cityId", user.getCity().getId());
		inputData.put("areaId", user.getArea().getId());
		inputData.put("addrTypeId", user.getAddressTypeId());
		inputData.put("laneDesc", user.getAddressTypeDesc());
		inputData.put("landMrk", user.getLandmark());
		inputData.put("houseNo", user.getHouseDescription());
		//inputData.put("subAreaId", user.getSubarea().getId());
		String inputDataInString=inputData.toString();
		System.out.println("inputDataInString for addrs"+inputDataInString);
		ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
		@SuppressWarnings("unchecked")
		HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
		String result=restTemplate.postForObject(ProjectDetails.UPADTEUSERADDRESS,  entity, String.class);
		
		System.out.println("Address saved List: from order dao on address save method "+result);
		return result;
	}
}
