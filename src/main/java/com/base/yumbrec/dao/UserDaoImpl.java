package com.base.yumbrec.dao;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.base.yumbrec.model.User;
import com.base.yumbrec.util.ApplicationMediaType;
import com.base.yumbrec.util.ProjectDetails;
import com.google.gson.JsonObject;

public class UserDaoImpl implements UserDao
{
	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public String registerUser(User user) 
	{
		JSONObject inputData = new JSONObject();
		System.out.println("first name is "+user.getUserFirstName());
		try 
		{
			inputData.put("firstname",user.getUserFirstName());
			inputData.put("lastname", user.getUserLastName());
			inputData.put("password", user.getUserPassword());
			inputData.put("mobile", user.getUserContactNumber());
			inputData.put("email", user.getUserEmailId());
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		System.out.println("Json input data details : "+inputData);
		String inputDataInString=inputData.toString();
		System.out.println("input data string:"+inputDataInString);
		ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	    String result=restTemplate.postForObject(ProjectDetails.USERREGISTERDETAILS,  entity, String.class);
	    System.out.println("result from database in dao for register user  >>>>"+ result);	
	    return result;
	}

	@Override
	public String userLogin(User user)
	{
		JSONObject inputData = new JSONObject();
	    try 
	    {
	    if(user.getUserContactNumber()!=null)
	    {
	     System.err.println("contact number: "+user.getUserContactNumber());
	     inputData.put("mobile", user.getUserContactNumber());
	    }
	    else if(user.getUserEmailId()!=null)
	    {
	     System.err.println("email: from dao "+user.getUserEmailId());
	     inputData.put("email", user.getUserEmailId());
	    }
	    inputData.put("password", user.getUserPassword());
	    inputData.put("guestFlag", "0");
	    }
	    catch (JSONException e) 
	    {
	     e.printStackTrace();
	    }
	    String inputDataInString=inputData.toString();
	    ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	    HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	    System.out.println(inputData+"inputdata");
	    String result=restTemplate.postForObject(ProjectDetails.USERLOGINDETAILS,  entity, String.class);
	    System.out.println("userLogin result from dao "+result); 

	    return result;
	}

	@Override
	public String getAddessBasedOnUserId(User userForAddress)
	{
		JSONObject inputData = new JSONObject();
	    try 
	    {
	      if(userForAddress.getUserId()!=0)
	       {
	         inputData.put("id", userForAddress.getUserId());
	       }
	   } 
	   catch (JSONException e)
	   {
	    e.printStackTrace();
	   }
	   String inputDataInString=inputData.toString();
	   System.out.println("address for user from user dao "+inputDataInString);
	   ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	   HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	   String result=restTemplate.postForObject(ProjectDetails.ADDRESSDETAILSBASEDONUSER,  entity, String.class);
	   System.out.println("result from userdao for address with id "+result); 
	   return result;
	}
    
	
	/*public ResponseEntity<String> getAddressDetails() 
    {
 	 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ADDRESSLIST, String.class); 
	    System.out.println("area: "+result);
	 return result;	
	}*/

	@Override
	public String forgotUserPassword(User user) 
	{
		 JSONObject inputData = new JSONObject();
		 try 
		 {
		   if(user.getUserContactNumber()!=null)
		    {
			   inputData.put("mobile", user.getUserContactNumber());
		    } 
		   else if(user.getUserEmailId()!=null)
		    {
			   inputData.put("email", user.getUserEmailId());
		    }
		 } 
		 catch (JSONException e) 
		 {
			e.printStackTrace();
		 }
		 String inputDataInString=inputData.toString();
		 ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
		 HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
		 System.out.println(inputData+"inputdata");
		 String result=restTemplate.postForObject(ProjectDetails.FORGOTUSERPAASWORD,  entity, String.class);
		 System.out.println("result "+result);	
		return result;
	}

	@Override
	public String changePassword(User user)
	{
		JSONObject inputdata=new JSONObject();
		try
		{
			if(user.getUserId()!=0)
			{
				inputdata.put("userId", user.getUserId());
				inputdata.put("password", user.getOldPassword());
				inputdata.put("newPassword", user.getUserPassword());
			}
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
		String inputdataString=inputdata.toString();
		ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
		HttpEntity entity=new HttpEntity(inputdataString,applicationMediaType.getApplictionType());
		String result=restTemplate.postForObject(ProjectDetails.CHANGEUSERPASSWORD, entity,String.class);
		System.out.println("result for change password from dao "+result);
				
		return result;
	}

	@Override
	public ResponseEntity<String> getPackageDetails() 
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ALLPACKAGEDETAILS, String.class); 
		 return result;
	}

	@Override
	public String getMealTimeDetails() 
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.MEALTIMEDETAILS, String.class); 
		 return result.getBody();
	}

	@Override
	public String getMealDurationList() 
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.MEALDURATIONDETAILS, String.class); 
		 return result.getBody();
	}

	@Override
	public String getWeekType() 
	{ 
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.WEEKTYPEDETAILS, String.class); 
		 return result.getBody();
	}

	@Override
	public ResponseEntity<String> getAddressDetails() 
	{
		 ResponseEntity<String> result=   restTemplate.getForEntity(ProjectDetails.ADDRESSLIST, String.class); 
		 System.out.println("area:"+result);
		 return result;
	}

	@Override
	public String updateUserAddress(User user) 
	{
		 System.out.println("User: "+user);
		 JSONObject inputData = new JSONObject();
	     try 
	     {
	       if(user.getUserId()!=0)
	       {
	         inputData.put("laneDesc","");
	         inputData.put("areaId",1);
	         inputData.put("cityId", 1);
	         inputData.put("stateId",1);
	         inputData.put("custDetId",user.getUserId());
	         inputData.put("addrFlag",1);
	         inputData.put("pincodeId",1);
	         inputData.put("addrTypeId",3);
	         inputData.put("houseNo","");
	         inputData.put("landMrk", user.getLandmark());
	        }
	    } 
	    catch (JSONException e) 
	     {
	    e.printStackTrace();
	     }
	      System.out.println("input data update: from userdao"+inputData);
	      String inputDataInString=inputData.toString();
	      ApplicationMediaType applicationMediaType=ApplicationMediaType.getInstance();
	      HttpEntity entity = new HttpEntity(inputDataInString,applicationMediaType.getApplictionType());
	      String result=restTemplate.postForObject(ProjectDetails.UPADTEUSERADDRESS,  entity, String.class);
	      System.out.println("result from userdao :"+result); 
	  return result;
	}
	
	
 }
