package com.base.yumbrec;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.base.yumbrec.model.Address;
import com.base.yumbrec.model.AllAreaDetails;
import com.base.yumbrec.model.Area;
import com.base.yumbrec.model.CartOrder;
import com.base.yumbrec.model.City;
import com.base.yumbrec.model.ListOfPackages;
import com.base.yumbrec.model.MealTime;
import com.base.yumbrec.model.PackageDescription;
import com.base.yumbrec.model.PackageDetails;
import com.base.yumbrec.model.PackageDuration;
import com.base.yumbrec.model.PincodeDetails;
import com.base.yumbrec.model.SubArea;
import com.base.yumbrec.model.Tax;
import com.base.yumbrec.model.User;
import com.base.yumbrec.model.WeekType;
import com.base.yumbrec.service.OrderService;
import com.base.yumbrec.service.UserService;
import com.base.yumbrec.util.CalculateEndDate;
import com.google.gson.Gson;


@Controller
public class HomeController
{
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	OrderService orderService;
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public  ModelAndView home(Locale locale) 
	{
		//System.out.println("Welcome to home controller");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		Map<String,Object> model=orderService.getPackageDetails();
		session.setAttribute("packageDetails", model.get("allpackageDetails"));
		
		return new ModelAndView("index","model",model);
	}
	
	@RequestMapping(value="/aboutUs",method=RequestMethod.GET)
	public ModelAndView aboutUs()
	{
		return new ModelAndView("aboutus");
	}
	
	
	@RequestMapping(value="/newregister",method=RequestMethod.GET)
	public ModelAndView login()
	{
		return new ModelAndView("login");
	}
	
	@RequestMapping(value="/contactus",method=RequestMethod.GET)
	public ModelAndView contactus() 
	{
	    return new ModelAndView("contactus");	
	}

	@RequestMapping(value = "/checkOrderDate", method = RequestMethod.POST) 
	public @ResponseBody String checkOrderDate(@RequestParam("ordDateId") String ordDateId,@RequestParam("mealTimeId") String mealTimeId) 
	{
	   return  String.valueOf(userService.getcutoffTimeBasedonId(mealTimeId, ordDateId));
	}
	
	
	
	public boolean timevalidator(String cutofftime)
    {
        DateFormat dateintimeformat=new SimpleDateFormat("h:mm a");
        SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
        String onlytime = sdf4.format(new Date());
        System.out.println("only time:"+onlytime);
        Date cutofftimeindate=new Date(),currenttime=new Date();
        try 
        {
            cutofftimeindate=dateintimeformat.parse(cutofftime);
            currenttime=dateintimeformat.parse(onlytime);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        //System.out.println("cut off time in date : from user service "+cutofftimeindate.getTime());
        return cutofftimeindate.getTime() - currenttime.getTime() > 0;
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addToCart", method = RequestMethod.POST)
	public @ResponseBody String addToCart(@RequestParam("packageId")String packageId,@RequestParam("ordDateId")String ordDate,
			@RequestParam("weekDaysId")String weekDaysId,@RequestParam("durationId")String durationId,@RequestParam("mealTimeId")String mealTimeId) throws ParseException 
	{
		//logger.info("::::::::::Home Controller: addToCart::::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		/*System.out.println("packageId:"+packageId);
		System.out.println("ordDate:"+ordDate);
		System.out.println("weekDaysId:"+weekDaysId);
		System.out.println("durationId:"+durationId);
		System.out.println("mealTimeId:"+mealTimeId);*/
		
		String packageDescrption=null;
		String mealDuration=null;
		String mealTimeDesc=null;
		String startDate=null;
		String weekDays=null;
		String priceofpackage=null;
		
		/*boolean cutoffTimebaseonId=false;
		cutoffTimebaseonId=userService.getcutoffTimeBasedonId(mealTimeId,ordDate);
		if (cutoffTimebaseonId) 
		{*/
		HttpSession session = request.getRequest().getSession();
		Map<String,Object> model=orderService.getPackageDetails();
		//System.out.println("mealTime Id is "+ mealTimeId);
		List<ListOfPackages> allpackageDetails =(List<ListOfPackages>)model.get("allpackageDetails");
		
		//System.out.println("package size "+allpackageDetails.size()); 
		
	    session.setAttribute("allpackageDetails", allpackageDetails);
	    
		if(packageId!=null && ordDate!=null && weekDaysId!=null && durationId!=null && mealTimeId!=null)
		 {
		     int count=0;
			 List<CartOrder> cartDetails=new ArrayList<CartOrder>();
			 cartDetails=(List<CartOrder>) session.getAttribute("cartDetails");
				
					  if(cartDetails!=null && cartDetails.size()>0 )
					  {
						  for(CartOrder cart:cartDetails)
						  {
							  if(cart.getMealTime().getId()==(Integer.parseInt(mealTimeId)))
							  {
								  count++;
							  }
						  }
					  }
					  if(cartDetails==null)
					  {
						  cartDetails=new ArrayList<CartOrder>();
					  }
					  if(count==0)
					  {
							 if(allpackageDetails!=null &&allpackageDetails.size()>0)
							 {
								 for(ListOfPackages cartOrder: allpackageDetails )
								 {
								 if(cartOrder.getPackageId()==(Integer.parseInt(packageId)))
								 {
									CartOrder newItemDetails=new CartOrder();
									int cartItemId=0;
									if(cartDetails==null)
									{
										cartItemId=0;
									}
									else
									{
										cartItemId=cartDetails.size()-1;
										//System.out.println("package Id in the Cart Items "+cartDetails);
									}
									newItemDetails.setMealTime(cartOrder.getMealTime());
									newItemDetails.setPrice(cartOrder.getPrice() );
									newItemDetails.setPackageName(cartOrder.getPackageName());
									//newItemDetails.setFoodType(cartOrder.getFoodType());
									newItemDetails.setPackageId(cartOrder.getPackageId());
									newItemDetails.setId(cartItemId);
									newItemDetails.setId(cartOrder.getPackageId());
									newItemDetails.setMealType(cartOrder.getMealType());
									
									
									MealTime mealTime=new MealTime();
									mealTime.setId((Integer.parseInt(mealTimeId)));
									if((Integer.parseInt(mealTimeId))==1)
									{
										mealTime.setMealTimeDesc("lunch");
									}
									else if((Integer.parseInt(mealTimeId))==2)
									{
										mealTime.setMealTimeDesc("dinner");
									}
									/*else if((Integer.parseInt(mealTimeId))==3)
									{
										mealTime.setMealTimeDesc("breakfast");
									}*/
									
									WeekType weekType=new WeekType();
									weekType.setId((Integer.parseInt(weekDaysId)));
									
									if((Integer.parseInt(weekDaysId))==1)
									{
										weekType.setWeekTypDesc("MON-FRY"); 
									}
									else if((Integer.parseInt(weekDaysId))==2)
									{
										weekType.setWeekTypDesc("MON-SAT"); 
									}
									else if((Integer.parseInt(weekDaysId))==0)
									{
										weekType.setWeekTypDesc("Daily"); 
									}
									CalculateEndDate calculateEndDate=new CalculateEndDate();
									String orderEndDate=null;
									float priceOfItem=0;
									
									PackageDuration packageDuration=new PackageDuration();
									// for one day
									if((Integer.parseInt(durationId))==1)
									{
										packageDuration.setId(Integer.parseInt(durationId));
										packageDuration.setPkgDurDesc("1 Day");
										weekType.setWeekTypDesc("1 Day"); 
										orderEndDate=ordDate;
										newItemDetails.setOrderStartDate(ordDate);
										//newItemDetails.setOrderEndDate(ordDate); 
										priceOfItem=newItemDetails.getPrice().getOneDayPrice();
									}
									else if((Integer.parseInt(durationId))==2)
									{
										// for 1 week and  5 day plan
										packageDuration.setId(Integer.parseInt(durationId));
										packageDuration.setPkgDurDesc("1 week");
										
										if((Integer.parseInt(weekDaysId))==1)
										{
											orderEndDate=calculateEndDate.endDate(ordDate, 6, 5);
											priceOfItem=newItemDetails.getPrice().getOneWeekFiveDayPrice();
										}
										else if((Integer.parseInt(weekDaysId))==2)
										{
											// for 1 week and  6 day plan
											orderEndDate=calculateEndDate.endDate(ordDate, 6, 6);
											priceOfItem=newItemDetails.getPrice().getOneWeekSixDayPrice();
										}
									}
									else if((Integer.parseInt(durationId))==3)
									{
										// for 2 weeks and  5 day plan
										packageDuration.setId(Integer.parseInt(durationId));
										packageDuration.setPkgDurDesc("2 weeks");
										
										if((Integer.parseInt(weekDaysId))==1)
										{
											orderEndDate=calculateEndDate.endDate(ordDate, 13, 5);
											priceOfItem=newItemDetails.getPrice().getTwoWeekFiveDayPrice();
											//System.out.println("newItemDetails.getPrice().getTwoWeeksFiveDayPrice(); "+newItemDetails.getPrice().getTwoWeekFiveDayPrice());
										}
										else if((Integer.parseInt(weekDaysId))==2)
										{
											// for 2 weeks and  6 day plan
											orderEndDate=calculateEndDate.endDate(ordDate, 13, 6);
											priceOfItem=newItemDetails.getPrice().getTwoWeekSixDayPrice();
											//System.out.println("newItemDetails.getPrice().getTwoWeeksSixDayPrice(); "+newItemDetails.getPrice().getTwoWeekSixDayPrice());
										}
										
									}
									else if((Integer.parseInt(durationId))==4)
									{
										packageDuration.setId(Integer.parseInt(durationId));
										packageDuration.setPkgDurDesc("1 month");
										
										// for 60 days and  5 day plan
										if((Integer.parseInt(weekDaysId))==1)
										{
											orderEndDate=calculateEndDate.endDate(ordDate, 29, 5);
											priceOfItem=newItemDetails.getPrice().getOneMonthFiveDayPrice();
										}
										else if((Integer.parseInt(weekDaysId))==2)
										{
											// for 60 days and  6 day plan
											orderEndDate=calculateEndDate.endDate(ordDate, 29, 6);
											priceOfItem=newItemDetails.getPrice().getOneMonthSixDayPrice();
										}
									}
									
									PackageDescription pkgDesc=new PackageDescription();
									pkgDesc.setPackageMenuDesc(allpackageDetails.get(0).getPackageDetails().getPackageMenuDesc());
									
									Tax tax=new Tax();
									tax.setTaxPnct(allpackageDetails.get(0).getTax().getTaxPnct());
									
									System.out.println("allpackageDetails.get(0).getTax().getTaxPnct() "+allpackageDetails.get(0).getTax().getTaxPnct());
									
									String s=pkgDesc.getPackageMenuDesc();

									newItemDetails.setMealTime(mealTime);
									newItemDetails.setWeekType(weekType);
									newItemDetails.setOrderStartDate(ordDate);
									newItemDetails.setOrderEndDate(orderEndDate);
									newItemDetails.setQuantity(1);
									newItemDetails.setItemPriceForOneQuantity(priceOfItem);
									newItemDetails.setPriceOfItem(priceOfItem);
									newItemDetails.setPackageDuration(packageDuration);
									newItemDetails.setPkgDesc(pkgDesc);
									newItemDetails.setTax(tax);
									
									//System.out.println("order end date "+orderEndDate);
									cartDetails.add(newItemDetails);
					
									session.setAttribute("cartDetails", cartDetails);
									session.setAttribute("cartSize", String.valueOf(cartDetails.size()));
									//System.out.println("cart me kya macha:"+ session.getAttribute("cartDetails"));
									
									if(  count==0)
									{
										
										mealTimeDesc=cartDetails.get(cartDetails.size()-1).getMealTime().getMealTimeDesc();
										mealDuration=cartDetails.get(cartDetails.size()-1).getPackageDuration().getPkgDurDesc();
										weekDays=cartDetails.get(cartDetails.size()-1).getWeekType().getWeekTypDesc();
										startDate=cartDetails.get(cartDetails.size()-1).getOrderStartDate();
										packageDescrption=cartDetails.get(cartDetails.size()-1).getPackageName();
										priceofpackage=String.valueOf(cartDetails.get(cartDetails.size()-1).getPriceOfItem());
												
										session.setAttribute("mealTimeDesc", mealTimeDesc);
										session.setAttribute("mealDuration", mealDuration);
										session.setAttribute("weekDays", weekDays);
										session.setAttribute("startDate", startDate);
										session.setAttribute("packageDescrption", packageDescrption);
										session.setAttribute("priceofpackage", priceofpackage);
										
									   String cartsize=String.valueOf(cartDetails.size());
									     
									   String [] cartDetailsArray={mealTimeDesc+"",mealDuration+"",weekDays+"",startDate+"",packageDescrption+"",cartsize+"",priceofpackage+""};
									   Gson json=new Gson();
									   return json.toJson(cartDetailsArray);
									}
									else
									{
										return String.valueOf("no");
									}
								}
							}
						}
					  }
					  
		 }
		 else
		 {
			return String.valueOf("no");
		 }
			return String.valueOf("no");
		 }

	@RequestMapping(value = "/getItems", method = RequestMethod.POST)
	public @ResponseBody String getItems(@RequestParam("mealTimeId")String mealTimeId)
	{
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		
		HttpSession session = request.getRequest().getSession(false);
		
		Map<String,Object> model=orderService.getPackageDetails();
		//System.out.println("mealTime Id is "+ mealTimeId);
		List<ListOfPackages> allpackageDetails =(List<ListOfPackages>)model.get("allpackageDetails");
		List<ListOfPackages> packagesBasedOnMealTime=new ArrayList<ListOfPackages>();
		//System.out.println("package size "+allpackageDetails.size()); 
		
		 session.setAttribute("allpackageDetails", allpackageDetails);
		
		 if(mealTimeId!=null)
		{
			for(ListOfPackages packageDetails:allpackageDetails)
			{
				if(packageDetails.getMealTime().getId()==(Integer.parseInt(mealTimeId)))
				{
					packagesBasedOnMealTime.add(packageDetails); 
				}
			}
		}
		 Gson gson = new Gson();
	     // convert your list to json
	     String jsonCartList = gson.toJson(packagesBasedOnMealTime);
	     // print your generated json
	    // System.out.println("jsonCartList: " + jsonCartList);

	     return jsonCartList;
	}
	
	
	 @RequestMapping(value = "/getMachedNames.web", method = RequestMethod.GET)
	 public @ResponseBody String getMachedNames(@RequestParam("term") String name)
	 {
	  //System.out.println("hai in gertmatched details");
	  Map<String,Object> modelForAddressList=new HashMap<String, Object>();
	  modelForAddressList =orderService.getPackageDetails();
	  List<PincodeDetails> pincodeDetails=(List<PincodeDetails>) modelForAddressList.get("pincodeList");
	  List<String> returnMatchName = new ArrayList<String>();
	  for (PincodeDetails pincode : pincodeDetails) 
	  {
	   if (pincode.getPincodeDesc().toUpperCase().contains(name.toUpperCase())) 
	    {
	      returnMatchName.add(pincode.getPincodeDesc());
	    }
	  }
	  Gson json=new Gson();
	  //System.out.println("pincode list"+returnMatchName);
	  return json.toJson(returnMatchName);
	  
	 }
	 
	@RequestMapping(value = "/getAreaDetails", method = RequestMethod.POST)
	public @ResponseBody String  getAreaDetails() 
	 {
			//System.out.println("controller from get area details ");
			List<AllAreaDetails>areaDetailsList=orderService.getAreaDetails();
			logger.info(areaDetailsList.get(0).getAreaList().get(0).getAreaDesc()+"  description");
			Gson json=new Gson();
			return json.toJson(areaDetailsList);
	}
	
	
	/*@RequestMapping(value = "/addressedit", method = RequestMethod.POST)
	public ModelAndView addresspage(@ModelAttribute("user") User user) 
	{
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		String userId=null;
		if(session.getAttribute("userId")!=null)
		{
		 userId=String.valueOf(session.getAttribute("userId"));
		}
		
		
		user.setUserId(Integer.parseInt(userId));
		Map<String,Object> modelForAddressList=new HashMap<String, Object>();
		Map<String,Object> model=new HashMap<String, Object>();
		
		model=orderService.getMasterAddressDetails();
		
		System.out.println("model.get allAreaDetails"+model.get("allAreaDetails"));
		System.out.println("model.get allAreaDetails"+model.get("pincodeList"));
		System.out.println("model.get allAreaDetails"+model.get("allCityDetails"));
		
		
		modelForAddressList.put("arealist", model.get("allAreaDetails"));
		modelForAddressList.put("pincodeList", model.get("pincodeList"));
		modelForAddressList.put("stateList",model.get("stateList"));
		@SuppressWarnings("unchecked")
		List<PincodeDetails> pincodelistdtls=(List<PincodeDetails>) model.get("pincodeList");
		String  pincode=user.getPincodeDetails().getPincodeDesc();
		for(PincodeDetails pincodes:pincodelistdtls)
		 {
			 if(pincode.equals(pincodes.getPincodeDesc()))
			 {
				 PincodeDetails pincodeDetails=user.getPincodeDetails();
				 pincodeDetails.setId(pincodes.getId());
				 user.setPincodeDetails(pincodeDetails);
			 }
		 }
	     modelForAddressList.put("cityList", model.get("allCityDetails"));
		 String userDetails=orderService.addresSave(user);
		 //System.out.println("address saved i think lets see");
		 modelForAddressList=userService.getAddressBasedOnUserId(user);
		
	return new ModelAndView("addresspage","model",model);
		
	}*/
	
	@RequestMapping(value="/saveAddress",method=RequestMethod.POST)
	public ModelAndView saveAddress(@ModelAttribute("user") User user)
	{
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		
		//System.out.println("user pincode is "+user.getPincodeDetails().getPincodeDesc());
		
		String userId=null;
		if(session.getAttribute("userId")!=null)
		{
		 userId=String.valueOf(session.getAttribute("userId"));
		}
		user.setUserId(Integer.parseInt(userId));
		Map<String,Object> model=new HashMap<String, Object>();
		Map<String,Object> modelForAddressList=new HashMap<String, Object>();
		model=orderService.getMasterAddressDetails();
		
		modelForAddressList.put("arealist", model.get("allAreaDetails"));
		modelForAddressList.put("pincodeList", model.get("pincodeList"));
		modelForAddressList.put("stateList",model.get("stateList"));
		modelForAddressList.put("cityList", model.get("allCityDetails"));
		
		
		
		String userDetails=orderService.addresSave(user);
		
		System.out.println("userDetails for the updated user "+userDetails);
		
		//System.out.println("address saved i think lets see");
		modelForAddressList=userService.getAddressBasedOnUserId(user);
		
	   return new ModelAndView("addresspage","model",modelForAddressList);	
	}
	
	
	
	
}//end of controller
