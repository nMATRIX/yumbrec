 package com.base.yumbrec.util;

public interface ProjectDetails
{

	String URL="http://localhost:8009/yumbrec/";
//	String URL="http://192.168.1.7:8082/eorder/";
//	String URL="http://192.168.1.05:8084/eorder/";
//	String URL="http://spiceboxbeta.elasticbeanstalk.com/";
//    String URL="http://foodever-ws.elasticbeanstalk.com/"; 
//	String URL="http://packageDetailsBasedOnMealTime/";
//  String URL="http://eorder-crm.elasticbeanstalk.com/";
	
	//POST URLS	
	String updateGuestAddress=URL+"updateGuestAddress";
	String ALLPACKAGEDETAILSFORWEEK=URL+"packageDetailsForListOfDays";
	String DELIVERYLOCTIONPINCODE_URL=URL+"pincodeCheck";
	String USERREGISTERDETAILS=URL+"register";
	String USERLOGINDETAILS=URL+"login";
	String FORGOTUSERPAASWORD=URL+"forgotpassword";
	String CHANGEUSERPASSWORD=URL+"changePassword";
	String AREADETAILSBASEDONCITY="";
	String PINCODEBASEDONCITY=URL+""; 
	String ADDRESSDETAILSBASEDONUSER=URL+"getAddressWithId";
	String UPADTEUSERADDRESS=URL+"updateAddress"; 
	String ORDERSAVE=URL+"ordersave";
	String MYORDERS=URL+"manageOdrerDetails";
	String UPDATEPAYMODE=URL+"paymod";
	String packageDetailsBasedOnMealTime=URL+"packageDetailsBasedOnMealTime";
	String subAreas=URL+"subAreas";
	
	// GET URLS
	String ALLPACKAGEDETAILS=URL+"packageDetails";
	String FOODTYPEDETAILS=URL+"foodTypeDetails";
	String WEEKTYPEDETAILS=URL+"weekDurationDetails";
	String MENUTYPEDETAILS=URL+"menuTypeDetails";
	String MEALDURATIONDETAILS=URL+"mealDurationDetails";
	String MEALTIMEDETAILS=URL+"mealTimeDetails";
	String DELIVERYCHARGEDETAILS=URL+"deliveryChargesDetails";
	String PAYMENTTYPEDETAILS=URL+"paymentTypeDetails";
	String PAYMENTGATEWAYDETAILS=URL+"paymentGatewayDetails";
	String CLIENTDETAILS=URL+"clientDetails";
	String TAXDETAILS=URL+"taxDetails";
	String MEALPLANDETAILS=URL+"mealPlanDetails";
	String PINCODEDETAILS=URL+"pincodeDetails";
	String CITYDETAILS=URL+"city";
	String ADDRESSLIST=URL+"addressList";
	//String STATEDETAILS=;
	
	
	
	// CONSTANT VALUES DETAILS
	String SUCCESSKEY="1";
	String FAILUREKEY="2";
	String NOTREGISTERKEY="4";
	String DATANOTSEND="3";
	String SUCCESS="success";
	
	
	
	
	
	
}
