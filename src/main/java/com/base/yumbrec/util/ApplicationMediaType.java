package com.base.yumbrec.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class ApplicationMediaType 
{
	
	public static ApplicationMediaType appicationMediaType;

	private ApplicationMediaType()
	{
	}
	public static ApplicationMediaType getInstance() 
	{
		if (appicationMediaType == null) 
		{
			appicationMediaType = new ApplicationMediaType();
		}
		return appicationMediaType;
	}
	public  HttpHeaders getApplictionType()
	{
		 List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		    acceptableMediaTypes.add(MediaType.APPLICATION_JSON);

		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(acceptableMediaTypes);
		    return headers;	
	}
}
