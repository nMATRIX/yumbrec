package com.base.yumbrec.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class CurrentDate {
	public static String dateInGMT() {
		  Date currentDate=new Date();
		  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		  TimeZone tz = TimeZone.getTimeZone("GMT");
		  dateFormat.setTimeZone(tz);
		  String dateInIndianTimeZone=dateFormat.format(currentDate);
		  return dateInIndianTimeZone;
		  
		 }
	
	}


