package com.base.yumbrec.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.base.yumbrec.model.Address;
import com.base.yumbrec.model.CartOrder;
import com.base.yumbrec.model.ListOfPackages;
import com.base.yumbrec.model.User;
import com.base.yumbrec.service.OrderService;
import com.base.yumbrec.service.UserService;
import com.base.yumbrec.util.ProjectDetails;
import com.google.gson.Gson;

@Controller
public class UserController 
{
	
	@Autowired
	UserService userService;
	@Autowired
    OrderService orderSerivce;
	
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@RequestMapping(value="/userlogin",method=RequestMethod.GET)
	public ModelAndView userlogin()
	{
		System.out.println("going to login .......");
		return new ModelAndView("login");
	}
	
	
	@RequestMapping(value = "/checkLoginStatus", method = RequestMethod.GET,produces = "application/json")
	public ModelAndView checkLoginStatus(@ModelAttribute("user") User user) 
	{
	    ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
	 
	    String sessionLoginId=(String) session.getAttribute("userId");
	    List<CartOrder> cartDetailsList=(List<CartOrder>) session.getAttribute("cartDetails");
	    String cartSize=null;
	    String userId=null;
	    if(cartDetailsList!=null && cartDetailsList.size()>0 && sessionLoginId!=null)
	    {
	    	
	    	Map<String,Object> model1=new HashMap<String, Object>();
	    	
	    	model1=orderSerivce.getMasterAddressDetails();
	    	
			if(session.getAttribute("userId")!=null)
			{
			 userId=String.valueOf(session.getAttribute("userId"));
			}
			user.setUserId(Integer.parseInt(userId));
	    	Map<String,Object> model=new HashMap<String, Object>();
			model=userService.getAddressBasedOnUserId(user);
			
			model.put("cityList",model1.get("allCityDetails"));
			model.put("statelist", model1.get("stateList"));
			model.put("pincodelist", model1.get("pincodeList"));
			model.put("allareaDetails", model1.get("allAreaDetails"));
			
			
		   return new ModelAndView("addresspage","model",model);
	    }
	    if(cartDetailsList!=null && cartDetailsList.size()>0 && sessionLoginId==null)
	    {	
	    	return new ModelAndView("login");
	    }
	    else	    
	    {
	    	 session.setAttribute("selectorder", "please select order");
	    	 return new ModelAndView("redirect:/");
	    }
	}
	
	
	@RequestMapping(value="/register" ,method = RequestMethod.POST )
	public @ResponseBody String register(@RequestParam("firstname") String firstname,@RequestParam("lastname") String lastname,  @RequestParam("mobile") String mobile , @RequestParam("password") String password , @RequestParam("email") String email)
	{
		logger.info("welcome to User Registration");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		session.setMaxInactiveInterval(30);
	    mobile = mobile.substring(mobile.length() - 10);
		System.out.println("mobile is printing here "+mobile);
		
		User user1=new User();
		user1.setUserContactNumber(mobile.trim());
		user1.setUserEmailId(email.trim());
		user1.setUserFirstName(firstname.trim());
		user1.setUserLastName(lastname.trim());
		user1.setUserPassword(password.trim());
		
		String message=null;
		User userDetails=userService.registerUser(user1);
		System.out.println("key "+userDetails.getSuccesskey());
		if(Integer.parseInt(userDetails.getSuccesskey())==1)
		{
			 session.setAttribute("firstname",userDetails.getUserFirstName());
		     session.setAttribute("lastname",userDetails.getUserLastName());
		     session.setAttribute("contantNumber",userDetails.getUserContactNumber());
		     session.setAttribute("password",userDetails.getUserPassword());
		     session.setAttribute("email", userDetails.getUserEmailId());
		     session.setAttribute("userId", userDetails.getUserId()+"");
		     session.setAttribute("sessionLoginId", userDetails.getUserId()+"");
		     
		    System.out.println("session.getAttribute('firstname') "+  session.getAttribute("firstname"));
		
		System.out.println("userDetails.getUserFirstName() "+ userDetails.getUserFirstName());
		System.out.println("userDetails.getUserLastName() "+ userDetails.getUserLastName());
		}
		else if(ProjectDetails.FAILUREKEY.equals(userDetails.getFailurekey()))
		{
			 message="Details are already Exist,please login";
		}
		System.out.println("message from register "+message);
     	System.out.println("userDetails "+userDetails);
		System.out.println(userDetails.getErrorMessage()+" user message");
		Gson json=new Gson();
		return json.toJson(message);
	}
	
	/*@RequestMapping(value="/register" ,method = RequestMethod.POST )
	public @ResponseBody String register(@ModelAttribute("register") User user,@ModelAttribute("firstname") String firstname,@ModelAttribute("lastname") String lastname,  @ModelAttribute("mobile") String mobile , @ModelAttribute("password") String password , @ModelAttribute("email") String email,@ModelAttribute("confirmpassword") String confirmpassword)
	{
		
		logger.info("welcome to User Registration");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		session.setMaxInactiveInterval(30);
	    mobile = mobile.substring(mobile.length() - 10);
		System.out.println("mobile is printing here "+mobile);
		
		User user1=new User();
		user1.setUserContactNumber(mobile.trim());
		user1.setUserEmailId(email.trim());
		user1.setUserFirstName(firstname.trim());
		user1.setUserLastName(lastname.trim());
		user1.setUserPassword(password.trim());
		
		String message=null;
		User userDetails=userService.registerUser(user1);
		System.out.println("key "+userDetails.getSuccesskey());
		if(Integer.parseInt(userDetails.getSuccesskey())==1)
		{
			 session.setAttribute("firstname",userDetails.getUserFirstName());
		     session.setAttribute("lastname",userDetails.getUserLastName());
		     session.setAttribute("contantNumber",userDetails.getUserContactNumber());
		     session.setAttribute("password",userDetails.getUserPassword());
		     session.setAttribute("email", userDetails.getUserEmailId());
		     session.setAttribute("userId", userDetails.getUserId()+"");
		     session.setAttribute("sessionLoginId", userDetails.getUserId()+"");
		     
		    System.out.println("session.getAttribute('firstname') "+  session.getAttribute("firstname"));
		
		System.out.println("userDetails.getUserFirstName() "+ userDetails.getUserFirstName());
		System.out.println("userDetails.getUserLastName() "+ userDetails.getUserLastName());
		}
		else if(ProjectDetails.FAILUREKEY.equals(userDetails.getFailurekey()))
		{
			 message="Details are already Exist,please login";
		}
		System.out.println("message "+message);
		System.out.println("userDetails.getUserId() "+userDetails.getUserId());
     	System.out.println(userDetails);
		System.out.println(userDetails.getErrorMessage()+" user message");
		Gson json=new Gson();
		return json.toJson(message);
	}*/
	
	@RequestMapping(value = "/logOut", method = RequestMethod.POST )
	public ModelAndView getOrderPage()
	{
		logger.info("welcome to userProfile");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		
		session.setAttribute("firstname",null);
		session.setAttribute("lastname", null);
		session.setAttribute("contantNumber",null);
		session.setAttribute("password",	null);
		session.setAttribute("email",	null);
		session.setAttribute("userId",	null);
		session.setAttribute("sessionLoginId",null);
		session.setAttribute("cartSize", null);
		session.setAttribute("cartDetails", null);
		session.invalidate();
		
		//System.out.println("cart size in session in logout "+session.getAttribute("cartSize"));
		//System.out.println("cartDetails size in session in log out "+session.getAttribute("cartDetails"));
		return new ModelAndView("index");
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST,produces = "application/json")
	public ModelAndView login(@Valid @ModelAttribute("user") User modelUser,BindingResult bindingResult ,@ModelAttribute("mobile") String mobile,@ModelAttribute("emailid") String emailId,@ModelAttribute("passwordid") String password) 
	{
		 logger.info("::::::::::Home Controller:login::::::::::::::::");
		 ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		 HttpSession session = request.getRequest().getSession(false);
		 Map<String,Object> model=new HashMap<String, Object>();
	     int message=0;
	    
	     System.out.println("emailId from login "+emailId);
	     System.out.println("from login mobile  "+mobile);
	    
	     User user=new User();
	     user.setUserPassword(password);
	     
	     
	     if(mobile==null || mobile.equals(""))
	     {
				System.out.println(mobile+"mobile is" );
		 }
	     else
	     {
			user.setUserContactNumber(mobile);
		 }
					
	     if(emailId==null || emailId.equals(""))
	     {
				
		 }
	     else
	     {
			user.setUserEmailId(emailId);
		 }
	     
	     
	     /*if(mobile!=null)
	     {
	    	 user.setUserContactNumber(mobile);
	     }
	     else if(emailId!=null) 
	     {
	    	// user.setUserContactNumber(mobile);
	    	 user.setUserEmailId(emailId);
		 }*/
	     
	     User userDetails=userService.userLogin(user);
	     
	     System.out.println("userDetails.getErrorMessage() "+userDetails.getErrorMessage());
	     
	     if(userDetails.getErrorMessage()==null)
	     {
	     session.setAttribute("firstname",userDetails.getUserFirstName());
	     session.setAttribute("lastname",userDetails.getUserLastName());
	     session.setAttribute("contantNumber",userDetails.getUserContactNumber());
	     session.setAttribute("password",userDetails.getUserPassword());
	     session.setAttribute("email", userDetails.getUserEmailId());
	     session.setAttribute("userId", userDetails.getUserId()+"");
	     session.setAttribute("sessionLoginId", userDetails.getUserId()+"");
	   
	     // get addresses
	   
	     User userForAddress=new User();
		 String userId=(String) session.getAttribute("userId");
		 //System.out.println("sessionLoginId : from login url  "+userDetails.getUserId());
		
		 userForAddress.setUserId(Integer.parseInt(userId));
		 //System.out.println("going to serivce from user controller...");
		 model = userService.getAddressBasedOnUserId(userForAddress);
			  
		 session.setAttribute("home", model.get("homeAddress"));
		 session.setAttribute("office", model.get("officeAddress"));
		 session.setAttribute("other", model.get("newAddress"));
		 
	     Address homeAddress= (Address) model.get("homeAddress");
		 Address officeAddress= (Address) model.get("officeAddress");
		 Address newAddress=  (Address) model.get("newAddress");
		 Address [] responseString=new Address[3];
		 responseString[0]=homeAddress;
	     responseString[1]=officeAddress;
	     responseString[2]=newAddress;
	     }
	     String userId=(String) session.getAttribute("userId");
         List<CartOrder> cartDetailsList=(List<CartOrder>) session.getAttribute("cartDetails");
	     if(userId!=null && cartDetailsList!=null && cartDetailsList.size()>0)
	     {
	    	 Map<String,Object> model1=new HashMap<String, Object>();
	    	 model1=orderSerivce.getMasterAddressDetails();
	    	 model.put("cityList",model1.get("allCityDetails"));
			 model.put("statelist", model1.get("stateList"));
			 model.put("pincodelist", model1.get("pincodeList"));
			 model.put("allareaDetails", model1.get("allAreaDetails"));
	       return new ModelAndView("addresspage","model", model);
	     }
	     else if(cartDetailsList!=null && cartDetailsList.size()>0 && userId==null)
	     {
	    	 return new ModelAndView("login");    
	     }
	     else if(cartDetailsList==null && userId!=null)
	     {
	    	 return new ModelAndView("redirect:/");
	     }
	     else if(bindingResult.hasErrors())
	     {
	    	 session.setAttribute("errormsg",userDetails.getErrorMessage());
	    	 //System.out.println("userDetails.getErrorMessage() "+userDetails.getErrorMessage());
	    	// System.out.println("am from binding");
	    	 return new ModelAndView("redirect:/userlogin");
	     }
	     else
	     {
	    	 session.setAttribute("errormsg",userDetails.getErrorMessage());
	    	 //System.out.println("am not from bindinig");
	    	 return new ModelAndView("redirect:/userlogin");
	     }
	}
	
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST) 
	public @ResponseBody String forgotPassword(@RequestParam("email") String email) 
	{
		logger.info(":::::::::: User Controller: forgotpassword::::::::::::::::");
	
	    User user=new User();
	    user.setUserEmailId(email);
	    int message=0;
	    User userDetails=userService.forgotUserPassword(user);
	    if(userDetails.getErrorMessage()==null)
	    {
		   message=1;
	    }
	    else
	    {
		   message=2;
	    }
	   // System.out.println(userDetails.getErrorMessage()+" user message");
	    Gson json=new Gson();
	    return json.toJson(message);
	}

	@RequestMapping(value="/changePassword", method=RequestMethod.POST)
	public @ResponseBody String changePassword(@RequestParam("oldpassword") String oldPassword,@RequestParam("newpassword") String newpassword)
	{
		logger.info(":::::::::: User Controller: forgotpassword::::::::::::::::");
		ServletRequestAttributes request=(ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session=request.getRequest().getSession();
		
		String userId=(String) session.getAttribute("userId");
		//System.out.println("useId ::: "+userId);
		
		User user=new User();
		user.setOldPassword(oldPassword);
		user.setUserPassword(newpassword);
		
		User userDetails=userService.changePassword(user);
		
		
		return null;
	}
	/*
	@RequestMapping(value = "/saveAddress", method = RequestMethod.POST,produces = "application/json")
	public  @ResponseBody String saveAddress(@RequestParam("addressTypeForSave")String addressTypeForSave,@RequestParam("cityId")String cityId,@RequestParam("areaId")String areaId,@RequestParam("pincodeId")String pincode,@RequestParam("laneDescriptionid")String laneDescription,@RequestParam("addressMealTimeAndDateDesc")String addressMealTimeAndDateDesc) 
	{
		logger.info(":::::::::: saveAddress::::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession(false);
		Map<String,Object> model=new HashMap<String, Object>();
		//System.out.println("addressTypeForSave: user from user controller "+addressTypeForSave);
		//System.out.println("addressMealTimeAndDateDesc: user from user controller "+addressMealTimeAndDateDesc);
		model=userService.getPackageDetails();
		
		User user=new User();
		Address address=new Address();
		String userId=(String)session.getAttribute("userId");
		if(userId==null)
		{
			return "no";
		}
		user.setUserId(Integer.parseInt(userId));
		
		  user.setHouseDescription("");
		  user.setLandmark(laneDescription);
		  String result=userService.updateUserAddress(user);
		  
		  address.setHouseNo("");
		  //address.setArea(area);
		  address.setLandMrk(laneDescription);
		  
		  JSONObject jObject  = new JSONObject(result);
		  int addressId=jObject.getInt("addressId");
		  address.setId(addressId);
		  //System.out.println("result for address: user from user controller "+result);
		  List<CartOrder> cartDetails=(List<CartOrder>) session.getAttribute("cartDetails");
		  for(int i=0;i<cartDetails.size();i++)
		  {
			  CartOrder cartOrder=cartDetails.get(i);
			  if(cartOrder.getPackageId()==Integer.parseInt(addressMealTimeAndDateDesc))
			  {
				  cartOrder.setAddress(address);
			  }
		  }
		  Gson json=new Gson();
		  String jsonString=json.toJson(user);
		  //System.out.println("User string: user from user controller "+jsonString);
		return jsonString;
	}*/
	
	@RequestMapping(value = "/addAddressForAllOrders", method = RequestMethod.POST,produces = "application/json")
	public  @ResponseBody String addAddressForAllOrders(@RequestParam("addressType")String addressType) 
	{
		logger.info(":::::::::: User Controller : addAddressForAllOrders::::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Map<String,Object> model=new HashMap<String, Object>();
		HttpSession session = request.getRequest().getSession(false);
		
		String userId=(String)session.getAttribute("userId");
		//System.out.println("session.getAttribute   "+session.getAttribute("userId"));
		
		User userForAddress=new User();
		userForAddress.setUserId(Integer.parseInt(userId));
		Address address=new Address();
		model=userService.getAddressBasedOnUserId(userForAddress);
		//System.out.println("addressType in cart: from user Controller "+addressType);
		if(Integer.parseInt(addressType)==1)
		{
			address=(Address)model.get("homeAddress");
		}
		else if(Integer.parseInt(addressType)==2)
		{
			   address=(Address)model.get("officeAddress");
		}
		else if(Integer.parseInt(addressType)==3)
		{
		   address=(Address)model.get("newAddress");
        }
		List<CartOrder> cartPackageDetails=new ArrayList<CartOrder>();
		cartPackageDetails=(List<CartOrder>)session.getAttribute("cartDetails");
		  
		List<CartOrder> cartPackageDetailsAfterAddress=new ArrayList<CartOrder>();
		  
	    if(cartPackageDetails!=null && cartPackageDetails.size()>0)
		  {
		   for(int i=0;i<cartPackageDetails.size();i++ )
		     {
			CartOrder cartOrder=cartPackageDetails.get(i);
			cartOrder.setAddress(address);
		    cartPackageDetailsAfterAddress.add(cartOrder);
		     }
		  }
		  //System.out.println("cart details with address in cart: from user Controller "+cartPackageDetailsAfterAddress);
		  model=userService.getPackageDetails();
		  //System.out.println(" All package details in cart: from user Controller "+model.get("allpackageDetails"));
		 
		  List<ListOfPackages>  allpackageDetails=(List<ListOfPackages>) model.get("allpackageDetails");
		  if(cartPackageDetailsAfterAddress!=null && cartPackageDetailsAfterAddress.size()>0)
		   {
			   for(int i=0;i<cartPackageDetailsAfterAddress.size();i++ )
			   {
				CartOrder cartOrder=cartPackageDetailsAfterAddress.get(i);
				cartOrder.setQuantity(1);
				
				for(int j=0;j<allpackageDetails.size();j++)
				{
				if(cartOrder.getPackageId()==allpackageDetails.get(j).getPackageId())
				{
				 cartOrder.setPackageName(allpackageDetails.get(j).getPackageName());
				// System.out.println("package name: from user controller"+cartOrder.getPackageName());
			    }
			    if(cartOrder.getPackageDuration().getId()==1)
			    {
				 float oneDayPrice=allpackageDetails.get(j).getPrice().getOneDayPrice(); 
				 cartOrder.setItemPriceForOneQuantity(oneDayPrice);
				 cartOrder.setPriceOfItem(oneDayPrice);
			    }
			    else if(cartOrder.getPackageDuration().getId()==2)
			    {
				   float twentyDayPrice=allpackageDetails.get(j).getPrice().getOneWeekFiveDayPrice();
				   cartOrder.setItemPriceForOneQuantity(twentyDayPrice);
				   cartOrder.setPriceOfItem(twentyDayPrice);
			    }
			    else if(cartOrder.getPackageDuration().getId()==3)
			    {
				   float fortyDayPrice=allpackageDetails.get(j).getPrice().getTwoWeekFiveDayPrice();
				   cartOrder.setItemPriceForOneQuantity(fortyDayPrice);
				   cartOrder.setPriceOfItem(fortyDayPrice);
			    }
			   else if(cartOrder.getPackageDuration().getId()==4)
			   {
				   float sixtyDayPrice=allpackageDetails.get(j).getPrice().getOneWeekFiveDayPrice();
				   cartOrder.setItemPriceForOneQuantity(sixtyDayPrice);
				   cartOrder.setPriceOfItem(sixtyDayPrice);
			   }
			   if(cartOrder.getPackageDuration().getId()==1)
			   {
				   cartOrder.getPackageDuration().setPkgDurDesc("1 Day"); 
			   }
			   else if(cartOrder.getPackageDuration().getId()==2)
			   {
				   cartOrder.getPackageDuration().setPkgDurDesc("One Week");
			   }
			   else if(cartOrder.getPackageDuration().getId()==3)
			   {
				   cartOrder.getPackageDuration().setPkgDurDesc("Two Weeks");
			   }
			   else if(cartOrder.getPackageDuration().getId()==4)
			   {
				   cartOrder.getPackageDuration().setPkgDurDesc("One Month");
			   }
			   }
			   }
			  }
		   System.out.println("cart orders after adding all: from user Controller"+cartPackageDetailsAfterAddress);
		   session.setAttribute("cartDetails", cartPackageDetailsAfterAddress);
		   CartOrder [] cartOrderString=new CartOrder[cartPackageDetailsAfterAddress.size()];
		   for(int i=0;i<cartPackageDetailsAfterAddress.size();i++)
		    {
			 cartOrderString[i]=cartPackageDetailsAfterAddress.get(i);
		    }
		  Gson json=new Gson();
		  String jsonString=json.toJson(cartOrderString);
		return jsonString;
	}
}
