package com.base.yumbrec.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.base.yumbrec.model.Address;
import com.base.yumbrec.model.Area;
import com.base.yumbrec.model.BillDetails;
import com.base.yumbrec.model.CartOrder;
import com.base.yumbrec.model.City;
import com.base.yumbrec.model.DeliveryCharges;
import com.base.yumbrec.model.ListOfPackages;
import com.base.yumbrec.model.Orders;
import com.base.yumbrec.model.PackageDetails;
import com.base.yumbrec.model.PaymodeDetails;
import com.base.yumbrec.model.PincodeDetails;
import com.base.yumbrec.model.SavePkg;
import com.base.yumbrec.model.State;
import com.base.yumbrec.model.Tax;
import com.base.yumbrec.model.User;
import com.base.yumbrec.service.OrderService;
import com.google.gson.Gson;


@Controller
public class OrderController 
{
	@Autowired
	OrderService orderService;
	
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	
	@SuppressWarnings("unchecked")
	public ModelAndView allPkgDetails(Locale locale, Model model1,@ModelAttribute("pincode") PincodeDetails pincode,@ModelAttribute("user") User user)
	{
		Map<String,Object> model=new HashMap<String, Object>();
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
		List<PackageDetails> allPackageDetails=new ArrayList<PackageDetails>();
		allPackageDetails=(List<PackageDetails>)session.getAttribute("cartPackageDetails");
		List<PackageDetails> packageDetails=new ArrayList<PackageDetails>();
		packageDetails=(List<PackageDetails>)session.getAttribute("packageDetails");
		
		model=orderService.getPackageDetails();
		
		session.setAttribute("packageDetails", model.get("allpackageDetails"));
		
		return new ModelAndView("index","model",model);
	}
	
	
	
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	 public ModelAndView checkout() 
	 {
		Map<String,Object> model=new HashMap<String, Object>(); 
		System.out.println("checkout page.......");
		
		logger.info("::::::::::OrderController::checkout page:::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
	    String cartSize=(String) session.getAttribute("cartSize");
	    String userLogin=(String) session.getAttribute("sessionLoginId");
	  
	    @SuppressWarnings("unchecked")
		List<CartOrder> cartDetailsList=(List<CartOrder>) session.getAttribute("cartDetails");
	  
	    //System.out.println("user name "+userLogin+" cartsize is "+cartSize);
	    if(userLogin!=null && cartSize!=null && cartDetailsList!=null )
	      {
		    User userForAddress=new User();
		    userForAddress.setUserId(Integer.parseInt(userLogin));
		    model = orderService.getAddressBasedOnUserId(userForAddress);
	        session.setAttribute("home", model.get("homeAddress"));
		    session.setAttribute("office", model.get("officeAddress"));
		    session.setAttribute("other", model.get("newAddress"));
		    Address homeAddress= (Address) model.get("homeAddress");
		    Address officeAddress= (Address) model.get("officeAddress");
		    Address newAddress=  (Address) model.get("newAddress");
		    float grandTotal=0;
		    for(int i=0;i<cartDetailsList.size();i++)
		      {
			    grandTotal=grandTotal+cartDetailsList.get(i).getPriceOfItem(); 
		      }
		    
		    System.out.println("cartDetailsList.get(i).getPriceOfItem(); "+cartDetailsList.get(0).getPriceOfItem());
		    System.out.println("grand total "+grandTotal);
		    model.put("cartDetailsList", cartDetailsList);
		    model.put("cartDetailsListSize",cartDetailsList.size());
		    Map<String,Object> model1=orderService.getMasterAddressDetails();
		    List<City> cityList=(List<City>) model1.get("allCityDetails");
		    List<Area> areaList=(List<Area>) model1.get("allAreaDetails");
		    model.put("cityList", cityList);
		    model.put("areaList",areaList);
		    model.put("grandTotalOfItems",grandTotal+"");
		    session.setAttribute("grandTotalOfItems", grandTotal+"");
		    
		    if(newAddress!=null)
		     {
		      model.put("newAddress", newAddress);
	  	     }
		     else
		     {
	          model.put("newAddress", null);
	  	     }	
		    // System.out.println("model in service check out url "+model);
		   return new ModelAndView("checkout","model",model);
	     }
	     else
	     {
		   return new ModelAndView("redirect:/");
	     }
	 }
	
	
	 @RequestMapping(value = "/addresspage", method = RequestMethod.GET)
	 public ModelAndView addresspage(@ModelAttribute("user") User user) 
	 {
		Map<String,Object> model=new HashMap<String, Object>(); 
		System.out.println("checkout page.......");
		
		logger.info("::::::::::OrderController::checkout page:::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = request.getRequest().getSession();
	    String cartSize=(String) session.getAttribute("cartSize");
	    String userLogin=(String) session.getAttribute("sessionLoginId");
	  
	    @SuppressWarnings("unchecked")
		List<CartOrder> cartDetailsList=(List<CartOrder>) session.getAttribute("cartDetails");
	  
	    //System.out.println("user name "+userLogin+" cartsize is "+cartSize);
	    if(userLogin!=null && cartSize!=null && cartDetailsList!=null )
	      {
		    User userForAddress=new User();
		    //if user have already address in db it is come to here
		    userForAddress.setUserId(Integer.parseInt(userLogin));
		    model = orderService.getAddressBasedOnUserId(userForAddress);
	        session.setAttribute("home", model.get("homeAddress"));
		    session.setAttribute("office", model.get("officeAddress"));
		    session.setAttribute("other", model.get("newAddress"));
		   
		    Address homeAddress= (Address) model.get("homeAddress");
		    Address officeAddress= (Address) model.get("officeAddress");
		    Address newAddress=  (Address) model.get("newAddress");
		    float grandTotal=0;
		    for(int i=0;i<cartDetailsList.size();i++)
		      {
			    grandTotal=grandTotal+cartDetailsList.get(i).getPriceOfItem(); 
		      }
		    //System.out.println("cartDetailsList.get(i).getPriceOfItem(); "+cartDetailsList.get(0).getPriceOfItem());
		    //System.out.println("grand total "+grandTotal);
		    model.put("cartDetailsList", cartDetailsList);
		    model.put("cartDetailsListSize",cartDetailsList.size());
		    
		    Map<String,Object> model1=orderService.getMasterAddressDetails();
		    List<City> cityList=(List<City>) model1.get("allCityDetails");
		    List<Area> areaList=(List<Area>) model1.get("allAreaDetails");
		    List<State> statelist=(List<State>) model1.get("stateList");
		    List<PincodeDetails> pincodelist=(List<PincodeDetails>) model1.get("stateList");
		    model.put("cityList", cityList);
		    model.put("areaList",areaList);
		    model.put("statelist",statelist );
		    model.put("pincodelist", pincodelist);
		    model.put("grandTotalOfItems",grandTotal+"");
		    session.setAttribute("grandTotalOfItems", grandTotal+"");
		    
				  
		    if(newAddress!=null || homeAddress!=null ||officeAddress!=null)
		     {
		      model.put("newAddress", newAddress);
		      model.put("homeAddress", homeAddress);
		      model.put("officeAddress", officeAddress);
	  	     }
		     else
		     {
	          model.put("newAddress", null);
	  	     }	
		     //System.out.println("model in service check out url "+model);
		   return new ModelAndView("addresspage","model",model);
	     }
	     else
	     {
		   return new ModelAndView("redirect:/");
	     }
	 }
	 
  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/deleteOrder", method = RequestMethod.POST)
  public @ResponseBody String deleteOrder(@RequestParam("id") String id) 
  {
	logger.info("::::::::::Order Controller: deleteOrder ::::::::::::::::");
	ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	//System.out.println("packageId:"+id);
	
	HttpSession session = request.getRequest().getSession();
	List<ListOfPackages> cartItemsList=(List<ListOfPackages>)session.getAttribute("allpackageDetails");
	List<CartOrder> cartDetails=new ArrayList<CartOrder>();
	cartDetails=(List<CartOrder>) session.getAttribute("cartDetails");
    int count=0;
	if(id!=null)
	{
	  if(cartDetails!=null && cartDetails.size()>0 )
	  {
		for(int i=0;i<cartDetails.size();i++)
		{
		  if(Integer.parseInt(id)==cartDetails.get(i).getPackageId())
		  {
			cartDetails.remove(i);
			count=cartDetails.size();
		  }
		}
	  }
	}
	session.setAttribute("cartDetails", cartDetails);
	session.setAttribute("cartSize", String.valueOf(cartDetails.size()));
	return String.valueOf(count+"");
	}
  
  
    @RequestMapping(value = "/orderSaveWithOutCopuon", method = RequestMethod.POST)
	public ModelAndView orderSaveWithOutCopuon(@ModelAttribute("user")  User user, RedirectAttributes redirectAttributes) 
     {
		logger.info(":::::::::: Order Controller:orderSaveWithOutCopuon::::::::::::::::");
		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String deliverycharge="0";
		HttpSession session = request.getRequest().getSession();
		Orders orders=new Orders();
		Map<String,Object> model=new HashMap<String, Object>();
		//System.out.println("we are in Order Controller for placing the order");
		
		String userId=(String) session.getAttribute("userId");
		
		String  contantNumber=null;
		String email=null;
		String userName=null;
		if(userId!=null)
		{
			 email=(String) session.getAttribute("email");
			 contantNumber=(String) session.getAttribute("contantNumber");
			 userName=(String) session.getAttribute("firstname");
		}
		else if(userId==null)
		{
			return new ModelAndView("redirect:/");
		}
		List<DeliveryCharges> deliveryChargesList=orderService.deliveryChargesList();
		@SuppressWarnings("unchecked")
		List<CartOrder> cartPackageDetailsAfterAddress=(List<CartOrder>) session.getAttribute("cartDetails");
		if(cartPackageDetailsAfterAddress==null)
		{
			cartPackageDetailsAfterAddress=new ArrayList<CartOrder>();
		}
		for(int i=0;i<cartPackageDetailsAfterAddress.size();i++)
		{
			int packDurationId=cartPackageDetailsAfterAddress.get(i).getPackageDuration().getId();
			for(int j=0;j<deliveryChargesList.size();j++){
			int deliverchargeId=deliveryChargesList.get(j).getId();
			if(packDurationId==deliverchargeId){
				deliverycharge=deliveryChargesList.get(j).getDelvChrg();
			}
			}
		}
		List<Tax> taxList=orderService.gettaxList();
		float grndTot=0;
		float taxpercentage=Float.parseFloat(taxList.get(0).getTaxPnct());
		float subAmount=0;
		@SuppressWarnings("unchecked")
		List<CartOrder> cartorderdtls=(List<CartOrder>) session.getAttribute("cartDetails");
		if(cartorderdtls!=null)
		{
		  for(int i=0;i<cartorderdtls.size();i++)
			{
			  float priceOfItem=cartorderdtls.get(i).getPriceOfItem();
			  subAmount=subAmount+priceOfItem;
		}
		}
		else
		{
			return new ModelAndView("redirect:/");
		}
		grndTot=subAmount+(subAmount*taxpercentage/100);
		List<SavePkg> savePkgs=new ArrayList<SavePkg>();
		for(int i=0;i<cartorderdtls.size();i++)
		{
		  if(cartorderdtls.get(i).getAddress()!=null)
			 {
			   SavePkg savePkg=new SavePkg();
			   savePkg.setPkgId(String.valueOf(cartorderdtls.get(i).getPackageId()));
			   savePkg.setAddressId(String.valueOf(cartorderdtls.get(i).getAddress().getId()));
			   savePkg.setFoodTypId("1");
			   savePkg.setMealTimeId(String.valueOf(cartorderdtls.get(i).getMealTime().getId()));
			   savePkg.setMealTypeId("1");
			   savePkg.setOrdStDt(String.valueOf(cartorderdtls.get(i).getOrderStartDate()));
			   savePkg.setOrdEndDt(cartorderdtls.get(i).getOrderEndDate()); 
			   savePkg.setPackageAmount(String.valueOf(cartorderdtls.get(i).getPriceOfItem()));
			   savePkg.setPkgDurId(String.valueOf(cartorderdtls.get(i).getPackageDuration().getId()));
			   savePkg.setPkgQunty(String.valueOf(cartorderdtls.get(i).getQuantity()));
			   savePkg.setWeekDur(String.valueOf(cartorderdtls.get(i).getWeekType().getId()));
			   savePkgs.add(savePkg);		 
			 }
			 else
			 {
				 redirectAttributes.addFlashAttribute("message", "please enter address for all Orders");
				 return new ModelAndView("redirect:/checkout");
			 }
		 }
		 orders.setCustomerId(userId);
		 orders.setDelChrg(deliverycharge);
		 orders.setGrndTot(String.valueOf(grndTot));
		 orders.setSavePkgs(savePkgs);
		 orders.setSubAmnt(String.valueOf(subAmount));
		 //System.out.println("orders "+orders);
		 
		 if(user!=null)
		 {
		   if(user.getPaymode()==4)
			 {
				orders.setPayModId(user.getPaymode()+"");
				BillDetails billDetails=orderService.orderSave(orders); 
				if(billDetails!=null)
				{
					PaymodeDetails paymodeDetails=new PaymodeDetails();
					paymodeDetails.setOrderId(billDetails.getCustBillId()+"");
					paymodeDetails.setPaymodeId(4);
					paymodeDetails.setTxnid(billDetails.getCustBillId()+"");
					session.setAttribute("orderId",billDetails.getCustBillId()+"");
					boolean updateStatus=orderService.updatePaymodeId(paymodeDetails);
					session.setAttribute("cartDetails",null);
					session.setAttribute("cartSize", null);
				    session.setAttribute("finalAmntString", null);
				    model.put("custBillId", billDetails.getCustBillId());
				  return new ModelAndView("thanks","model",model);
				}
			 }
		   else if(user.getPaymode()==6)
		   {
				 orders.setPayModId(user.getPaymode()+"");
				 BillDetails billDetails=orderService.orderSave(orders);
				 if(billDetails!=null && contantNumber!=null)
				 {
						 session.setAttribute("orderId",billDetails.getCustBillId()+"");
						 session.setAttribute("finalAmntString", grndTot+"");
						 session.setAttribute("cntcNumber",contantNumber);
						 session.setAttribute("email",email);
						 session.setAttribute("firstname",userName);
					 return new ModelAndView("redirect:/gotoPayU");
				 }
				 return new ModelAndView("redirect:/checkout");
			 }
		 }
		return null;
	}
    
     @RequestMapping(value = "/myOrders", method = RequestMethod.GET)
	 public ModelAndView myorders() 
     {
	   ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	   HttpSession session = request.getRequest().getSession();
	   String userLoginId=(String)session.getAttribute("userId");
	   Map<String , Object> model=new HashMap<String, Object>();
	   if(userLoginId!=null)
	   {
	     String userId=userLoginId;
	     model = orderService.getMyOrders(Integer.parseInt(userId));
	     //System.out.println("user id in model order controller "+model.get("userId"));
	     //System.out.println("model in controller: for get my orders "+model);
	    return new ModelAndView("orders","model",model);
	   }
	   else
	   {
		 return new ModelAndView("redirect:/checkout");
	   }
	 }
     
    @SuppressWarnings({  "unchecked" })
 	@RequestMapping(value = "/increseItemQuantity", method = RequestMethod.POST,produces = "application/json")
 	public @ResponseBody String increseItemQuantity(@ModelAttribute("packageId") String packageId) 
    {
 		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
 		HttpSession session = request.getRequest().getSession();
 		List<CartOrder> cartPackageDetails=new ArrayList<CartOrder>();
 		cartPackageDetails=(List<CartOrder>)session.getAttribute("cartDetails");
 		int selectedPackageId=0;
 		String PackageName=null;
 		int packageQuantity=0;
 		float packageItemtotalPrice=0;
 		float packagePrice=0; 
 		float garndTotalOfItems=0;
 		int totalItemQuantity=0;
 		if(cartPackageDetails!=null && cartPackageDetails.size()>0)
 		{
 			for(int i=0;i<cartPackageDetails.size();i++)
 			{
 				if(Integer.parseInt(packageId)==cartPackageDetails.get(i).getId())
 				{
 					float itemPrice=cartPackageDetails.get(i).getItemPriceForOneQuantity();
 					int itemQuantity=cartPackageDetails.get(i).getQuantity();
 					selectedPackageId=Integer.parseInt(packageId);
 					cartPackageDetails.get(i).setPriceOfItem(Math.round(itemPrice*(++itemQuantity)));
 					cartPackageDetails.get(i).setQuantity((itemQuantity));
 					//System.out.println(cartPackageDetails.get(i).getQuantity()+" in increase quantity from order controller in increase method");
 					packageQuantity=cartPackageDetails.get(i).getQuantity();
 					packageItemtotalPrice=cartPackageDetails.get(i).getPriceOfItem();
 					//Inclusive Tax calculation
 					float itemTax=0;
 					itemTax=(packageItemtotalPrice*100)/(100+Float.parseFloat(cartPackageDetails.get(i).getTax().getTaxPnct()));
 					itemTax=packageItemtotalPrice-itemTax;
 					cartPackageDetails.get(i).setTaxAmount(itemTax);
 				}
 			}
 		}
 		float totalTaxOfItems=0;
 		if(cartPackageDetails!=null && cartPackageDetails.size()>0)
 		{
 			for(int i=0;i<cartPackageDetails.size();i++)
 			{
 				garndTotalOfItems=garndTotalOfItems+cartPackageDetails.get(i).getPriceOfItem();
 				totalItemQuantity=totalItemQuantity+cartPackageDetails.get(i).getQuantity();
 				totalTaxOfItems=totalTaxOfItems+cartPackageDetails.get(i).getTaxAmount();
 			}
 		}
 	/*	if(garndTotalOfItems==0){
 			String grandTotal=(String)session.getAttribute("garndTotalOfItems");
 		if(grandTotal!=null){
 			garndTotalOfItems=Float.parseFloat(grandTotal);
 		}
 		}*/
 		session.setAttribute("garndTotalOfItems", garndTotalOfItems+"");
 		session.setAttribute("totalItemQuantity", totalItemQuantity+"");
 		session.setAttribute("cartPackageDetails", cartPackageDetails);
 		String [] packageDetailsWithGrandTotal={packageId+"",PackageName,packageItemtotalPrice+"",packagePrice+"",packageQuantity+"",garndTotalOfItems+"",""+totalItemQuantity};
 		Gson json=new Gson();
 		return json.toJson(packageDetailsWithGrandTotal);
 	}
 	
 	
 	
 	@SuppressWarnings({  "unchecked" })
 	@RequestMapping(value = "/decreaseItemQuantity", method = RequestMethod.POST,produces = "application/json")
 	public @ResponseBody String decreaseItemQuantity(@ModelAttribute("packageId") String packageId) 
 	{
 		ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
 		HttpSession session = request.getRequest().getSession();
 		List<CartOrder> cartPackageDetails=new ArrayList<CartOrder>();
 		cartPackageDetails=(List<CartOrder>)session.getAttribute("cartDetails");
 		int selectedPackageId=0;
 		String PackageName=null;
 		int packageQuantity=0;
 		float packageItemtotalPrice=0;
 		float packagePrice=0; 
 		float garndTotalOfItems=0;
 		int totalItemQuantity=0;
 		
 		if(cartPackageDetails!=null && cartPackageDetails.size()>0)
 		{
 			for(int i=0;i<cartPackageDetails.size();i++)
 			{
 				if(Integer.parseInt(packageId)==cartPackageDetails.get(i).getId())
 				{
 					float itemPrice=cartPackageDetails.get(i).getItemPriceForOneQuantity();
 					int itemQuantity=cartPackageDetails.get(i).getQuantity();
 					selectedPackageId=Integer.parseInt(packageId);
 					if(cartPackageDetails.get(i).getQuantity()>1){
 					cartPackageDetails.get(i).setPriceOfItem(Math.round(itemPrice*(itemQuantity-1)));
 					cartPackageDetails.get(i).setQuantity((itemQuantity-1));
 					packageQuantity=cartPackageDetails.get(i).getQuantity();
 					packageItemtotalPrice=cartPackageDetails.get(i).getPriceOfItem();
 					}
 					else
 					{
 						packageQuantity=cartPackageDetails.get(i).getQuantity();
 						//System.out.println(packageQuantity+" from order controller in decrease method");
 						packageItemtotalPrice=cartPackageDetails.get(i).getPriceOfItem();
 					}
 					packageItemtotalPrice=cartPackageDetails.get(i).getPriceOfItem();
 					
 					//Inclusive Tax calculation
 					float itemTax=0;
 					
 					System.out.println("cartPackageDetails.get(i).getTax().getTaxPnct()) "+cartPackageDetails.get(i).getTax().getTaxPnct());
 					
 					itemTax=(packageItemtotalPrice*100)/(100+Float.parseFloat(cartPackageDetails.get(i).getTax().getTaxPnct()));
 					itemTax=packageItemtotalPrice-itemTax;
 					cartPackageDetails.get(i).setTaxAmount(itemTax);
 					//System.out.println(itemTax+" itemtax from order controller in decrease method");
 				}
 			}
 		}
 		float totalTaxOfItems=0;
 		if(cartPackageDetails!=null && cartPackageDetails.size()>0)
 		{
 			for(int i=0;i<cartPackageDetails.size();i++)
 			{
 				garndTotalOfItems=garndTotalOfItems+cartPackageDetails.get(i).getPriceOfItem();
 				totalItemQuantity=totalItemQuantity+cartPackageDetails.get(i).getQuantity();
 				totalTaxOfItems=totalTaxOfItems+cartPackageDetails.get(i).getTaxAmount();
 			}
 		}
 		session.setAttribute("garndTotalOfItems", garndTotalOfItems+"");
 		session.setAttribute("totalItemQuantity", totalItemQuantity+"");
 		session.setAttribute("cartPackageDetails", cartPackageDetails);
 		System.out.println(packageId+"packageId");
 		String [] packageDetailsWithGrandTotal={packageId+"",PackageName,packageItemtotalPrice+"",packagePrice+"",packageQuantity+"",garndTotalOfItems+"",""+totalItemQuantity};
 		Gson json=new Gson();
 		return json.toJson(packageDetailsWithGrandTotal);
 	}     
     
  
    @RequestMapping(value = "/getCountOfItems", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody String getCountOfItems() 
    {
		 logger.info("::::::::::Order Controller::getCountOfItems:::::::::::::::");
		 ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		 HttpSession session = request.getRequest().getSession();
		 List<CartOrder> cartDetailsList=(List<CartOrder>) session.getAttribute("cartDetails");
		 String cartSize=null;
		 if(cartDetailsList!=null && cartDetailsList.size()>0)
		 {
	        cartSize=cartDetailsList.size()+"";
		 }
	     String userLogin=(String) session.getAttribute("sessionLoginId");
	     //System.out.println("user login id form checkoutcart " +userLogin);
	     Gson json=new Gson();
	     String []arr={cartSize,userLogin};
		return json.toJson(arr);
	}
}
