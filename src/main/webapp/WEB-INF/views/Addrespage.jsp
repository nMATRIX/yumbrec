<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<%@ include file="header.jsp" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/slick.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/slick-theme.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/style.css" rel="stylesheet"> <!--- User Style Sheet ---->	
    <link href="${pageContext.servletContext.contextPath}/resources/css/external.css" rel="stylesheet"> <!--- Other Style Sheet ---->	
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> <!--- Other Style Sheet ---->	
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/slick.min.js"></script>
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jssor.slider.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	
	<script>
		
	</script>
	<script type="text/javascript">
	
	function getSubAreasforsub(){
		var areaId=$("#areaIdforsub").val();
		
		$('#subAreaIdforsub').empty();
		var url = "${pageContext.request.contextPath}/getSubAreas";
	       $.ajax({  
	        type : "POST",   
	        url : url, 
	        datType: "json",
	        ContentType: "application/json; charset=utf-8",
	        data : "areaId="+areaId,
	          success : function(response) 
	          {  
	        	alert(response +" from get sub areas");
	          var options = $('#subAreaIdforsub');
	          options.append('<option selected="selected" value="0">Select Sub Area</option>');
	                  $.each(response, function ()
	                		  {
	                            options.append($('<option/>').val(this.id).text(this.subAreaDesc));
	                         });
	         },  
	          error : function(xhr, status, textError) 
	          {
	           alert('Error :::::' + xhr.responseText + ' - ' + status + ' - ' + textError);   
	          }  
	         });
	}
	
	function getpincode()
	{
		   $('#pincodeId').autocomplete({
			     source: function (request, response)
			     {
			               $.getJSON("${pageContext.request.contextPath}/vendors/getPincode", 
			            		   {
			                          term: request.term
			                       }, response);
			           }
			    });
	}
	
	function addresstype(e)
	{
	$("#addressTypeeId").val(e);
	$('#myModal').modal('show');
	}
	
	
	 function addresstype1(e)
	 {
		 var url = window.location.href; 
		 var splits=url.split("=")[1];
		 var mealtimeId=splits;
		 var address = e.split("_");
		 var addressType=address[0];
		 var id=address[1];
		  if(id>0)
		  {
		     window.location="${pageContext.servletContext.contextPath}/choose?addrssId="+id+"&mealtimeId="+mealtimeId;
		  }
		  else
		  {
		   if(addressType=='new')
		   {
		    $("#enterNewAddress").show();
		   }
		   if(addressType=='home'){
		    $("#enterHomeAddress").show();   
		   }
		   if(addressType=='office'){
		    $("#enterOfficeAddress").show();
		   }
		  }
		 }
	
	</script>
  </head>
  <body>
  
  
  
  
	
	<!---- Content Part ---->
	<div>
		<div class="address-page p-tb30">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
						<div class="f-size20 color-green f-bold text-capitalize">Choose address for all orders</div>
					</div>
					<div class="col-lg-offset-1 col-lg-10 col-lg-offset-1 col-md-offset-1 col-md-10 col-md-offset-1 col-sm-12">
						<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
							<div class="panel panel-default m-top30">
								<div class="panel-heading">
									<div class=""><i class="fa fa-home color-fff"></i></div>
									<div class="f-size16 color-fff">Home</div>
								</div>
								<div class="panel-body">
									<div class="addressDescription" id="addressDescriptionForHome"><!-- readonly="readonly" -->
								<span id="enterHomeAddress" style="display:none">Please enter Address</span>
<%-- 								 <c:if test="${not empty model.homeAddress.houseNo}" > --%>
<%-- 								${model.homeAddress.houseNo},</c:if> --%>
								 <c:if test="${not empty model.homeAddress.laneDesc}" >
								${model.homeAddress.laneDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.homeAddress.landMrk}" >
								${model.homeAddress.landMrk},</c:if>
								 <c:if test="${not empty model.homeAddress.area.areaDesc}" >
								${model.homeAddress.area.areaDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.homeAddress.city.cityDesc}" >
								${model.homeAddress.city.cityDesc}-</c:if>
								 <c:if test="${not empty model.homeAddress.pincode.pincodeDesc}" >
								${model.homeAddress.pincode.pincodeDesc}.</c:if>
							</div>
									<a class="text-decnone edit-address" onclick="addresstype(1);"><i class="fa fa-edit"></i></a>
									  <button type="button" class="btn"  onclick="addresstype1('home_${model.homeAddress.id}')"><i class="fa fa-check-circle fa-lg"></i> Choose</button>
									
								</div>
<%-- 								<div class="panel-footer"><a href="${pageContext.servletContext.contextPath}/choose?addrssId=${model.homeAddress.id}" class="btn btn-default"><i class="fa fa-check p-right10"></i>Choose</a></div> --%>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
							<div class="panel panel-default m-top30">
								<div class="panel-heading">
									<div class=""><i class="fa fa-building-o color-fff"></i></div>
									<div class="f-size16 color-fff">Office</div>
								</div>
								<div class="panel-body">
									<div class="addressDescription" id="addressDescriptionForOffice">
							<span id="enterOfficeAddress" style="display:none">Please enter Address</span>
<%-- 							 <c:if test="${not empty model.officeAddress.houseNo}" > --%>
<%-- 								${model.officeAddress.houseNo},</c:if> --%>
								 <c:if test="${not empty model.officeAddress.laneDesc}" >
								${model.officeAddress.laneDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.officeAddress.landMrk}" >
								${model.officeAddress.landMrk},</c:if>
								 <c:if test="${not empty model.officeAddress.area.areaDesc}" >
								${model.officeAddress.area.areaDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.officeAddress.city.cityDesc}" >
								${model.officeAddress.city.cityDesc}-</c:if>
								 <c:if test="${not empty model.officeAddress.pincode.pincodeDesc}" >
								${model.officeAddress.pincode.pincodeDesc}.</c:if>
							
							</div>
									<a class="text-decnone edit-address" onclick="addresstype(2);"><i class="fa fa-edit"></i></a>
									
									  <button type="button" class="btn"  onclick="addresstype1('office_${model.officeAddress.id}')"><i class="fa fa-check-circle fa-lg"></i> Choose</button>
								</div>
<%-- 								<div class="panel-footer"><a href="${pageContext.servletContext.contextPath}/choose?addrssId=${model.officeAddress.id}" class="btn btn-default"><i class="fa fa-check p-right10"></i>Choose</a></div> --%>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
							<div class="panel panel-default m-top30">
								<div class="panel-heading">
									<div class=""><i class="fa fa-university color-fff"></i></div>
									<div class="f-size16 color-fff">Other</div>
								</div>
								<div class="panel-body">
									<div class="addressDescription" id="addressDescriptionForNew">
							<span id="enterNewAddress" style="display:none">Please enter Address</span>
<%-- 							 <c:if test="${not empty model.newAddress.houseNo}" > --%>
<%-- 								${model.newAddress.houseNo},</c:if> --%>
								 <c:if test="${not empty model.newAddress.laneDesc}" >
								${model.newAddress.laneDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.newAddress.landMrk}" >
								${model.newAddress.landMrk},</c:if>
								 <c:if test="${not empty model.newAddress.area.areaDesc}" >
								${model.newAddress.area.areaDesc},</c:if>
								<br/>
								 <c:if test="${not empty model.newAddress.city.cityDesc}" >
								${model.newAddress.city.cityDesc}-</c:if>
								 <c:if test="${not empty model.newAddress.pincode.pincodeDesc}" >
								${model.newAddress.pincode.pincodeDesc}.</c:if>								
								
							</div>
									<a class="text-decnone edit-address" onclick="addresstype(3);"><i class="fa fa-edit"></i></a>
									
								 <button type="button" class="btn"  onclick="addresstype1('new_${model.newAddress.id}')"><i class="fa fa-check-circle fa-lg"></i> Choose</button>	
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>			
		</div>	
	</div>
	<!---- Content Part ---->	
	
	<!------ Edit Address ---->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit Address</h4>
		  </div>
		 
		  <div class="modal-body">
			<form:form method="post" action="${pageContext.servletContext.contextPath}/addressedit"  modelAttribute="user"  class="edit-address" data-toggle="validator" role="form" >
				<ul class="row p-all0 list-none">
					<li class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group required">
							<label for="register-password" class="text-capitalize">select city
							<sup><i class="fa fa-asterisk imput-imp"></i></sup>
							</label>
							 <form:input path="addressTypeId" type="text" value="" id="addressTypeeId"  style="display:none"/>  
							<form:select path="city.id" id="cityId" required="required">                                    
									<form:option value="0">Select City</form:option>
									 <c:forEach var="city" items="${model.cityList}" varStatus="loop">
									<form:option value="${city.id}">${city.cityDesc}</form:option>
									</c:forEach>
							</form:select>
						</div>
					</li>
					<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group required">
							<label for="register-password" class="text-capitalize">select area
							<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
							<form:select path="area.id" class="" id="areaIdforsub" onchange="getSubAreasforsub();" required="required"  >                                    
													<form:option value="0">Select Area</form:option>
													 <c:forEach var="area" items="${model.arealist}" varStatus="loop">
													<form:option value="${area.id}">${area.areaDesc}</form:option>
													</c:forEach>
												</form:select>
						</div>
					</li>
					<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group required">
							<label for="register-password" class="text-capitalize">select sub area
							<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
							
							<form:select path="subArea.id" id="subAreaIdforsub" class="form-control m-tb10">
													<form:option value="0" >Sub Area</form:option>
													
												</form:select>
							
						</div>
					</li>
					<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group required">							
							<label for="register-password" class="text-capitalize">Pincode
							<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
					            <form:input path="pincodeDetails.pincodeDesc" type="number" id="pincodeId" min="000000" max="999999" name="pincodeDetails.pincodeDesc" class="form-control" placeholder="Enter Pincode Here" onfocus="getpincode()"/>
						</div>
					</li>
					<li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group required">
							<label for="register-password" class="text-capitalize">Enter address
							<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
							<form:textarea path="landmark" class="" rows="4"></form:textarea>
						</div>
					</li>
					<li>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success " >Edit</button>
						</div>
					</li>
				</ul>
			</form:form>	
		  
		  </div>
		</div>

		</div>
		</div>
  </body>
</html>