<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<%@include file="header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec Contact Us</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script>
		 $(document).ready(function () {
			$('#datePick').multiDatesPicker();
		});
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
  </head>
  
  
  
  <body>
	<!---- content part --->
	<div>
		<div class="contactus">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 colxs-12 text-center m-bottom10 text-capitalize color-theme">
						<h3>Contact US</h3>
						<hr />
						<div class="color-000 text-capitalize"><h3>We'd <i class="fa fa-heart-o"></i> to Serve</h3></div>
						<div class="f-size12 color-000">Enjoy The Fresh Food Through Out The Mouth</div>
					</div>
					<div class="col-lg-offset-1 col-lg-5 col-md-6 col-sm-12 col-xs-12 m-top25">
							<div class="color-theme f-size18 m-bottom10 text-capitalize">contact form<hr /></div>
						<form data-toggle="validator" role="form">
							<div class="form-group">
								<label for="inputName" class="control-label">Name</label>
								<input type="text" class="form-control input-form" id="inputName" placeholder="First Name" required>
							</div>
							<div class="form-group">
								<label for="inputName" class="control-label">Mobile Number</label>
								<input type="text" class="form-control input-form" id="inputName" placeholder="Mobile Number" required>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="control-label">Email</label>
								<input type="email" class="form-control input-form" id="inputEmail" placeholder="Email" data-error="Bruh, that email address is invalid" required>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="control-label">Area</label>
								<input type="text" class="form-control input-form" id="inputEmail" placeholder="Area" data-error="Select Area" required>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="control-label">Area</label>
								<textarea class="form-control input-form" data-error="Select Area" rows="5" cols="50" required></textarea>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary f-size18 pull-right">Submit</button>
							</div>
						</form>
					</div>					
					<div class="col-lg-offset-1 col-lg-4 col-md-6 col-sm-12 col-xs-12 m-top25">
						<ul class="p-all0 list-none">  
							<li class="color-theme f-size18 m-bottom10 text-capitalize">Address<hr /></li>
							<li>
								<div class="d-table">
									<div class="d-tbcell"><i class="fa fa-map-marker"></i></div>
									 &nbsp;  &nbsp; <div class="d-tbcell">
										<ul class="p-all0 list-none">
											<li>Plot No.18,2nd Floor,</li>
											<li>Opposite GHMC Park,</li>
											<li>ICRISAT Colony, Madinaguda,</li>
											<li>Hyderabad, Telangana 500050.</li>
										<ul>
									</div>
								</div>
							</li>  
							<li>
								<div class="d-table">
									<div class="d-tbcell v-middle"><i class="fa fa fa-phone"></i></div>
									<div class="d-tbcell v-middle">
										 &nbsp; 01-154-258-2056
									</div>
								</div>
							</li>   
							<li>
								<div class="d-table">
									<div class="d-tbcell v-middle"><i class="fa fa fa-envelope-o"></i></div>
									<div class="d-tbcell v-middle">
										 &nbsp; Reach Us here  : nibbilematrix.com
									</div>
								</div>
							</li>                                   
						</ul>
						<hr />
						<div><iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15221.071583532432!2d78.33301555!3d17.4947206!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1454575469188" frameborder="0" style="border:0" allowfullscreen></iframe></div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	<!---- content part --->
  
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
	
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>