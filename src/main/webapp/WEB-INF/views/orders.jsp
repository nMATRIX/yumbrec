<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page session="true"%>
<%@ include file="header.jsp" %>
<jsp lang="en" class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Yumbrec my orders</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- Bootstrap -->
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/ex-css.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/component.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet">
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap-datetimepicker.min.css">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Signika:300,400,700,900' rel='stylesheet' type='text/css'>
    
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-2.1.4.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery.js"></script>
</head>


<body>

	<div class="wrapper">
		<section id="mysb" style="padding-bottom: 0;">
			<div class="container">
				<div class="section-heading scrollpoint sp-effect3">
					<h1 class="color-fff">My Orders</h1>
				</div>
				<div>
				<h4><font color="red">${successOrder}</font></h4>
				</div>
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-3 scrollpoint sp-effect1">
							<div class="table-responsive">
								<table class="table walletMoney">
									<tr>
										<td>
											<h3 class="media-heading">Wallet Money : Rs. 50</h3>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"
							style="float: right">
							<button type="button" class="btn btn-default btn-lg"
								id="ChangePassword" style="display: inline" data-toggle="modal"
								data-target="#changePasswordModal">Change Password</button>
						</div>
					</div>
				</div>
 -->
<!-- 				<div id="changePasswordModal" class="modal fade"> -->
<!-- 					<div class="modal-dialog modal-sm"> -->
<!-- 						<div class="modal-content"> -->
<!-- 							<div class="modal-header"> -->
<!-- 								<button type="button" class="close" data-dismiss="modal" -->
<!-- 									aria-hidden="true">�</button> -->
<!-- 								<h4 class="modal-title">Change Password</h4> -->
<!-- 							</div> -->
<!-- 							<div class="modal-body"> -->
<%-- 								<form> --%>
<!-- 									<div class="form-group"> -->
<!-- 										<label for="oldPassword">Old Password:</label> <input -->
<!-- 											type="password" class="form-control" id="oldPassword" -->
<!-- 											required="required" /> <label for="newPassword">New -->
<!-- 											Password:</label> <input type="password" class="form-control" -->
<!-- 											id="newPassword" required="required" /> <label -->
<!-- 											for="confirmPassword">Confirm Password:</label> <input -->
<!-- 											type="password" class="form-control" id="confirmPassword" -->
<!-- 											required="required" /> -->
<!-- 									</div> -->
<!-- 									<button type="button" data-dismiss="modal" aria-hidden="true" -->
<!-- 										class="btn btn-success btn-block">Submit</button> -->
<!-- 									<button type="button" data-dismiss="modal" aria-hidden="true" -->
<!-- 										class="btn btn-danger btn-block">Cancel</button> -->

<%-- 								</form> --%>
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->

				
				<div class="row">
					<div class="col-md-12 scrollpoint sp-effect1">
						<div class="table-responsive">
							<table class="orderDescription table table-striped">
								<thead>
									<tr>
										<th><h4>
												<font style="color:green">Bill No.</font>
											</h4></th>
											<th><h4>
												<font style="color:green">Order No.</font>
											</h4></th>
											 <th><h4>
												<font style="color:green">PackageName</font>
											</h4></th>
										<th><h4>
												<font style="color:green">Week Type</font>
											</h4></th>
<!-- 										<th><h4> -->
<!-- 												<font color="#FFF">Duration</font> -->
<!-- 											</h4></th> -->
										<th><h4>
												<font style="color:green">Quantity</font>
											</h4></th>
										<th><h4>
												<font style="color:green">Duration</font>
											</h4></th>
										<!-- <td><h4>
												<font color="#FFF">Order Date</font>
											</h4></td> -->
										<td><h4>
												<font style="color:green">Delv. Start</font>
											</h4></td>
										<td><h4>
												<font style="color:green">Delv. End</font>
											</h4></td>
										<th><h4>
												<font style="color:green">Amount</font>
											</h4></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="9" id="paidOrders" >Paid orders</td>
									</tr>	
									
										<c:forEach var="billWiseList" items="${model.paidOrdersList}">
										<c:if test="${billWiseList.breakFastOrdersList.size()>0}">
										<c:forEach var="breakfast" items="${billWiseList.breakFastOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${breakfast.orderId}</td>
												<td>${breakfast.pkgName}</td>
												
													<td>${breakfast.weekTypDesc}</td>
													<td>${breakfast.pkgQuantity}</td>
													<td>${breakfast.pkgDurDescription}</td>
											
												<td>${breakfast.orderStartDate}</td>
												<td>${breakfast.orderEndDate}</td>
													
													<td>${breakfast.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										
								<c:if test="${billWiseList.lunchOrdersList.size()>0}">
										<c:forEach var="lunchOrders" items="${billWiseList.lunchOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${lunchOrders.orderId}</td>
												<td>${lunchOrders.pkgName}</td>
												
													<td>${lunchOrders.weekTypDesc}</td>
													<td>${lunchOrders.pkgQuantity}</td>
													<td>${lunchOrders.pkgDurDescription}</td>
											
												<td>${lunchOrders.orderStartDate}</td>
												<td>${lunchOrders.orderEndDate}</td>
													
													<td>${lunchOrders.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										
										
										<c:if test="${billWiseList.dinnerOrdersList.size()>0}">
										<c:forEach var="dinnerOrders" items="${billWiseList.dinnerOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${dinnerOrders.orderId}</td>
												<td>${dinnerOrders.pkgName}</td>
												
													<td>${dinnerOrders.weekTypDesc}</td>
													<td>${dinnerOrders.pkgQuantity}</td>
													<td>${dinnerOrders.pkgDurDescription}</td>
											
												<td>${dinnerOrders.orderStartDate}</td>
												<td>${dinnerOrders.orderEndDate}</td>
													
													<td>${dinnerOrders.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										</c:forEach>
										<tr>
										<td colspan="9" id="paidOrders" >UnpaidOrdersList orders</td>
									</tr>	
									
										<c:forEach var="billWiseList" items="${model.unPaidOrdersList}">
										
										<c:if test="${billWiseList.breakFastOrdersList.size()>0}">
										<c:forEach var="breakfast" items="${billWiseList.breakFastOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${breakfast.orderId}</td>
												<td>${breakfast.pkgName}</td>
												
													<td>${breakfast.weekTypDesc}</td>
													<td>${breakfast.pkgQuantity}</td>
													<td>${breakfast.pkgDurDescription}</td>
											
												<td>${breakfast.orderStartDate}</td>
												<td>${breakfast.orderEndDate}</td>
													
													<td>${breakfast.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										
								<c:if test="${billWiseList.lunchOrdersList.size()>0}">
										<c:forEach var="lunchOrders" items="${billWiseList.lunchOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${lunchOrders.orderId}</td>
												<td>${lunchOrders.pkgName}</td>
												
													<td>${lunchOrders.weekTypDesc}</td>
													<td>${lunchOrders.pkgQuantity}</td>
													<td>${lunchOrders.pkgDurDescription}</td>
											
												<td>${lunchOrders.orderStartDate}</td>
												<td>${lunchOrders.orderEndDate}</td>
													
													<td>${lunchOrders.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										<c:if test="${billWiseList.dinnerOrdersList.size()>0}">
										<c:forEach var="dinnerOrders" items="${billWiseList.dinnerOrdersList}">
											<tr>
												<td>${billWiseList.billId}</td>
												<td>${dinnerOrders.orderId}</td>
												<td>${dinnerOrders.pkgName}</td>
												
													<td>${dinnerOrders.weekTypDesc}</td>
													<td>${dinnerOrders.pkgQuantity}</td>
													<td>${dinnerOrders.pkgDurDescription}</td>
											
												<td>${dinnerOrders.orderStartDate}</td>
												<td>${dinnerOrders.orderEndDate}</td>
													
													<td>${dinnerOrders.pkgAmount}</td>
												
											</tr>
											</c:forEach>
										</c:if>	
										</c:forEach>
<!-- 									<tr> -->
										<!-- <td>&nbsp;</td>
										<td colspan="8"><b>Delivery Address : </b>Address
											Description,Address Line 1,Address Line 2....</td> -->
<!-- 									</tr> -->
									
									
										<!-- <td>&nbsp;</td>
										<td colspan="8"><b>Delivery Address : </b>Address
											Description,Address Line 1,Address Line 2....</td> -->
<!-- 									</tr> -->

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</section>
		<footer>
			
		</footer>

	</div>



	<script src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/slick.min.js"></script>
    <%-- <script src="${pageContext.servletContext.contextPath}/resources/js/placeholdem.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/waypoints.min.js"></script> --%>
<%--     <script src="${pageContext.servletContext.contextPath}/resources/js/scripts.js"></script>    --%>

<script type="text/javascript">
$(document).ready(function(){

	
	$("#checkOutCart").click(function(){
		
		
		$("#checkIdFromCartOrLogin").val(1);
		
		var url = "${pageContext.servletContext.contextPath}/getCountOfItems";
		  $.ajax({
		 		type : "POST",
		 		url : url,
		 		datType : "json",
		 		ContentType : "application/json; charset=utf-8",
		 		data :{} ,
		 		success : function(newResponse) {
// 						alert(newResponse);				
		 			if((newResponse[0]==null   || newResponse[0]=='0' || newResponse[0]=='') || (newResponse[1]==null || newResponse[1]=='0' || newResponse[1]=='')){
//			 			alert("get count"+newResponse);
//	 	 			$('#myCarousel').carousel(3);
		 			homepagePage();
		 		}
		 		else{
		 			checkoutPage();
		 		}
		 		
		 		
		 		
		 			
		 		     },
		 		error : function(xhr, status, textError) {
//			 			alert(status);
	 		        alert('Error :::::' + xhr.responseText + ' - ' + status + ' - ' + textError);   
	 		       }
		 		    });
		
		
	 });
		
	 
	
});


function homepagePage(){
//	 alert("checkoutPage");	 
	 window.location="${pageContext.servletContext.contextPath}/";
} 
function checkoutPage(){
//	 alert("checkoutPage");	 
	 window.location="${pageContext.servletContext.contextPath}/checkout";
	 
	 
} 

</script>




</body>

</jsp>