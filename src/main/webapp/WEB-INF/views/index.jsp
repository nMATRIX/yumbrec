<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="header.jsp" %>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script>
		/*  $(document).ready(function () {
			$('#ordDateId').multiDatesPicker();
		}); */
		
		$(document).ready(function () {
	      	$('#mealTimeId .mealtime-btn').on('click', function ()
	      			{
	      		$('#messageForAddToCart').hide();
	      	});
	      });
		
		
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
    
    
    <script type="text/javascript">
    
    function reload()
    {
    	location.reload();
    }
    
    function packageIdDetails(pkgid)
    {
    	
    	//alert(pkgid);
    	//val means we are appending the packageid value to hiddenfeild
    	$("#packageId").val(pkgid);
    }
    
    function addToCart()
    {
    	
    	var mealTimeId=''; 
    	
    	mealTimeId=$("#mealTimeId input[type='radio']:checked").val();
    	//alert("mealTimeId "+mealTimeId);
    	var durationId=''; 
    	durationId=$("#durationId input[type='radio']:checked").val();
    	//alert("durationId "+durationId);
    	var weekDaysId='';
    		weekDaysId=$("#weekDaysId input[type='radio']:checked").val();
        //alert("weekDaysId "+weekDaysId);	
    	
    	var packageId='';
    	packageId=$("#packageId").val();
    	//alert(packageId);
    	
    	var ordDateId='';
    	ordDateId=$("#ordDateId").val();
    	//alert(ordDateId);

    	var status=true;
    	
    	if(durationId!='')
    	{
    		if(durationId=='1')
    		{
    			weekDaysId='0';
    		}
    	}

    	if(mealTimeId!='' && mealTimeId!=null && durationId!='' && durationId!=null  && weekDaysId!='' && weekDaysId!=null && packageId!='' && packageId!=null && ordDateId!='' && ordDateId!=null  )
    	{
    	if(mealTimeId=='1')
    		{
    		    $("#dinnerCutOffTime").hide();
    		    $("#lunchCutOffTime").hide();
    		    $("#messageForAddToCart").text('This Lunch Item is already added to cart please select another mealtime');
    		    $("#messageForAddToCartLunch").show();
    		    $("#messageForAddToCartDinner").hide();
    		} else if(mealTimeId=='2')
    		{
    			$("#dinnerCutOffTime").hide();
    		    $("#lunchCutOffTime").hide();
    			$("#messageForAddToCart").text('This Dinner Item is already added to cart please select another mealtime');
    			$("#messageForAddToCartDinner").show();
    			$("#messageForAddToCartLunch").hide();
    			
    		} 
    		
    		var url = "${pageContext.servletContext.contextPath}/checkOrderDate";
    	     $.ajax({
    	 		type : "POST",
    	 		url : url,
    	 		ContentType : "application/json; charset=utf-8",
    	 		data :{ordDateId:ordDateId,mealTimeId:mealTimeId} ,
    	 		success : function(response) 
    	 		{
    	 			//alert(response +" checkOrderDate response");
    	 			if(response=="false")
    	 			{
    	 			
    	 				status=false;
    	 				if(mealTimeId=='1')
    	 					{
    	 					//alert("lunch nahi");
    	 					$("#messageForAddToCart").text("");
    	 					$("#lunchCutOffTime").show();
    	 					$("#dinnerCutOffTime").hide();
    	 					}
    	 				else
    	 					{
    	 					$("#messageForAddToCart").text("");
    	 					    $("#lunchCutOffTime").hide();
    	 						$("#dinnerCutOffTime").show();
    	 					}
    	 			}
    	 			else
    	 			{  
    	 				//alert("add to cart")
    	 				var url = "${pageContext.servletContext.contextPath}/addToCart";
    	 			     $.ajax({
    	 			 		type : "POST",
    	 			 		url : url,
    	 			 		ContentType : "application/json; charset=utf-8",
    	 			 		data :{packageId:packageId,ordDateId:ordDateId,weekDaysId:weekDaysId,durationId:durationId,mealTimeId:mealTimeId} ,
    	 			 		success :
    	 			 			function(response) 
    	 			 		     {
    	 			 			   if(response=="no")
    	 			 			    {
    	 			 				  $("#messageForAddToCart").show();
    	 			 			    }
    	 			 			    else
    	 			 			    {  
    	 			 				         //alert(response);
    	 			 				         var your_data = JSON.parse(response);
    	 			 				         //alert(your_data);
    	 			 				         
    	 			 				     $("#summeryId").append("<li><div class='order-lists'>"
											+"<div class='d-table m-bottom10'>"
												+"<div id='standardMealid' class='d-tbcell v-middle text-left meal-title text-capitalize'><span id='standardMealid' style='color:green'>"+your_data[4]+"</span></div>"
												+"<div class='d-tbcell v-middle'></div>"
											+"</div>"
											+"<div class='d-table m-bottom10'>"
												+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>Price</div>"
												+"<div class='d-tbcell v-middle'><span id='mealTimeSizeId'>"+your_data[6]+"</span></div>"
											+"</div>"
											/* +"<div class='d-table m-bottom10'>"
											+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>quantity</div>"
											+"<div class='d-tbcell v-middle'><span id='mealTimeSizeId'>"+your_data[5]+"</span></div>"
										    +"</div>" */
											+"<div class='d-table m-bottom10'>"
												+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>Meal type</div>"
												+"<div id='mealTimeSizeId' class='d-tbcell v-middle text-capitalize'><span id='mealTimeSizeId'>"+your_data[0]+"</span></div>"
											+"</div>"
											+"<div class='d-table m-bottom10'>"
												+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>Meal duration</div>"
												+"<div id='mealDurationsizeId' class='d-tbcell v-middle text-capitalize'><span id='mealDurationsizeId'>"+your_data[1]+"</span></div>"
											+"</div>"
											+"<div class='d-table m-bottom10'>"
												+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>Week Days</div>"
												+"<div id='weekdaysSizeId' class='d-tbcell v-middle text-capitalize'><span id='weekdaysSizeId'>"+your_data[2]+"</span></div>"
											+"</div>"
											+"<div class='d-table'>"
												+"<div class='d-tbcell v-middle width-meal text-left text-capitalize'>Start Date</div>"
												+"<div id='startdatesizeId' class='d-tbcell v-middle'><span id='startdatesizeId'>"+your_data[3]+"</span></div>"
											+"</div>"
										+"</div></li>");
     	 			 				
     	 			 				         $("#cartSizeErrorMsg").empty();
     	 			 				         $("#validAllSelection").hide();
     	 			 				         $('#cartSizeIdWithItems').show();
    	 			 				/* $('#mealTimeSizeId').replaceWith($('#mealTimeSizeId').html(your_data[0]));
    	 			 				$('#mealDurationsizeId').replaceWith($('#mealDurationsizeId').html(your_data[1]));
    	 			 				$('#weekdaysSizeId').replaceWith($('#weekdaysSizeId').html(your_data[2]));
    	 			 				$('#startdatesizeId').replaceWith($('#startdatesizeId').html(your_data[3]));
    	 			 				$('#standardMealid').replaceWith($('#standardMealid').html(your_data[4])); */
    	 			 				
//     	 			 				$("#mealTimeSizeId").empty();
//     	 			 				$("#mealDurationsizeId").empty();
//     	 			 				$("#weekdaysSizeId").empty();
//     	 			 				$("#startdatesizeId").empty();
    	 			 				
    	 			 				$("#cartSizeIdWithOutItems").empty();
    	 			 				$("#carticonsummeryid").append();
    	 			 				$("#cartSizeIdWithItems").empty(your_data[5]);
    	 			 				$("#cartSizeIdWithItems").append(your_data[5]); 
    	 			 			   
    	 			 			  }
    	 			 		     },
    	 			 		error : function(xhr, status, textError) {//alert("package not added"); 
    	 			 		}
    	 			 		    });
    	 		    	}
    	 		     },
    	 		error : function(xhr, status, textError) { }
    	 		    });
    		}
    	   else
    	    {
    		    $("#cartSizeErrorMsg").empty();
    			$("#validAllSelection").show();
    		}
    	}

    function check()
    {
    	var url = "${pageContext.servletContext.contextPath}/checkLoginStatus";
         $.ajax({
     		type : "GET",
     		url : url,
     		//datType : "json",
     		//ContentType : "application/json; charset=utf-8",
     		//data :{} ,
     		success : function(response)
     		{
     			if(response[0]!=null&&response[1]!=null)
     			{
     				    window.location = "${pageContext.servletContext.contextPath}/addresspage"
     			}
     			else if(response[0]==null&&response[1]!=null)
     			{
     				window.location = "${pageContext.servletContext.contextPath}/userlogin"
     			}
     			else
     			{
     				$("#cartSizeErrorMsg").css("color","red");
     				$("#cartSizeErrorMsg").text("Please select order");
     			}
     			alert("please select the order");
    		 },
    		error : function(xhr, status, textError) 
    		{
//    	        alert('Error :::::' + xhr.responseText + ' - ' + status + ' - ' + textError);   
           }
    		    });
    }

    function mymessage() 
    {
		alert("please select the order");
    	$("errormsgfororder").show();
	}
    </script>
  </head>
  
  <body>
	<!---- content part --->
	<div>
		<div class="order-itemdispaly">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="order-itemdispalyinner">
							<div class="enjoy-food color-fff f-bold text-capitalize"><h3> enjoy the fresh food through out the mouth</h3></div>
						</div>	
					</div>	
				</div>	
			</div>	
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="book-monthly f-size18 color-fff text-capitalize">book your monthly meals now</div>
					</div>				
				</div>				
			</div>
			 <div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<div class="item-blocks">
							<div id="links">
							    <c:forEach var="packageDetails" items="${model.allpackageDetails}" >
						          <a href="#" id="btn_02" class="tog flip-images" onclick="packageIdDetails(${packageDetails.packageId})"> 
						              <img id="${packageDetails.packageId}"   src="${pageContext.servletContext.contextPath}/resources/images/standardmeal.png" class="img-responsive" >${packageDetails.packageName}<%--  <c:if test="${sessionScope.cartSize!=null}"> ${packageDetails.price.} </c:if> --%>
						          </a> 
				                </c:forEach>
							</div>
						</div>
					</div>
				</div>
			</div>  
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div id="panel">
						<button id="close"><i class="fa fa-remove"></i></button>
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								
								<%-- <c:if test="${sessionScope.selectorder!=null}">sessionScope.selectorder<div onunload="mymessage()"> </div></c:if>
								<span id="errormsgfororder" style="display: none">Please select the order</span> --%>
									<div class="order-meals">
									  <span id="cartSizeErrorMsg"></span>	
				                      <!-- <span id="messageForAddToCartLunch" style="display: none">This Lunch Item is already added to cart please select another mealtime</span>
				                      <span id="messageForAddToCartDinner" style="display: none">This Dinner Item is already added to cart please select another mealtime</span> -->
			                          <div id="messageForAddToCart" class="color-red error-msg f-size18" style="display:none">This Meal Time is already added to cart please select another mealtime</div>
		                             <span id="lunchCutOffTime"  style="display:none">Lunch time is completed</span>
		                              <span id="dinnerCutOffTime"  style="display:none">Dinner time is completed</span>
			                          <div id="validAllSelection" class="color-red error-msg f-size18" style="display:none">Please Select All Combinations</div>
									  <input type="hidden" id="packageId">
									<span style="color:red">${ sessionScope.selectorder}</span>
									 <div id="mealTimeId" class="btn-group d-table m-bottom10" data-toggle="buttons">	
											<div class="d-tbcell v-middle order-title color-000 text-left">Meal time :</div>	
											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input id="mealTime" name="mealTime" type="radio" autocomplete="off" value="1" checked> Lunch
												   <img class="pull-right" src="${pageContext.servletContext.contextPath}/resources/images/Lunch2.png" />
											</label>

											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="mealTime" name="mealTime" autocomplete="off" value="2" checked>Dinner
												<img class="pull-right" src="${pageContext.servletContext.contextPath}/resources/images/Dinner2.png" />
											</label>
										</div>
										
										<div id="durationId" class="btn-group d-table m-bottom10" data-toggle="buttons">	
											<div class="d-tbcell v-middle order-title color-000 text-left">Meal Duration :</div>	
											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="packageDuration" name="pakageDurationName" autocomplete="off" value="1"  checked> Trial 1 day
											</label>

											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="packageDuration" name="pakageDurationName" autocomplete="off" value="2">1 Week
											</label>

											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="packageDuration" name="pakageDurationName" autocomplete="off" value="3">2 Weeks
											</label>

											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="packageDuration" name="pakageDurationName" autocomplete="off" value="4">1 Month
											</label>
										</div>
										
										<div id="weekDaysId" class="btn-group d-table m-bottom10" data-toggle="buttons">	
											<div class="d-tbcell v-middle order-title color-000 text-left">Week Days :</div>	
											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="weekType" name="weekTypeDurationName" autocomplete="off"  value="1" checked > MON-FRi
											</label>

											<label class="btn btn-default d-tbcell v-middle mealtime-btn text-uppercase">
												<input type="radio" id="weekType" name="weekTypeDurationName" autocomplete="off" value="2">MON-SAT
											</label>
										</div>
										<div class="d-table">
											<div class="d-tbcell v-middle order-title color-000 text-left">Select Date</div>
											<div class="d-tbcell v-middle">
											<input id="ordDateId" name="ordDate" type="date" />
											</div>
											<div class="add-tocart">
											<button onclick="addToCart();" id="addToCartBtn" class="btn color-fff text-capitalize"><i class="fa fa-cart-plus p-right10"></i>add to cart</button>
											</div>
										</div>	
									</div>
								</div>
								<div class="col-lg-offset-1 col-lg-3 col-md-4 col-sm-12 col-xs-12">
									<div class="order-itemsdetails">
										<div  class="cart-icon"><i id="carticonsummeryid" class="fa fa-cart-plus fa-2x color-fff"></i></div>
											<span class="color-444 f-size16 text-center text-capitalize"><h4>Order Summary</h4></span>						
									       	<div  class="scrollbar" >
									       
 												<div class="order-shows force-overflow" > 
 													<ol id="summeryId" class="p-all0"> 
 													
 											   <c:if test="${sessionScope.cartSize!=null}"> 
 													
 													<c:forEach var="cartOrder" items="${sessionScope.cartDetails}" >
											       <li>
												<div class="order-lists">
													<div class="d-table m-bottom10">
														<div id="startdatesizeId" class="d-tbcell v-middle text-left meal-title text-capitalize"><span>${cartOrder.packageName}</span></div>
													    <div class="d-tbcell v-middle"></div>
													</div>
													<div class="d-table m-bottom10">
													   <div  class="d-tbcell v-middle width-meal text-left text-capitalize">Price</div>
													  <div  id="quantityidId" class="d-tbcell v-middle"><span>${cartOrder.priceOfItem}</span></div>
													</div>
													<div class="d-table m-bottom10">
													   <div  class="d-tbcell v-middle width-meal text-left text-capitalize">quantity</div>
													  <div  id="quantityidId" class="d-tbcell v-middle"><span>${cartOrder.quantity}</span></div>
													</div> 
													<div class="d-table m-bottom10">
														<div  class="d-tbcell v-middle width-meal text-left text-capitalize">Meal type</div>
														<div  id="mealTimeSizeId" class="d-tbcell v-middle text-capitalize"><span>${cartOrder.mealTime.mealTimeDesc}</span></div>
													</div>
													<div class="d-table m-bottom10">
														<div  class="d-tbcell v-middle width-meal text-left text-capitalize">Meal duration</div>
														<div id="mealDurationsizeId" class="d-tbcell v-middle text-capitalize"><span>${cartOrder.packageDuration.pkgDurDesc}</span></div>
													</div>
													<div class="d-table m-bottom10">
														<div class="d-tbcell v-middle width-meal text-left text-capitalize">week days</div>
														<div id="weekdaysSizeId" class="d-tbcell v-middle text-capitalize"><span>${cartOrder.weekType.weekTypDesc}</span></div>
													</div>
													<div class="d-table">
														<div class="d-tbcell v-middle width-meal text-left text-capitalize"></div>
														<div id="startdatesizeId" class="d-tbcell v-middle"><span>${cartOrder.orderStartDate}</span></div>
													</div>
															</div>
														</li>
													</c:forEach>
										            </c:if>
													</ol> 
												</div> 
											</div> 
											<%-- <c:if test="${sessionScope.userId!=null}"> 
											 <div class="proceed-tocart"><button onclick="check()" ="btn-lg f-size14 text-capitalize">proceed to checkout</button></div>
                                            </c:if>	 --%>
                                           <%--  <c:if test="${sessionScope.userId==null}">	 --%>
                                             <div class="proceed-tocart"><a href="${pageContext.servletContext.contextPath}/checkLoginStatus" ><button class="btn-lg f-size14 text-capitalize">proceed to checkout</button></a></div>
                                           <%--  </c:if> --%>
                                            							 
									 </div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!---- content part --->
  
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>