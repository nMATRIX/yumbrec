<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script>
		 $(document).ready(function () {
			$('#datePick').multiDatesPicker();
		});
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
  </head>
  
  
  <body>
  
	<!---- content part --->
	<div>
		<div class="myorder-selection">
			<div class="container">
			<div><h3 class="text-capitalize color-theme text-center">My Orders</h3></div>
			<hr />
			<br />
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead>
						<tr class="meal-selection">
							<th class="width200">Meal Type</th>
							<th>Item </th>
							<th class="width250">Address</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">${billWiseList.dinnerOrdersList.pkgName}</td>
							<td class="p-all0">
								<table class="table table-bordered table-striped nopadding">
									<thead>
										<tr class="item-all">
											<th>Meal type</th>
											<th>Quantity</th>
											<th>Meal Duration</th>
											<th>Price</th>
										</tr>
									</thead>
									<thead>
									<c:if test="${billWiseList.lunchOrdersList.size()>0}">
									  <c:forEach var="lunchOrders" items="${billWiseList.lunchOrdersList}">
										<tr>
											<td>${lunchOrders.orderId}</td>
											<td>${billWiseList.lunchOrdersList.size()}</td>
											<td>${lunchOrders.weekTypDesc}</td>
											<td>${lunchOrders.pkgAmount}</td>
										</tr>
										</c:forEach>
										</c:if>
										<c:if test="${billWiseList.dinnerOrdersList.size()>0}">
										<c:forEach var="dinnerOrders" items="${billWiseList.dinnerOrdersList}">
										<tr>
											<td>${dinnerOrders.orderId}</td>
											<td>${billWiseList.dinnerOrdersList.size()}</td>
											<td>${dinnerOrders.weekTypDesc}</td>
											<td>${dinnerOrders.pkgAmount}</td>
										</tr>
										</c:forEach>
										</c:if>
									</thead>
								</table>
							</td>
							<td>
								<div class="color-444 f-size14">	
									Plot No.18,2nd Floor,
									Opposite GHMC Park,
									ICRISAT Colony, Madinaguda,
									Hyderabad, Telangana 500050.
								</div>
								<a id="select-add" class="pull-right" href="#" data-toggle="modal" data-target="#choose-address" data-backdrop="static" data-keyboard="false"><i class="fa fa-edit"></i></a>
							</td>
						</tr>
						<tr>
							<td class="text-center">Healthy Meal</td>
							<td class="p-all0">
								<table class="table table-bordered table-striped nopadding">
									<thead>
										<tr class="item-all">
											<th>Meal type</th>
											<th>Quantity</th>
											<th>Meal Duration</th>
											<th>Price</th>
										</tr>
									</thead>
									<thead>
										<tr>
											<td>Lunch</td>
											<td>3</td>
											<td>1 Week</td>
											<td>1000</td>
										</tr>
										<tr>
											<td>Dinner</td>
											<td>3</td>
											<td>1 Week</td>
											<td>1000</td>
										</tr>
									</thead>
								</table>
							</td>
							<td>
								<div class="color-444 f-size14">	
									Plot No.18,2nd Floor,
									Opposite GHMC Park,
									ICRISAT Colony, Madinaguda,
									Hyderabad, Telangana 500050.
								</div>
								<a id="select-add" class="pull-right" href="#" data-toggle="modal" data-target="#choose-address" data-backdrop="static" data-keyboard="false"><i class="fa fa-edit"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</div>
		</div>
	</div>
	<!---- content part --->
  
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
	
	<!---- forgot password popup --->
	<div id="choose-address" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit/Choose Address</h4>
				</div>
				<div class="modal-body">
					<form class="edit-address" data-toggle="validator" role="form">
						<ul class="row p-all0 list-none">
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize d-table">select city
									<sup><i class="fa fa-asterisk imput-imp"></i></sup>
									</label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select sub area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">Pincode
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<input id="register-password" class="input-form" name="register-password" placeholder="Pincode" title="" type="text" value="" required />
								</div>
							</li>
							<li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">Enter address
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<textarea class="" rows="4"></textarea>
								</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-capitalize" >Edit</button>
				</div>
			</div>

		</div>
	</div>
	<!---- forgot password popup --->
	
	<!--- Edit address --->
	<div id="choose-address" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit/Choose Address</h4>
				</div>
				<div class="modal-body">
					<form class="edit-address" data-toggle="validator" role="form">
						<ul class="row p-all0 list-none">
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize d-table">select city
									<sup><i class="fa fa-asterisk imput-imp"></i></sup>
									</label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select sub area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">Pincode
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<input id="register-password" class="input-form" name="register-password" placeholder="Pincode" title="" type="text" value="" required />
								</div>
							</li>
							<li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">Enter address
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<textarea class="" rows="4"></textarea>
								</div>
							</li>
						</ul>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-capitalize" >Edit</button>
				</div>
			</div>

		</div>
	</div>
	<!--- Edit address --->
	
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>