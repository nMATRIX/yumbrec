<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.base.yumbrec.model.PackageDetails"%>
<%@ page import="com.base.yumbrec.model.Address"%>
<%@ include file="header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script>
		 $(document).ready(function () {
			$('#datePick').multiDatesPicker();
		});
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
	
	<script type="text/javascript">
	
	function addressbutton()
	{
		$("#proceedtocheckoutbutton").show();
	}
	
	function saveAddress()
	{
		var url = "${pageContext.servletContext.contextPath}/saveAddress";
	     $.ajax({
	 		type : "POST",
	 		url : url,
	 		datType : "json",
	 		ContentType : "application/json; charset=utf-8",
	 		data :{addressTypeForSave:$("#addressTypeForSave").val(),cityId:1,areaId:1,pincodeId:1,laneDescriptionid:$("#laneDescriptionid").val(),addressMealTimeAndDateDesc:$("#addressMealTimeAndDateDesc").val()} ,
	 		success : function(data) 
	 		{
	 		   if(data=='no')
	 		   {
	 	 			 homepagePage();
	 	 	   }
	 		   else
	 		   {
	 	 		  var other = 3;
	 			  $("#addAddressModal").modal('hide');
	 				checkoutPage();
	 	 	 	}
	 			
			
	 		  },
	 		 error : function(xhr, status, textError) 
	 		     {
			     }
	 		    });
	}
	
	function checkoutPage()
	{
		 window.location="${pageContext.servletContext.contextPath}/checkout";
	}
	
	function addAddressToCart(e)
	{
		   var addressType=3;
		   var url = "${pageContext.servletContext.contextPath}/addAddressForAllOrders";
		      $.ajax({
		       type : "POST",
		       url : url,
		       datType : "json",
		       ContentType : "application/json; charset=utf-8",
		       data : "addressType="+ addressType,
		       success : function(response) 
		       {
		    	checkoutPage();
		       },
		    error : function(xhr,status,textError) 
		      {
		      }

		   });
	}
	
	
	
	
	function addressTypeHidden(e)
	{
	$("#addressHiddenId").val(e);
	
	var v =$("#addressHiddenId").val();
	//alert("v " +v);
	$("#choose-address").show();
	}
	
	</script>
	
	
	
	
  </head>
  <body>
	
	<!---- content part --->
	<div>
		<div class="address-page">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 colxs-12 text-center m-bottom10 text-capitalize color-theme"><h3>Choose address for all orders</h3></div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
						<div class="panel panel-warning">
							<div class="panel-heading text-center">
								<i class="fa fa-home color-000"></i>
								<h3 class="panel-title color-000">Home</h3>
							</div>
							  <div align="right" class="menuItemType col-md-12"><!-- onclick="addNewAddress()" -->
						       <font color="#444"><i id="addHomeAddressId"  data-toggle="modal" data-target="#addAddressModal" class="fa fa-pencil-square fa-2x"></i></font>
					          </div>
							<div class="panel-body" id="addressDescriptionForHome">
							<span id="enterHomeAddress" style="display: none">Please enter Address</span>
								 <c:if test="${not empty model.homeAddress.houseNo}">
								   ${model.homeAddress.houseNo},
								</c:if> 
								<br />
								 <c:if test="${not empty model.homeAddress.area.areaDesc}">
								    ${model.homeAddress.area.areaDesc},
								</c:if> 
								<br />
								
								<c:if test="${not empty model.homeAddress.landMrk}">
								    ${model.homeAddress.landMrk},
								</c:if>
								<br />
								 <c:if test="${not empty model.homeAddress.city}">
								   ${model.homeAddress.city.cityDesc}-
								</c:if>
								<br />
								<c:if test="${not empty model.newAddress.pincode.pincodeDesc}">
								   ${model.newAddress.pincode.pincodeDesc}
								</c:if><br />
								<a id="choose-adds" onclick="addressTypeHidden(1)" href="#" data-toggle="modal" data-target="#choose-address"><i class="fa fa-edit"></i></a>
							</div>
							<div class="panel-footer clearfix">
							<div class="text-center">
								<a href="#" id="chooseAddressButton" value="1" onclick="addAddressToCart('${model.homeAddress.id}')" class="btn btn-primary"><i class="fa fa-check-circle p-right10"></i>Choose</a>
							</div>
							</div>
						</div>
					 </div>					
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
						<div class="panel panel-warning">
							<div class="panel-heading text-center">
								<i class="fa fa-building color-000"></i>
								<h3 class="panel-title color-000">Office</h3>
							</div>
							<div class="panel-body">
								<span id="enterHomeAddress" style="display: none">Please enter Address</span>
								<c:if test="${not empty model.officeAddress.houseNo}">
								    ${model.officeAddress.houseNo},
								</c:if>
								<br />
								<c:if test="${not empty model.officeAddress.area.areaDesc}">
								   ${model.officeAddress.area.areaDesc},
								</c:if>
								<br />
								
								<c:if test="${not empty model.officeAddress.landMrk}">
								    ${model.officeAddress.landMrk},
								</c:if>
								<br />
								<c:if test="${not empty model.officeAddress.city.cityDesc}">
								    ${model.officeAddress.city.cityDesc}-
								</c:if><br />
								<c:if test="${not empty model.newAddress.pincode.pincodeDesc}">
								   ${model.newAddress.pincode.pincodeDesc}
								</c:if>
								<br />
								<a id="choose-adds" onclick="addressTypeHidden(2)" href="#" data-toggle="modal" data-target="#choose-address"><i class="fa fa-edit"></i></a>
							</div>
							<div class="panel-footer clearfix">
							<div class="text-center">
									<a href="#" class="btn btn-primary" id="chooseAddressButton" value="2" onclick="addAddressToCart('${model.officeAddress.id}')" ><i class="fa fa-check-circle p-right10"></i>Choose</a>
								</div>
							</div>
						</div>
					</div>	 	 			
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
						<div class="panel panel-warning">
							<div class="panel-heading text-center">
								<i class="fa fa-institution color-000"></i>
								<h3 class="panel-title color-000">Other</h3>
							</div>
							<div class="panel-body">
								<span id="enterHomeAddress" style="display: none">Please enter Address</span>
								<c:if test="${not empty model.newAddress.houseNo}">
								    ${model.newAddress.houseNo},
								</c:if>
								<br />
								<c:if test="${not empty model.newAddress.area.areaDesc}">
								    ${model.newAddress.area.areaDesc},
								</c:if>
								<br />
								
								<c:if test="${not empty model.newAddress.landMrk}">
								    ${model.newAddress.landMrk},
								</c:if>
								<br />
								<c:if test="${not empty model.newAddress.city.cityDesc}">
								   ${model.newAddress.city.cityDesc}
								</c:if><br />
								<c:if test="${not empty model.newAddress.pincode.pincodeDesc}">
								   ${model.newAddress.pincode.pincodeDesc}
								</c:if>
								<br />
								<a id="choose-adds" onclick="addressTypeHidden(3)" href="#" data-toggle="modal" data-target="#choose-address"><i class="fa fa-edit"></i></a>
							</div>
							<div class="panel-footer clearfix">
							<div class="text-center">
									<a href="#" class="btn btn-primary" id="chooseAddressButton" value="3" onclick="addAddressToCart('${model.newAddress.id}')" ><i class="fa fa-check-circle p-right10"></i>Choose</a>
								</div>
							</div>
						</div> 
					</div> 			
				<div id="proceedtocheckoutbutton" style="display: none">
				<button id="proceedtocheck" onclick="addAddressToCart('${model.newAddress.id}')">Proceed to checkout</button>
				</div>
				
				</div>
			</div>
		</div>
	</div>
	<!---- content part --->
  
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
	
	<!---- forgot password popup --->
	<div id="choose-address" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal">&times;</button>
				    <h4 class="modal-title">Edit/Choose Address</h4>
				</div>
				<div class="modal-body">
					<form:form method="post" action="${pageContext.servletContext.contextPath}/saveAddress"  modelAttribute="user"  class="edit-address" data-toggle="validator" role="form" >
						<ul class="row p-all0 list-none">
							 <li class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group required">
									<label for="register-password"  class="text-capitalize d-table">select city
									<sup><i class="fa fa-asterisk imput-imp"></i></sup>
									</label>
									<form:input type="hidden" id="addressHiddenId" path="addressTypeId"></form:input>
							        <form:select path="city.id" id="cityId" required="required">                                    
									   <form:option value="0">Select City</form:option>
									   <c:forEach var="city" items="${model.cityList}" varStatus="loop">
									      <form:option value="${city.id}">${city.cityDesc}</form:option>
									   </c:forEach>
							       </form:select>
								</div>
							</li> 
							 <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<form:select path="area.id" class="" id="areaId"  required="required"  >                                    
										<form:option value="0">Select Area</form:option>
									    <c:forEach var="area" items="${model.allareaDetails}" varStatus="loop">
											 <form:option value="${area.id}">${area.areaDesc}</form:option>
									   </c:forEach>
								    </form:select>
								</div>
							</li> 
							<!--  <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group required">
									<label for="register-password" class="text-capitalize">select sub area
									<sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
									<select>
										<option>1</option>
										<option>1</option>
										<option>1</option>
										<option>1</option>
									</select>
								</div>
							</li>  -->
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						       <div class="form-group required">							
							      <label for="register-password" class="text-capitalize">Pincode
							           <sup><i class="fa fa-asterisk imput-imp"></i></sup></label>
					                   <form:input path="pincodeDetails.pincodeDesc" type="number" id="pincodeId" min="000000" max="999999" name="pincodeDetail" class="form-control" placeholder="Enter Pincode Here" onfocus="getpincode()"/>
						       </div>
					      </li>
							<li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<input type="text" value="" id="addressTypeForSave" name="addressType" style="display: none"> 
					            <input type="text" value="0" id="addressMealTimeAndDateDesc" name="addressMealTimeAndDate" style="display: none">
						        <div class="">
							       <div class="">
							          <div>
								      <label for="loginPassword" class="control-label">
								      <font color="#000000" style="text-shadow: none;"><sup>*</sup>Enter Address</font></label>
							        </div>
								     <form:textarea style="height: 80px;" path="landmark" maxlength="200" class="form-control" name="landmark" id="laneDescriptionid" required="required"></form:textarea>
							     </div>
						      </div>
							</li>
							</ul>
					          <button type="submit"  class="btn btn-primary text-capitalize" >Submit</button>
			      </form:form>
				</div>
			   
			</div>

		</div>
	</div>
	<!---- forgot password popup --->
	
	
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>