<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.base.yumbrec.model.PackageDetails"%>
<%@ page import="com.base.yumbrec.model.Address"%>
<%@ include file="header.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec checkout page</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script>
		 $(document).ready(function () {
			$('#datePick').multiDatesPicker();
		});
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
	
	
	<script type="text/javascript">
	
	function checkoutPage()
	{
		 window.location="${pageContext.servletContext.contextPath}/checkout";
	} 
	function homepagePage()
	{
		 window.location="${pageContext.servletContext.contextPath}/";
	} 
	
	function subplan(e)
	{
		$("#addressMealTimeAndDateDesc").val(e);
		$("#addressTypeForSave").val("newAddress");
		$("#addAddressModal").modal('show');
	}
	
	function deleteOrder(e)
	{
		//alert("delete was clicked");
		
		var url = "${pageContext.servletContext.contextPath}/deleteOrder"
	    $.ajax({
	       type : "POST",
	       url : url,
//		   datType : "json",
	       ContentType : "application/json; charset=utf-8",
	       data : {id: e},
	       success : function(response) 
	          {
            	 // alert("deleted "+response);
		          if(response!='0')
		             {
	 	            	//alert(" item deleted ");
			            checkoutPage();
		             }
		          else
		             {
			            homepagePage();
		             }
	           },
	        error : function(xhr,status,textError) 
	        {
	        }
	          });
	}
	
	
function increseItemQuantity(e)
	{
	//alert("hello increase");
	var url = "${pageContext.servletContext.contextPath}/increseItemQuantity";
	$.ajax({
	       type : "POST",
	       url : url,
		   datType : "json",
	       ContentType : "application/json; charset=utf-8",
	       data : {packageId: e},
	       success : function(response) 
	          {
		          if(response!='0')
		             {
		        	  window.location.reload(true);	              
		        	  }
		          else
		             {
			            homepagePage();
		             }
	           },
	        error : function(xhr,status,textError) 
	        {
	        	alert("fail1");
	        }
	          });
	}
	

function  decreseItemQuantity(e)
{
	//alert("hello decrease");
  var url=	"${pageContext.servletContext.contextPath}/decreaseItemQuantity";
  $.ajax({
      type : "POST",
      url : url,
	  datType : "json",
      ContentType : "application/json; charset=utf-8",
      data : {packageId: e},
      success : function(response) 
         {
	          if(response!='0')
	             {
	        	  //alert("response "+response[4]);
	        	  if(response[4]=='1')
	        		  {
	        		  //alert("at least one item will be there");
	        		  deleteOrder(e)
	        		  }
	        	  window.location.reload(true);	           
	        	 }
	          else
	             {
		            homepagePage();
	             }
          },
       error : function(xhr,status,textError) 
       {
    	   alert("fail2");
       }
         });
}

	
	
	</script>
	
  </head>
  
  <body>
	<!---- content part --->
	<div>
		<div class="yumbric-checkout">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-000 f-size18 text-capitalize m-bottom10">order summary</div>						
						
						<div class="checkout-lists table-responsive">
						<table class="table table-bordered table-striped">
							<tbody>
							<tr>
							<th><center>Package Details</center></th>
							<th class="width175 text-center"><center>Week Days</center></th>
							<th class="width175"><center>Quantity</center></th>
							<th class="width175"><center>Order Date</center></th>
							<th><center>Delete</center></th>
							<th class="width175"><center>Price</center></th>
							</tr>
							<c:forEach var="cartOrder" items="${model.cartDetailsList}" varStatus="loop">
								<tr>
									<td>
										<div class="d-table">
										<div class="d-tbcell v-middle p-right10">
											<img src="${pageContext.servletContext.contextPath}/resources/images/healthymeal.png" />
										</div>
										<div class="d-tbcell v-middle">
											<div>${cartOrder.packageName}</div>
											<div>${cartOrder.pkgDesc.packageMenuDesc}</div>											
										</div>
									</td>
									<td>
									<div>${cartOrder.weekType.weekTypDesc}</div>
									<%--  <td>${cartOrder.quantity}
									</td> --%>
									  <td class="text-center">
										<form id='myform' method='POST' action='#'>
											<input type="button" value='-' onclick="decreseItemQuantity(${cartOrder.id})" class='qtyminus' field='quantity' />
											<input type='text' name='quantity' value='${cartOrder.quantity}' class='qty' />
											<input type="button"  onclick="increseItemQuantity(${cartOrder.id})" value='+' class='qtyplus' field='quantity' />
										</form>
									</td>  
									<td class="text-center f-size18 f-bold">${cartOrder.orderStartDate}</td>
									<td class="text-center remove-item">
										<button class="btn btn-danger color-fff text-decnone color-444 f-size14" onclick="deleteOrder('${cartOrder.id}')"><i class="fa fa-trash"></i><div></div></button> 
									</td>
									<td class="text-center"><div class=" f-size18 f-bold"><i class="fa fa-rupee p-right10"></i>${cartOrder.priceOfItem}</div></td>
								</tr>
								<%--<tr>
								 <td colspan="9"> <font color="#666666"  onclick="subplan('${cartOrder.packageId}')"><i class="fa fa-pencil-square fa-2x"></i></font> DeliveryAddress:
								<c:if test="${not empty  cartOrder.address}">
								    <c:if test="${not empty cartOrder.address.houseNo}" >
								    ${cartOrder.address.houseNo},</c:if> <c:if test="${ not empty cartOrder.address.laneDesc}" >
								    ${cartOrder.address.laneDesc},</c:if> 
								    <c:if test="${not empty model.newAddress.landMrk!=null}" >
								    ${cartOrder.address.landMrk}</c:if>
									 <c:if test="${not empty cartOrder.address.area.areaDesc}" >
									${cartOrder.address.area.areaDesc},</c:if> - <c:if test="${not empty cartOrder.address.city.cityDesc}" >
									${cartOrder.address.city.cityDesc}-</c:if>   <c:if test="${not empty cartOrder.address.pincode.pincodeDesc}" >
									${cartOrder.address.pincode.pincodeDesc}.</c:if>
								</c:if> 
								</td>
								</tr> --%>
								</c:forEach>
							</tbody>
						</table>
						</div>
					</div>
					
					<div id="online-paybtn" class="form-group" align="center">
						<form role="form" id="register" name="reister" modelAttribute="user" class="form-horizontal" action="${pageContext.servletContext.contextPath}/orderSaveWithOutCopuon"  method="post"> 
							   <input type="text" style="display:none" name="paymode" value="4"> 
							   <input type="submit"  class="btn btn-success btn-lg orderConfirm" value="Cash On Delivery" />
							   <br>
							   <br>
					   </form>
					  <%--  <form role="form" id="register" name="reister" modelAttribute="user" class="form-horizontal" action="${pageContext.servletContext.contextPath}/orderSaveWithOutCopuon" method="post"> 
							  <input type="text" style="display:none" name="paymode" value="6">
							  <input type="submit" class="btn btn-success btn-lg orderConfirm" value="Pay Online" />
					  </form> --%>
				</div>
					
					
					
				</div>
			</div>
		</div>
	</div>
	<!---- content part --->
  <div id="addAddressModal" class="addressModal modal fade">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<script type="text/javascript">
				$(document).ready(function(){
					$("#pincodeId")
							.autocomplete(
									{
										source : function(request, response) {
											$
													.getJSON(
															"${pageContext.request.contextPath}/getMachedNames.web",
															{
																term : request.term
															}, response);
										}
									});
				});
				</script>
				           <div class="modal-header">
				             <button type="button" class="close" data-dismiss="modal">&times;</button>
				             <h4 class="modal-title">Add New Address</h4>
				           </div>
				<div class="modal-body">
					<input type="text" value="" id="addressTypeForSave" name="addressType" style="display: none"> 
					<input type="text" value="0" id="addressMealTimeAndDateDesc" name="addressMealTimeAndDate" style="display: none">
						<div class="">
							<div class="">
							  <div>
								 <label for="loginPassword" class="control-label">
								 <font color="#000000" style="text-shadow: none;"><sup>*</sup>Enter Address</font></label>
								</div>
								<textarea style="height: 80px;" maxlength="200" class="form-control" name="landmark" id="laneDescriptionid" required="required"></textarea>
s							</div>
						</div>
					</div>
					<div class="modal-footer">
								<input type="button" class="btn btn-default"
									onclick="saveAddress()" value="submit" />
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
	
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>