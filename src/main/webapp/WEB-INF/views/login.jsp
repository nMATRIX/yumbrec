<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="header.jsp" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yumbrec</title>
	
	<link href="${pageContext.servletContext.contextPath}/resources/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" >

    <!-- css -->
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"> <!--- bootstrap Style Sheet ---->
    <link href="${pageContext.servletContext.contextPath}/resources/css/animate.css" rel="stylesheet"> 
	<link href="${pageContext.servletContext.contextPath}/resources/css/mdp.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/ec-css.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/style1.css" rel="stylesheet"> 
    <link href="${pageContext.servletContext.contextPath}/resources/css/media-queries.css" rel="stylesheet"> 
	
	
	<!--- Javascript --->
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/script.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui-1.11.1.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.multidatespicker.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script>
		 $(document).ready(function () {
			$('#datePick').multiDatesPicker();
			$('#emailRadioButton').on('click', function () {
				$('#errorid2').hide();
			});
		});
		$(document).ready(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100){
				$('#goTop').stop().animate({
					bottom: '20px'    
					}, 500);
			}
			else{
				$('#goTop').stop().animate({
				   bottom: '-50px'    
				}, 500);
			}
		});
		$('#goTop').click(function() {
			$('html, body').stop().animate({
			   scrollTop: 0
			}, 500, function() {
			   $('#goTop').stop().animate({
				   bottom: '-50px'    
			   }, 500);
			});
		});
	}); 
	</script>	
	<script type="text/javascript">
    
    $(document).ready(function(){
    $("#emailRadioButton").click(function(){
		$("#emailLabel").show();
		$("#emailField").show();
		$("#mobileNoLabel").hide();
		$("#mobileNoField").hide();
	});

	$("#mobileRadioButton").click(function(){
		$("#emailLabel").hide();
		$("#emailField").hide();
		$("#mobileNoLabel").show();
		$("#mobileNoField").show();
		
	});
    });
    
</script>
	
	<script type="text/javascript">
	
	
	 var specialKeys = new Array();
	    specialKeys.push(8); //Backspace
	    function IsNumeric(e) 
	    {
	        var keyCode = e.which ? e.which : e.keyCode
	        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	        document.getElementById("errorId").style.display = ret ? "none" : "inline";
	        return ret;
	    }	
	
	    var specialKeys = new Array();
	    specialKeys.push(8); //Backspace
	    function IsNumeric1(e) 
	    {
	        var keyCode = e.which ? e.which : e.keyCode
	        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	        document.getElementById("errorid2").style.display = ret ? "none" : "inline";
	        return ret;
	    }
	    
	
	function homepagePage()
	{
		 window.location="${pageContext.servletContext.contextPath}/";
	} 

	function addresspage()
	{
			 window.location="${pageContext.servletContext.contextPath}/addresspage";
	} 
	
	function login()
	{
		var status=true;
		var exp = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		var email=$("#emailForLogin").val();
		var password=$("#passwordForLogin").val();
		
		 if( exp.test(email.trim()))
		 {
			  $("#errorloginPageId").hide();
		 }
		 else
		 {
			  $('#errorloginPageId').text('Please Enter Valid Email Address');
			  $("#errorloginPageId").show();
			  status=false;
		 }
		var checkOutStatus=$("#checkIdFromCartOrLogin").val(); 
		var url = "${pageContext.servletContext.contextPath}/login";
		$.ajax({
		 		type : "POST",
		 		url : url,
		 		datType : "json",
		 		ContentType : "application/json; charset=utf-8",
		 		data :{inputNameId:email.trim(),passwordId:password.trim()} ,
		 		success : function(response) 
		 		{
	 			if(response!="1" && response!="2" && response!="3" && response!=null)
	 			{
					if(checkOutStatus=="1")
					{
						addresspage();
					}
					else
					{
						$("#errorForLogin").show();
					}
	 			}
	 			else if(response=="1")
	 			{
	 	        	 $("#errorloginPageId").text("Please Enter Correct Password");
	 	        	 $("#errorloginPageId").show();
	 	        }
	 			else if(response=="2")
	 			{
	 	        	 $("#errorloginPageId").text("Not Registered");
	 	        	 $("#errorloginPageId").show();
	 	        }
	 		   },
	 		error : function(xhr, status, textError) 
	 		{
			}
	 		    });
	}
	
	
	/*  function register(e)
	  {
		 		var status=true;
		    	var firstname=$("#userNameForRegister").val();
			    var lastname=$("#lastnameid").val();
			    var email=$("#emailForRegister").val();	
			    var mobile=$("#mobilenumberForRegister").val();
			    var password=$("#passowordid").val();
				var confirmPassword=$("#conforpasswordid").val();
				
				 var regex = /^[a-zA-Z ]{3,30}$/;
				 
				 if (regex.test(firstname.trim())) 
				 {
			    	  $("#userNameErrorMessage").hide();
			     }
			    else 
			    {
			    	  $("#userNameErrorMessage").show();
			    	  status=false;
			    }
				 
				 var exp = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		 		   
		 		  if( exp.test(email.trim()))
		 		  {
		 			  $("#emailIdErrorMessage").hide();
		 		  }
		 		  else
		 		  {
		 			  $("#emailIdErrorMessage").show();
		 			  status=false;
		 		  }
		 		  
		 		 exp=/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
		 		  if( exp.test(mobile.trim()))
		 		  {
		 			  $("#mobileErrorMessage").hide();
		 		  }
		 		  else
		 		  {
		 			  $("#mobileErrorMessage").show();
		 			  status=false;
		 			
		 		  }
		 		 if(mobile.trim().length>10)
		 		 {
		 			 $("#mobileErrorMessage").show();
		 			  status=false;
		 		 }   
				$("#passwordErrorMessage").hide();
				  if((password.trim()).length<6)
				  {
					  $(".error-message").show();
					  $("#passwordErrorMessage").show();
					  status=false;
				  }
				  $("#confirmpasswordErrorMessage").hide();
				  if((confirmPassword.trim())!=(password.trim()))
				  {
					  $(".error-message").show();
					  $("#passwordErrorMessage").hide();
					  $("#confirmpasswordErrorMessage").show();
					  status=false;
				  }  
					  
	     if(status)
		    {
				  var url = "${pageContext.servletContext.contextPath}/register";	  
				  alert("status 3"+status);
				  $.ajax({
						type : "POST",
						url : url,
						datType : "json",
						ContentType : "application/json",
						data : "&firstname="+firstname+"&lastname="+lastname+"&email="+email+"&mobile="+mobile+"&password="+password,
						success : function(response) 
							      {
								    alert(response);
								     if(response!=null)
								       {
								    	/*  alert("Registration succesful");
									     //$("#registerSuccesMsg").show();
									     $("#registerErrorMessage").hide();
									     homepagePage(); */
							/* 	       }
								       else
									   {
								    	   alert("Registration succesful");
										     //$("#registerSuccesMsg").show();
										     $("#registerErrorMessage").hide();
										     homepagePage();
									     //$("#registerSuccesMsg").hide();
									     //$("#registerErrorMessage").show();
									   }
							       },
						 error : function(xhr,status,textError) 
							{
							}
						});
		          }
	} */ 
	 
	 function register()
	 {
		    var status=true;
		    var firstname=$("#userNameForRegister").val();
		    var lastname=$("#lastnameid").val();
		    var email=$("#emailForRegister").val();	
		    var mobile=$("#mobilenumberForRegister").val();
		    var password=$("#passowordid").val();
			var confirmPassword=$("#conforpasswordid").val();
			
			/* $("#passwordErrorMessage").hide();
			  if((password.trim()).length<6)
			  {
				  $(".error-message").show();
				  $("#passwordErrorMessage").show();
				  status=false;
			  }
			  $("#confirmpasswordErrorMessage").hide();
			  if((confirmPassword.trim())!=(password.trim()))
			  {
				  $(".error-message").show();
				  $("#passwordErrorMessage").hide();
				  $("#confirmpasswordErrorMessage").show();
				  status=false;
			  }  */
			  
			  
			  if(status)
				{
				  var url = "${pageContext.servletContext.contextPath}/register";
				  
				  $.ajax({
						type : "POST",
						url : url,
						datType : "json",
						ContentType : "application/json",
						data : "&firstname="+firstname+"&lastname="+lastname+"&email="+email+"&mobile="+mobile+"&password="+password,
						success : function(response) 
						{
							if(response=="")
						       {
								alert("Registration succesful");
							     $("#registerErrorMessage").hide();
							     homepagePage();
						       }else
							   {
						    	     
							   }
							
						},
				  error : function(xhr,status,textError) 
					{
					}
				  
						});
				     
				}
			  
	 }
	 
	 
	 
	 
	 
	 
	 
	 function forgotPasswordMail()
	 {
    	 var email=$('#forgotpasswordId').val();
    	 var exp = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	 var status=true;
    	 if( exp.test(email))
    	 {
			  $("#errorForgotPassword").hide();
		 }
    	 else
    	 {
			  $('#errorForgotPassword').text('Please Enter Correct Email Id');
			  $("#errorForgotPassword").show();
			  status=false;
		 }
    	 if(status)
    	 {
     		var url = "${pageContext.servletContext.contextPath}/forgotPassword";
				  $.ajax({
				 		type : "POST",
				 		url : url,
				 		datType : "json",
				 		ContentType : "application/json; charset=utf-8",
				 		data :{email:email} ,
				 		success : function(newResponse) 
				 		{
				 			if((newResponse=='2'))
				 			{
				 				 $('#errorForgotPassword').text('This EmailId is Not Registered');
				 				 $("#errorForgotPassword").show();
				 		    }
				 		    else
				 		    {   
				 			     $('#errorForgotPassword').text('Successfully Password Sent To Your EmailId');
			 				     $("#errorForgotPassword").show();
				 		    }
				 		},
				 		error : function(xhr, status, textError) 
				 		{
			 		    }
				 		});
    	 }
    }
	
	</script>
  </head>
  
  <body>
	<!---- content part --->
	 <c:if test="${sessionScope.firstname==null}"> 
	
	<div class="yumbrec-login">
	
		<div id="myTab" class = "nav nav-tabs">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 col-md-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
						<ul class="row list-none p-all0">
							<li class="active col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
								<a href="#home" data-toggle="tab" class="text-decnone color-000 text-uppercase f-bold"><h3>Login</h3></a>
							</li>						   
							<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
								<a href="#ios" data-toggle="tab" class="text-decnone color-000 text-uppercase f-bold"><h3>SignUP</h3></a>
							</li>						
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 col-md-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
					<div id="yumbrec-form" class = "tab-content">
						<div class = "tab-pane fade in active" id = "home">
							<!---- LoginForm ----->
							
							<c:if test="${sessionScope.errormsg!=null}">
									<span id="errorForLogin" style="color:red">${sessionScope.errormsg }</span></c:if>
							<form data-toggle="validator" action="${pageContext.servletContext.contextPath}/login" modelAttribute="user"  name="myForm" method="post" >
							 <div id="errorloginPageId" style="display:none">Please enter valid e-mail</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="enter-logintype">
									<input type="hidden" required />
										<div class="btn-group" data-toggle="buttons">
									 <label id="emailRadioButton" class="btn radio-inline">
									 	<input type="radio" value="1" name="signInRadioButtonForCheckOut" checked />
									 	<i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Email</span>
									 </label>
						             <label id="mobileRadioButton" class="btn radio-inline">
						             	<input type="radio" value="2"  name="signInRadioButtonForCheckOut" />
						             	<i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Mobile</span>
						             	</label>
                    	              <br/>  
                    	              </div>
						             </div>
						             <div class="form-group">
						             <label id="emailLabel" for="emailId">Email:</label>
						             <input id="emailField"  type="email" class="form-control" id="emailId" name="emailid" />
						             </div>
						             <div class="form-group">
						             <label id="mobileNoLabel" for="mobileNo" style="display: none;">Mobile:</label>
						             <input id="mobileNoField" type="text" class="form-control" id="mobileNo" onkeypress="return IsNumeric1(event);" title="please enter only valid mobile numbers" name="mobile"  pattern="[7-9]{1}[0-9]{9}" minlength="10" maxlength="10" style="display: none;"/>
						             <span id="errorid2" style="color: Red; display: none">* Please enter only numbers </span>
						             </div>
						             <div class="form-group">
						             <label for="password">Password:</label>
						             <input type="password" class="form-control" id="password" name="passwordid" required/>
						             </div>
										<div class="form-group">
											<button  type="submit" class="btn btn-primary btn-block f-size18 m-top25">Submit</button>
										</div>
								</div>
								</div>
								
							</form>
							<!---- LoginForm ----->
						</div>  
						<div class = "tab-pane fade" id ="ios">
						
						<c:if test="${sessionScope.registererror!=null}">
									<span id="errorForLogin" style="color:red">${sessionScope.registererror }</span></c:if>
						
							<form name="registerform" method="get">  
							
							<div class="error-message">
							<!-- <span id="forallvalidation" style="display: none">Please enter all fields</span>
						       <span id="registerSuccesMsg" style="display:none" ><font color="green">Registration Successful</font></span>
					 		   <span id="userNameErrorMessage" style="display:none" ><font color="red">Please Enter valid First Name</font></span>
							   <span id="registerErrorMessage" style="display:none" ><font color="red">You are already register with this Details, please login </font></span>
	                    	   <span id="emailIdErrorMessage" style="display:none" ><font color="red">Please Enter valid Email</font></span>
	                    	   <span id="mobileErrorMessage"  style="display:none" ><font color="red">Please Enter valid MobileNumber</font></span>  
	                    	   <span id="passwordErrorMessage"  style="display:none" ><font color="red">Please Enter Password Minimum 6 characters</font></span> --> 
	                       	   <span id="confirmpasswordErrorMessage"  style="display:none"><font color="red">Password and Confirm password does not match</font></span>
                          	</div>
							
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label for="userNameForRegister" class="control-label">First Name</label>
											<input type="text" class="form-control input-form" id="userNameForRegister" name="firstname" placeholder="First Name" minlength="3" maxlength="20" required>
										    <!-- <span id="userNameForRegister.errors">First name May not be empty</span> -->
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label for="lastnameid" class="control-label">Last Name</label>
											<input type="text" class="form-control input-form" id="lastnameid"  name="lastname" placeholder="Last Name" minlength="3" maxlength="20" required>
											<!-- <span id="lastnameid.errors">May not be empty</span> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="mobilenumberForRegister" class="control-label">Mobile Number</label>
									<input type="text" class="form-control input-form" onkeypress="return IsNumeric(event);" id="mobilenumberForRegister" name="mobile" placeholder="Mobile Number" pattern="[7-9]{1}[0-9]{9}" title="please enter valid mobile number" minlength="10" maxlength="10" required>
									<span id="errorId" style="color: Red; display: none">* Please enter only numbers </span>
									<!-- <span id="mobilenumberForRegister.errors">Mobile number must not be empty</span> -->
								</div>
									<div class="form-group">
									<label for="emailForRegister" class="control-label">Email</label>
									<input type="email" class="form-control input-form" id="emailForRegister" placeholder="Email" name="email" data-error="Bruh, that email address is invalid" required>
									<!-- <span id="emailForRegister.errors">Email must not be empty</span> -->
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											<label for="passowordid" class="control-label">Password</label>
											<div class="form-group">
											<input type="password" data-minlength="6" class="form-control input-form" name="password" id="passowordid" placeholder="Password" required>
											<!-- <span id="passowordid.errors">Password must not be empty</span> -->
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											<label for="conforpasswordid" class="control-label">Confirm Password</label>
											<div class="form-group">
											<input type="password" class="form-control input-form" id="conforpasswordid" name="confirmpassword" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required>
											<!-- <span id="conforpasswordid.errors">Confirm password must not be empty</span> -->
											<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="submit" onclick="register()" class="btn btn-primary btn-block f-size18" value="Register">
								</div>
							  </form>  
						</div>

					</div>
				</div>
			</div>
		</div>
</div> </c:if>
	<!---- content part --->
  
  
	<!---- footer ----->
	<footer>
		<div class="footer-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">terms & conditions</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Why US</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">Testimonials</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="color-444 f-size16 text-capitalize">FAQs</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-rights">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="color-fff f-size12">@yumbrec.in All Rights Reserved.</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	<!---- footer ----->
	
	<!---- forgot password popup --->
	<div id="forgot-password" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Forgot Password</h4>
				</div>
				<div class="modal-body">
					<form data-toggle="validator" role="form" method="post">
						<div  style="display:none;" class="error-msg f-size14" id="errorForgotPassword"></div>
						<div class="form-group">
							<label for="forgotpasswordId" class="control-label">Enter MobileNumber/Email</label>
							<input type="text" class="form-control input-form" id="forgotpasswordId"" placeholder="Enter MobileNumber/Email" required>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<input type="button" onclick="forgotPasswordMail()" class="btn btn-primary text-capitalize" value="submit" >
				</div>
			</div>

		</div>
	</div>
	<!---- forgot password popup --->
	
	
  <a id="goTop"><i class="fa fa-chevron-up fa-2x color-fff"></i></a>
  
  </body>
</html>