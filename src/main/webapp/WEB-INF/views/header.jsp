<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
	function logOutFun()
	{
		//alert("hello logout");
		  var url = "${pageContext.servletContext.contextPath}/logOut";
	      $.ajax({
	      type : "POST",
	      url : url,
	      datType : "json",
	      ContentType : "application/json; charset=utf-8",
// 	      data : {pincodeName: $("#pincodeId").val()},
	      success : function(response) 
	      {
	    	  //alert("Your are Logged Out Successfully...");
	    	  window.location="${pageContext.servletContext.contextPath}/";
	      },
	      error : function(xhr,status,textError) 
	      {
	      }
	     });
	}
	
	
    function sessionOut()
    {
	window.location="${pageContext.servletContext.contextPath}/userlogin";
    }
	function home()
	{
		window.location="${pageContext.servletContext.contextPath}/";
	}
</script>

<script type="text/javascript" language="javascript">
         
        function  procedtordersummery() 
        {
        	 window.location="${pageContext.servletContext.contextPath}/checkLoginStatus";
		}
        
    </script>
</head>
	<!--------------------- header part -------------->
	<div>
		<div class="socila-iconsmain">		
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="call-now m-tb10 color-ccc">
							Contact: +91 9876543210
						</div>
					</div>

			   </div>
		  </div>
	  </div>
  
  <header>
	<nav id="navbar-main" class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			 	 <span class="sr-only">Toggle navigation</span>
				 <span class="icon-bar"></span>
				 <span class="icon-bar"></span>
				 <span class="icon-bar"></span>
			  </button>
			 <div><a class="navbar-brand p-all0 m-all0 chef-logo1" href="${pageContext.servletContext.contextPath}/"><img src="${pageContext.servletContext.contextPath}/resources/images/Yumbrec logo.png" /></a></div>
			</div>
				<div id="navbar" class="navbar-collapse collapse">
				  <ul class="nav navbar-nav navbar-right f-size18">
					<li data-target="#myCarousel" data-slide-to="0" class="abc active"><a href="${pageContext.servletContext.contextPath}/#" class="text-capitalize hvr-underline-reveal">Home</a></li>
					<li><a href="${pageContext.servletContext.contextPath}/aboutUs" class="text-capitalize hvr-underline-reveal">About Us</a></li>
					<li><a href="${pageContext.servletContext.contextPath}/" class="text-capitalize hvr-underline-reveal">Order Now</a></li>
					<li><a href="${pageContext.servletContext.contextPath}/contactus" class="text-capitalize hvr-underline-reveal">Contact Us</a></li>
					
					<c:if test="${sessionScope.sessionLoginId==null}">
					   <li id="loginId" data-target="#myCarousel" data-slide-to="3" class="abc"><a href="${pageContext.servletContext.contextPath}/userlogin" class="text-capitalize md-trigger hvr-underline-reveal" data-modal="modal-14">Login</a></li>
					</c:if>
					
					<c:if test="${sessionScope.sessionLoginId!=null}"> 
					   <li><a id="myorderId" href="${pageContext.servletContext.contextPath}/myOrders" >My Orders</a></li>
					   <li><a href="" id="signOut" onclick="logOutFun()">Logout</a></li><sup><p class="login-name">Welcome , ${sessionScope.firstname} ${sessionScope.lastname}</p></sup>
					</c:if> 
					
					<li>
					
					  <a href="#" id="checkOutCart">
						    <img onclick="procedtordersummery();" src="${pageContext.servletContext.contextPath}/resources/images/cart.png" alt="cart"/>
							<input type="text" id="totalcartItems" value="" style="display:none">
							<span id="cartSizeIdWithItems" class="color-red f-size18 dispalyCartItemCount" style="display:none">${sessionScope.cartSize}</span>
							<span id="cartSizeIdWithOutItems" class="color-red f-size18 dispalyCartItemCount" style="display:none">0</span>
							
							<c:if test="${sessionScope.cartSize==null}">
							   <script type="text/javascript">
							     $(document).ready(function()
							      {
							    	$('#cartSizeIdWithItems').hide();
							    	$('#cartSizeIdWithOutItems').show();
							      });
							    </script>
							</c:if>
							
							<c:if test="${sessionScope.cartSize!=null}">
							 <script type="text/javascript">
							    $(document).ready(function()
							    {
							    	$('#cartSizeIdWithItems').show();
							    	$('#cartSizeIdWithOutItems').hide();
 							    	//alert("haai in with cart");
							    });
							    </script>
							</c:if>
						</a>
					</li>
				  </ul>
				</div>
			  </div>
		  </nav>
		</header>
	</div>
	<!--------------------- header part -------------->
 

</html>