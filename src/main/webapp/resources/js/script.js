$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
});

$(document).ready(function(){
    
     $('#panel').hide(); 

    $("#links a.tog").on('click', function() 
      {
       if($("#links a.active").length) $("#links a.active").removeClass('active');
       
       if($("#panel").is(":visible") == true) 
		{ $('#panel').slideUp(); } 
		else {}
       $("#links a.active").removeClass('active');
       $(this).addClass('active');
       $('#panel').slideToggle('fast');
	});
 
   $("#close").click( function() {
        $('#panel').slideUp();
		$("#links a").removeClass('active');
    });

 });
 
 jQuery(document).ready(function(){
    $('.qtyplus').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal)) {
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
    $(".qtyminus").click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
});